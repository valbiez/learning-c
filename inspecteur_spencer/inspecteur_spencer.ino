#include <Arduino.h>
#include <CircuitOS.h>
#include <Spencer.h>
#include <PreparedStatement.h>
#include "inspecteur_lib/InspecteurGadget.hpp"
// Dans Arduino IDE : Sketch > include library > Add .zip library > inspecteur_lib.zip


const char* listenResult;
IntentResult* intentResult;
bool processingIntent = false;
bool detectingIntent = false;
PreparedStatement* statement = nullptr;
bool synthesizing = false;

InspecteurGadget inspecteurGadget;

// Ecoute ce qu'on a dit à Spencer et réalise l'action appropriée
// Déclenché quand Spencer a reçu une commande vocale
void listenProcess(){
  if(synthesizing){
    Serial.println("Another speech synthesis operation is already pending");
  }else{
    synthesizing = true;
    delete statement;
    statement = new PreparedStatement();

    /* ============         Spécifique Inspecteur Gadget         ============ */
    
    // Ici, quand on parle à spencer, on déclenche l'action de l'inspecteur
    // intentResult->transcript est le texte qui a été traduit de ce qu'on a dit
    String texte_que_spencer_a_compris = intentResult->transcript;
    String action_de_l_inspecteur = inspecteur(texte_que_spencer_a_compris);
    string test = "Spencer";
    String test2 = String (test.c_str());
    /* ============       Fin spécifique Inspecteur Gadget       ============ */
    
    
    Serial.println(texte_que_spencer_a_compris.c_str());
    Serial.println(action_de_l_inspecteur.c_str());
    statement->addTTS(action_de_l_inspecteur);//action_de_l_inspecteur;
    statement->prepare(speechPlay);
  }

}

void listenError(){
  LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-angry.gif")), true);

}

void listenCheck(){
  if(listenResult != nullptr && !processingIntent){
    processingIntent = true;
    delete intentResult;
    intentResult = nullptr;
    SpeechToIntent.addJob({ listenResult, &intentResult });

    LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-loading2.gif")), true);

  }
  if(processingIntent && intentResult != nullptr){
    detectingIntent = false;
    processingIntent = false;
    listenResult = nullptr;
    if(intentResult->error != IntentResult::Error::OK && intentResult->error != IntentResult::Error::INTENT){
      Serial.printf("Speech to text error %d: %s\n", intentResult->error, STIStrings[(int) intentResult->error]);
      listenError();
      delete intentResult;
      intentResult = nullptr;
      return;
    }
    if(intentResult->intent == nullptr){
      intentResult->intent = (char*) malloc(5);
      memcpy(intentResult->intent, "NONE", 5);
    }
    if(intentResult->transcript == nullptr){
      intentResult->transcript = (char*) malloc(1);
      memset(intentResult->transcript, 0, 1);
    }
    // Spencer a bien reçu notre commande vocale, il va maintenant pouvoir l'interpréter
    listenProcess();
    delete intentResult;
    intentResult = nullptr;
  }
}

void BTN_press(){
  if (Net.getState() == WL_CONNECTED) {
    LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-listen.gif")), true);
    if(detectingIntent){
      Serial.println("Another listen and intent detection operation is already pending");
    }else{
      detectingIntent = true;
      listenResult = nullptr;
      Recording.addJob({ &listenResult });
    }
  }

}

void speechPlay(TTSError error, CompositeAudioFileSource* source){
  synthesizing = false;
  if(error != TTSError::OK){
    Serial.printf("Text to speech error %d: %s\n", error, TTSStrings[(int) error]);
    delete source;
    delete statement;
    statement = nullptr;
    LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-angry.gif")), true);

    return;
  }
  LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-talk.gif")), true);
  Playback.playMP3(source);
  Playback.setPlaybackDoneCallback([](){
    LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-wink.gif")), true);

  });
  delete statement;
  statement = nullptr;

}

/* ============         Spécifique Inspecteur Gadget         ============ */   
String inspecteur(String text) {

  //ici on appelera notre programme inspecteur gadget
  string inspecteurAction = inspecteurGadget.goGoGadgeto("manteau");

  // String est un objet de la lib Arduino
  // Nous devons convertir les string de la librairie standard vers les String de arduino
  // convert from std:string to String with underlying C string
  //return String(inspecteurAction.c_str());
  return String("Go Go Gadgeto manteau! le manteau se gonfle.");
}
/* ============       Fin spécifique Inspecteur Gadget       ============ */

// Paramétrage du robot.
// Ici, on pré-installera les gadgets de l'inspecteur
// Les gagdets pouvant être installés sont
// - "manteau"
// - "grappin"
// - "lampe"
void setup() {


  /* ============         Spécifique Inspecteur Gadget         ============ */
  
  inspecteurGadget.installer("manteau");
  inspecteurGadget.installer("grappin");
  // inspecteurGadget.installer("lampe");
  
  /* ============       Fin spécifique Inspecteur Gadget       ============ */  
  
  Serial.begin(115200);
  Serial.println();
  Spencer.begin();
  Spencer.loadSettings();
  Input::getInstance()->setBtnPressCallback(BTN_PIN, BTN_press);

  LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-wifi.gif")), true);
  Net.connect([](wl_status_t state){
    if (Net.getState() == WL_CONNECTED) {
      Playback.playMP3(SampleStore::load(SampleGroup::Special, "badum0"));
      LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-smile.gif")), true);
    } else {
      Playback.playMP3(SampleStore::load(SampleGroup::Error, "wifi"));
      LEDmatrix.startAnimation(new Animation(new SerialFlashFileAdapter("GIF-noWifi.gif")), true);
    }

  });


}

void loop() {
  LoopManager::loop();
  listenCheck();


}
