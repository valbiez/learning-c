# Inspecteur Gadget - Spencer


## Structure du projet

Contenu du dossier `inspecteur_spencer`

```bash
assets/                         # images de ce readme pour 
                                # documentation

inspecteur_lib/                 # contenu du projet de 
                                # l'inspecteur Gadget (lib 
                                # indépendante d'Arduino)

    dependencies/               # fonctions additionnelles 
                                # indépendantes de 
                                # l'inspecteur
                                
    Gadget/                     # Détails d'implémentation 
                                # de la lib inspecteur gadget
    tests/
        main_test_suite.cpp     # suite de tests

    InspecteurGadget.hpp        # API de l'inspecteur Gadget. 
                                # C'est le seul fichier qui 
                                # est importé dans le projet 
                                # Arduino ou dans le main.cpp

    main.cpp                    # Exemple d'utilisation de 
                                # l'inspecteur (exécutable 
                                # généré par la commande 
                                # make inspecteur_lib)

inspecteur_spencer.ino          # Projet Arduino pour le 
                                # Spencer (il doit avoir le 
                                # même nom que le dossier)

Makefile                        # Commandes make pour 
                                # compiler et tester la lib
```

## Pré-requis Arduino IDE

- Au préalable, il faut avoir installé CircuitBlocks
    - Cela installe les libs dont a besoin Spencer pour compiler

- Ouvrir Arduino IDE (une version assez récente est nécessaire)
- Ouvrir le projet `inspecteur_spencer.ino`
- ![File > Preferences](assets/arduino_001.png)
- ![Additional Boards Manager URLs](assets/arduino_002.png)
- ```
  https://raw.githubusercontent.com/CircuitMess/Arduino-Packages/master/package_circuitmess.com_esp32_index.json
  ```
- ![OK](assets/arduino_003.png)
- OK
- ![Tools > Board: ____ > Boards Manager](assets/arduino_004.png)
- ![Boards Manager](assets/arduino_005.png)
    - Saisir "spencer"  
    - Cliquer sur "CircuitMess ESP32 Boards"
- ![Boards Manager](assets/arduino_006.png)
    - Installer
- ![Boards Manager : en train d'installer](assets/arduino_007.png)
- ![Boards Manager : "CircuitMess ESP32 Boards" installé](assets/arduino_008.png)
    - Fermer
- ![Tools > Board: _____ > CircuitMess ESP32 > Spencer by CircuitMess](assets/arduino_009.png)
- ![Vérifier (Sketch > Verify / Compile)](assets/arduino_010.png)
- ![Compilation en cours](assets/arduino_011.png)
    - Cela peut prendre du temps
- ![Compilation et vérification OK](assets/arduino_012.png)
    - Compilation Terminée

L'environnement de déploiement pour Spencer est prêt!

## (optionnel) Pré-requis pour construire sous windows sans Arduino IDE

- installer CMake pour Windows : https://github.com/Kitware/CMake/releases/download/v3.21.3/cmake-3.21.3-windows-x86_64.msi
    - Add cmake to PATH
- Installer arduino-cli
    - https://arduino.github.io/arduino-cli/0.19/installation/#latest-release


```bash
# Mettre à jour
arduino-cli core update-index
# Installer spencer
arduino-cli core install cm:esp32
# Vérifier que c'est installé
arduino-cli core list
#
# ID         Installed Latest Name
# cm:esp32   1.2.4     1.2.4  CircuitMess ESP32

# Vérifier si le spencer est branché
arduino-cli board list
# 
# Port         Type              Board Name              FQBN                 Core
# /dev/ttyUSB0 Serial Port (USB) Spencer by CircuitMess cm:esp32:spencer cm:esp32
# 

# Compile
arduino-cli compile --fqbn cm:esp32:spencer inspecteur_spencer --build-path bin

# Upload sketch to the board
arduino-cli upload -p /dev/ttyUSB0 --fqbn cm:esp32:spencer inspecteur_spencer
```


## Développement

Pour la partie Arduino, ouvrir le projet `inspecteur_spencer.ino` dans Arduino IDE.
Pour la partie "lib", on pourra ouvrir le projet `inspecteur_spencer` dans l'éditeur de code habituel.

On pourra compiler rapidement et tester la partie "lib" à l'aide du `Makefile`.

Compilation simple

```bash
make inspecteur_lib
```

Exécution des tests

```bash
make test
```

## Déploiement sur le Spencer

Pour construire le tout sur le Spencer, on utilisera Arduino IDE.


## TODO list

- [X] Faire passer les tests de la lib Inspecteur Gadget
- [X] Déplacer le code de l'inspecteur Gadget dans le dossier `inspecteur_lib`
- [X] Dans CircuitBlocks adapter le programme d'exemple "Echo" -> `inspecteur_spencer.ino` et branché une fonction personalisée `String inspecteur(String text)`
    ```cpp
    String inspecteur(String text) {
    return text;
    //ici on appelera notre programme inspecteur gadjet//
    }
    ```
- [X] Dans Arduino IDE
    - [X] Vérifier que le projet `inspecteur_spencer.ino` compile
    - [X] Vérifier qu'il peut utiliser la lib de l'inspecteur
- [X] Ajouter les libs de spencer et arduino
    - [ ] Mettre à jour le Makefile : http://philomenale.com/index.php/arduino-makefile-c/

        - [ ] compiler le `inspecteur_spencer.ino`
        - [ ] -> Voir avec Olivier (optionnel) téléverser le programme vers spencer en ligne de commande à partir de l'éditeur de texte ou d'une ligne de commande
- [X] Intégrer la lib inspecteur_lib au programme spencer
    - [X] inclure le `InspecteurGadget.hpp`
    - [X] appeler le programme et retourner une valeur du type `String`
        - simplement on dira "manteau" au lieu de "go go gadgeto manteau" pour faire simple au début.
        - (optionnel) On pourra faire la détection de phrases commençant par "go go gadgeto" plus tard
- [ ] Tester le programme complet sur le robot




## Remove_me

- Installer MinGW : https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download
    - ![Mingw setup settings](assets/mingw_001.png)
    - Ajouter au PATH
        - MINGW_PATH
        - 
- Ajouter au PATH : 
    - ARDUINO_SDK_PATH
    - C:\Program Files (x86)\Arduino\hardware\tools\avr\bin
