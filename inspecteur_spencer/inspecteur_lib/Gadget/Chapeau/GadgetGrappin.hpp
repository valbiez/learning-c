#if !defined(GADGET_GRAPPIN_HPP)
#define GADGET_GRAPPIN_HPP

#include "GadgetChapeau.hpp"

class GadgetGrappin : public GadgetChapeau
{

public:
  virtual string activer(){
    return "Go Go Gadgeto " + nom + "! " + action;
  }
  virtual string getNom(){
      return nom;
  }

private:
    string nom = "hook";
    string action = "he catches a thief with his grappling hook!";

};

#endif // GADGET_GRAPPIN_HPP
