#ifndef EMPLACEMENT_CHAPEAU_HPP
#define EMPLACEMENT_CHAPEAU_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
//#include "Gadget.hpp"
#include "GadgetChapeau.hpp"
#include "../EmplacementPourGadgets.hpp"

using namespace std;


class EmplacementChapeau : public EmplacementPourGadgets
{
public:

    string installer(GadgetChapeau *const gadget)
    {
       return this->EmplacementPourGadgets::installer(gadget);
    }
};

#endif
