#ifndef EMPLACEMENT_DIVERS_HPP
#define EMPLACEMENT_DIVERS_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "../Gadget.hpp"
#include "GadgetDivers.hpp"
#include "../EmplacementPourGadgets.hpp"

using namespace std;

// Représente les Gadgets installés sur un robot (par exemple sur un inspecteur gadget)
class EmplacementDivers : public EmplacementPourGadgets
{
public:

    string installer(GadgetDivers *const gadget)
    {
       return this->EmplacementPourGadgets::installer(gadget);
    }

private:

};

#endif