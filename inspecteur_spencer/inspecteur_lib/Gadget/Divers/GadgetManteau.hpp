#ifndef GADGETS_MANTEAU_HPP
#define GADGETS_MANTEAU_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "GadgetDivers.hpp"

using namespace std;

// Le manteau s'installe sur l'emplacement des gadgets divers
class GadgetManteau : public GadgetDivers
{

public:
  virtual string activer(){
    return "Go Go Gadgeto " + nom + "! " + action;
  }
  virtual string getNom(){
      return nom;
  }

private:
    string nom = "coat";
    string action = "the coat swells.";
};

#endif
