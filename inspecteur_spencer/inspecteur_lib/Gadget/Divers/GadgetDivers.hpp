#ifndef GADGET_DIVERS_HPP
#define GADGET_DIVERS_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "../Gadget.hpp"

using namespace std;

// Représente un gadget qui s'installe sur l'emplacement "Divers"
class GadgetDivers : public Gadget
{
public:
  virtual string activer() = 0;
  virtual string getNom() = 0;
};

#endif