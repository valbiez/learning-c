#ifndef GADGETS_INSTALLES_HPP
#define GADGETS_INSTALLES_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "Gadget.hpp"
#include "EmplacementPourGadgets.hpp"
#include "GadgetNonInstalle.hpp"

using namespace std;

// Représente les Gadgets installés sur un robot (par exemple sur un inspecteur gadget)
class EmplacementPourGadgets
{
public:

    string activer(string nomDuGadget)
    {
        return recupereGadget(nomDuGadget)->activer();
    }

    string activerAleatoirement()
    {
        if (nombreDeGadgetsInstalles() == 0)
        {
            return "No gadget installed! The inspector falls on his ass!";
        }
        return recupererGadgetAleatoirement()->activer();
    }

    int nombreDeGadgetsInstalles()
    {
        return gadgets.size();
    }

    // Constructeur (allocation dynamique de mémoire)
    EmplacementPourGadgets()
    {
        // initialisation d'une seed basée sur la date courante
        // pour le générateur de nombres aléatoires
        std::srand(std::time(nullptr));
    }

protected:
    string installer(Gadget *const gadget)
    {
        auto nomDuGadget = gadget->getNom();
        gadgets[nomDuGadget] = gadget;
        return "The " + nomDuGadget + " gadget has been installed!";
    }

private:

    Gadget *recupereGadget(string nomDuGadget)
    {
        std::map<string, Gadget *>::iterator search = gadgets.find(nomDuGadget);

        // si le gadget est installé
        if (search != gadgets.end())
        {
            return gadgets[nomDuGadget];
        }

        return new GadgetNonInstalle(nomDuGadget);
    }

    Gadget *recupererGadgetAleatoirement()
    {
        int gadgetAleatoirementSelectionne = nombreAleatoire(nombreDeGadgetsInstalles());

        auto iteratorFromBeginning = gadgets.begin();
        for (int i = 0; i < gadgetAleatoirementSelectionne; i++)
        {
            ++iteratorFromBeginning;
        }

        Gadget *gadget = iteratorFromBeginning->second;
        return gadget;
    }



    int nombreAleatoire(int nombreMax)
    {
        int randomNumber = std::rand();
        return randomNumber % (nombreMax);
    }

private:
    // liste des gadgets, on voudrait pouvoir accéder à un gadget par son nom
    std::map<string, Gadget *> gadgets;
};

#endif