#ifndef GADGET_LAMPE_HPP
#define GADGET_LAMPE_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "../Gadget.hpp"
#include "GadgetDoigts.hpp"

using namespace std;

class GadgetLampe : public GadgetDoigts
{
    
public:
  virtual string activer(){
    return "Go Go Gadgeto " + nom + "! " + action;
  }
  virtual string getNom(){
      return nom;
  }

private:
    string nom = "light";
    string action = "He takes out a lamp with one finger to light up in the dark!";

};


#endif