#ifndef GADGET_DOIGTS_HPP
#define GADGET_DOIGTS_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "../Gadget.hpp"

using namespace std;

// Représente un gadget qui s'installe sur l'emplacement "Doigts"
class GadgetDoigts : public Gadget
{
public:
  virtual string activer() = 0;
  virtual string getNom() = 0;
};

#endif