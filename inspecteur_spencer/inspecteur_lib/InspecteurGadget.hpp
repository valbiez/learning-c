#ifndef INSPECTEUR_GADGET_HPP
#define INSPECTEUR_GADGET_HPP

#include "Gadget/EmplacementPourGadgets.hpp"
#include "Gadget/Doigts/EmplacementDoigts.hpp"
#include "Gadget/Chapeau/EmplacementChapeau.hpp"
#include "Gadget/Divers/EmplacementDivers.hpp"

#include "Gadget/GadgetsInventes.hpp"

using namespace std;

const int EMPLACEMENT_DIVERS = 0;
const int EMPLACEMENT_CHAPEAU = 1;
const int EMPLACEMENT_DOIGTS = 2;

class InspecteurGadget
{
private:
    EmplacementDivers emplacementDivers;
    EmplacementChapeau emplacementChapeau;
    EmplacementDoigts emplacementDoigts;

    GadgetsInventes gadgetsInventes;

    int nombreAleatoire(int nombreMax)
    {
        int randomNumber = std::rand();
        return randomNumber % (nombreMax);
    }

    // Retourne le numéro d'un emplacement pour gadget aléatoireà condition qu'au moins un gadget soit installé dessus
    // Sinon retourne le numéro de l'emplacement divers
    int emplacementAleatoireWithInstalledGadget(){
        int numberOfNonEmptyGadgetSlots=0;
        int nonEmptyGadgetSlots[3];
        if(emplacementChapeau.nombreDeGadgetsInstalles()>0){
            nonEmptyGadgetSlots[numberOfNonEmptyGadgetSlots] = EMPLACEMENT_CHAPEAU;
            numberOfNonEmptyGadgetSlots++;
        }
        if(emplacementDivers.nombreDeGadgetsInstalles()>0){
            nonEmptyGadgetSlots[numberOfNonEmptyGadgetSlots] = EMPLACEMENT_DIVERS;
            numberOfNonEmptyGadgetSlots++;
        }
        if(emplacementDoigts.nombreDeGadgetsInstalles()>0){
            nonEmptyGadgetSlots[numberOfNonEmptyGadgetSlots] = EMPLACEMENT_DOIGTS;
            numberOfNonEmptyGadgetSlots++;
        }
        if(numberOfNonEmptyGadgetSlots == 0 ){
            return EMPLACEMENT_DIVERS;
        }
        return nonEmptyGadgetSlots[nombreAleatoire(numberOfNonEmptyGadgetSlots)];
    }


public:
    // Retourne le message indiquant si le gadget a été installé
    string installer(string nomDuGadget)
    {
        string a = "";
        auto gadgetInstallable = gadgetsInventes.recupererGadgetInvente(nomDuGadget);
        // Ici, je veux savoir si les gadgets sont inventés avant de les installer
        if (gadgetInstallable == NULL)
        {
            return "The " + nomDuGadget + " gadget has not been invented yet! Cannot install!";
        }

        if (nomDuGadget.compare("lampe") == 0)
        {
            return emplacementDoigts.installer((GadgetDoigts *)gadgetInstallable);
        }
        if (nomDuGadget.compare("hook") == 0)
        {
            return emplacementChapeau.installer((GadgetChapeau *)gadgetInstallable);
        }
        if (nomDuGadget.compare("coat") == 0)
        {
            return emplacementDivers.installer((GadgetDivers *)gadgetInstallable);
        }
        return "No slot supports " + nomDuGadget + " gadget";
    }

    // Retourne l'action du gadget
    string goGoGadgeto(string nomDuGadget)
    {
        if (nomDuGadget.compare("lampe") == 0)
        {
            return emplacementDoigts.activer(nomDuGadget);
        }
        if (nomDuGadget.compare("hook") == 0)
        {
            return emplacementChapeau.activer(nomDuGadget);
        }
        if (nomDuGadget.compare("coat") == 0)
        {
            return emplacementDivers.activer(nomDuGadget);
        }
        //il se casse la gueule
        return emplacementDivers.activer(nomDuGadget);
    }

    string goGoGadgetoAleatoire()
    {
        int gadgetSlot = emplacementAleatoireWithInstalledGadget();

        switch (gadgetSlot)
        {
        case EMPLACEMENT_DIVERS:
            return emplacementDivers.activerAleatoirement();
        case EMPLACEMENT_CHAPEAU:
            return emplacementChapeau.activerAleatoirement();
        case EMPLACEMENT_DOIGTS:
            return emplacementDoigts.activerAleatoirement();
        default:
            return emplacementDivers.activerAleatoirement();
        }
    }

    InspecteurGadget()
    {
        gadgetsInventes.inventer("coat", "The coat inflates. You see the inspector flying as a big bubble.");
        gadgetsInventes.inventer("hook", "he catches a thief with his grappling hook!");
    }

    InspecteurGadget(GadgetsInventes p_gadgetsInventes)
    {
        gadgetsInventes = p_gadgetsInventes;
    }
};

#endif