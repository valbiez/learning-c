#include <iostream>
#include "utility-functions/fonction_split.cpp"
using namespace std;

int main(int argc, char *argv[])
{
    string text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris luctus viverra volutpat. Vivamus venenatis risus vitae cursus eleifend. Maecenas sit amet leo ac eros lacinia luctus. Mauris in sapien et massa facilisis accumsan. In hac habitasse platea dictumst. Aenean in risus ut nibh tristique fermentum sed sit amet velit. Nam non ipsum vel augue congue fermentum. Nulla ac enim venenatis, bibendum turpis non, varius augue. Nunc cursus volutpat est, tincidunt dictum elit fringilla accumsan. Vestibulum aliquam arcu et dolor euismod venenatis.";
    cout << text << endl;
    cout << endl;

    list<string> chunks = split(text, 130);

    // Tant que la liste a encore des lignes
    while (chunks.size() >= 1 )
    {
        cout << chunks.front() << endl;
        cout << endl;
        // enlever la ligne deja affichée
        chunks.pop_front();
    }
    
    return 0;
}
