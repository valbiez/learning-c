#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch_amalgamated.hpp"

#include "../InspecteurGadget.hpp"
#include "../Gadget/Divers/EmplacementDivers.hpp"
#include "../Gadget/Divers/GadgetManteau.hpp"
#include "../Gadget/Chapeau/EmplacementChapeau.hpp"
#include "../Gadget/Chapeau/GadgetGrappin.hpp"
#include "../Gadget/Doigts/EmplacementDoigts.hpp"
#include "../Gadget/Doigts/GadgetLampe.hpp"

TEST_CASE("L'inspecteur Gadget peut installer et utiliser des gadgets", "[inspecteurGadget]")
{
    InspecteurGadget inspecteur;
    SECTION("aucun gadget n'est installé par défaut")
    {

        string actionDuGedget = inspecteur.goGoGadgeto("coat");
        REQUIRE(actionDuGedget == "The coat gadget is not installed. The inspector falls on his ass!");
        REQUIRE(inspecteur.goGoGadgeto("hook") == "The hook gadget is not installed. The inspector falls on his ass!");
        REQUIRE(inspecteur.goGoGadgeto("truc non inventé") == "The truc non inventé gadget is not installed. The inspector falls on his ass!");

        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "No gadget installed! The inspector falls on his ass!");
    }

    SECTION("installer un manteau permet d'activer le manteau")
    {
        REQUIRE(inspecteur.installer("coat") == "The coat gadget has been installed!");
        REQUIRE(inspecteur.goGoGadgeto("coat") == "Go Go Gadgeto coat! The coat inflates. You see the inspector flying as a big bubble.");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Go Go Gadgeto coat! The coat inflates. You see the inspector flying as a big bubble.");
    }

    SECTION("installer un grappin permet d'activer le grappin")
    {

        REQUIRE(inspecteur.installer("hook") == "The hook gadget has been installed!");
        REQUIRE(inspecteur.goGoGadgeto("hook") == "Go Go Gadgeto hook! he catches a thief with his grappling hook!");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Go Go Gadgeto hook! he catches a thief with his grappling hook!");
    }

    SECTION("installer gadget qui n'a pas encore été inventé est impossible")
    {
        REQUIRE(inspecteur.installer("turbo smartphone") == "The turbo smartphone gadget has not been invented yet! Cannot install!");
    }
}

TEST_CASE("On peut inventer des gadgets et fournir cette liste à l'inspecteur", "[inspecteurGadget]")
{

    GadgetsInventes gadgetsInventes;

    SECTION("installer gadget qui n'a pas encore été inventé est impossible")
    {
        InspecteurGadget inspecteur = InspecteurGadget(gadgetsInventes);
        REQUIRE(inspecteur.installer("turbo smartphone") == "The turbo smartphone gadget has not been invented yet! Cannot install!");
    }

    SECTION("installer gadget qui a été inventé n'est pas possible si aucun emplacement ne le supporte")
    {
        REQUIRE(gadgetsInventes.inventer("turbo smartphone", "The inspector takes a selfie and sends it to instagram!") == "The turbo smartphone gadget is invented!");
        InspecteurGadget inspecteur = InspecteurGadget(gadgetsInventes);
        REQUIRE(inspecteur.installer("turbo smartphone") == "No slot supports turbo smartphone gadget");
        REQUIRE(inspecteur.goGoGadgeto("turbo smartphone") == "The turbo smartphone gadget is not installed. The inspector falls on his ass!");
    }

    SECTION("Les gadgets standard ne sont pas présents si on founit des gadgets inventés")
    {
        InspecteurGadget inspecteur = InspecteurGadget(gadgetsInventes);
        REQUIRE(inspecteur.installer("manteau") == "The manteau gadget has not been invented yet! Cannot install!");
    }
}

TEST_CASE("Emplacement des gadgets divers", "[emplacementDivers]")
{

    EmplacementDivers emplacementDivers;

    SECTION("on ne peut pas activer un gadget divers qui n'a pas été installé")
    {

        REQUIRE(emplacementDivers.activer("coat") == "The coat gadget is not installed. The inspector falls on his ass!");
    }

    SECTION("on peut activer un gadget divers qui a été installé")
    {
        emplacementDivers.installer(new GadgetManteau());
        REQUIRE(emplacementDivers.activer("coat") == "Go Go Gadgeto coat! the coat swells.");
    }
}

TEST_CASE("Emplacement des gadgets chapeau", "[gadgetChapeau]")
{

    EmplacementChapeau emplacementChapeau;

    SECTION("on ne peut pas activer un gadget du chapeau qui n'a pas été installé")
    {

        REQUIRE(emplacementChapeau.activer("grapple") == "The grapple gadget is not installed. The inspector falls on his ass!");
    }

    SECTION("on peut activer un gadget du chapeau qui a été installé")
    {
        emplacementChapeau.installer(new GadgetGrappin());
        REQUIRE(emplacementChapeau.activer("hook") == "Go Go Gadgeto hook! he catches a thief with his grappling hook!");
    }
}

TEST_CASE("Emplacement des doigts", "[EmplacementDoigts]")
{

    EmplacementDoigts emplacementDoigts;

    SECTION("on ne peut pas activer un gadget du doigt qui n'a pas été installé")
    {
        REQUIRE(emplacementDoigts.activer("light") == "The light gadget is not installed. The inspector falls on his ass!");
    }

   SECTION("on peut activer un gadget de la lampe qui a été installé")
    {
        emplacementDoigts.installer(new GadgetLampe());
        REQUIRE(emplacementDoigts.activer("light") == "Go Go Gadgeto light! He takes out a lamp with one finger to light up in the dark!");
    }

}

// Etapes prochaines
// ✔ 1. Renommer "GadgetsInstalles" en "EmplacementPourGadgets"
// ✔ 2. Déplacer ce qui concerne les gadgets divers (EmplacementDivers, GadgetDivers, GadgetManteau) dans un dossier "Divers"
// ✔ 2.b. Créer les autres types d'emplacement et de gadgets
// ✔ 2.b.1 : Divers : Manteau
// ✔ 2.b.2 : Chapeau : Grappin
// ✔ 2.b.3 : Doigts : Lampe "Une lampe sort du doigt pour éclairer dans l'obscurité"
// ✔ - créer l'emplacementDoigts; juste 5 gadgets;
// ✔ - créer gadgetDoits;
// ✔ - créer gadgetLampe;
// ✔ - empecher de mettre un gadget autre que doigts sur un emplacement de doigt
// ✔ 3. Passer EmplacementPourGadgets::installer(Gadget* gadget) en `protected`
// ✔ - Dans l'inspecteur Gadget remplacer EmplacementPourGadgets par EmplacementDivers et EmplacementChapeau
// Mettre à jour le comportement de l'inspecteur et les tests car on a des novelles problématiques qui on émergées
// - Que se passe quand on installe un seul gadget et que l'inspecteur active aléatoirement?
// - Dans l'inspecteur on veut savoir de quel type est un gadget installable pour pouvoir le mettre dans le bon emplacement (Gadget::getType()?)
// - Tests pour les cas d'emplacements non supportés
// - Renommage GadgetsInventes -> GadgetsInstallables
// - Renommages Catégories de gadgets -> les catégories sont préfixées par "Gadget" et les gadgets concrets ne sont plus préfixés
//   Ex. GadgetLampe -> Lampe, GadgetGrappin -> Grapping ...
// - Est-ce que la classe "GadgetInvente" est encore utile?
// 4. Duplication dans les gadgets spécifiques : "GoGo Gadget ..." : ce comportement pourrait être remonté dans Gadget
// X. Branche l'inspecteur à l'assistant vocal
/*
TEST_CASE("L'inspecteur peut lister les gadget installés", "[inspecteurGadget]")
{
    SECTION("aucun gadget n'est installé par défaut")
    {

        InspecteurGadget inspecteur;
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{}));
    }

    SECTION("1 gadget installé")
    {
        InspecteurGadget inspecteur;
        inspecteur.installer("manteau");
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau"}));
    }
    SECTION("2 gadgets installés")
    {

        InspecteurGadget inspecteur;
        inspecteur.installer("manteau");
        inspecteur.installer("grappin");
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau", "grappin"}));
    }
}
*/
/*
TEST_CASE("L'inspecteur peut lister les gadget pouvant être installés", "[inspecteurGadget]")
{
    SECTION("par défaut le manteau et le grappin sont installables")
    {
        InspecteurGadget inspecteur;
        auto liste = inspecteur.listeGadgetsInstallables();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau", "grappin"}));
    }

    SECTION("aucun gadget n'est installable")
    {
        InspecteurGadget inspecteur = InspecteurGadget(GadgetsInventes());
        auto liste = inspecteur.listeGadgetsInstallables();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{}));
    }

}
*/