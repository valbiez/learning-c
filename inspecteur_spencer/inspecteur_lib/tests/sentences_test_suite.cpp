#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch_amalgamated.hpp"
#include "../utility-functions/fonction_split.cpp"
/**
 * 
Par ex avec une pharse a au max 20 caractères

```
12345678901234567890

moins de 20
->
moins de 20

12345678901234567890

plus de 20 caractères
->
plus de 20 
caractères

12345678901234567890
                   X
plus de 40 caractères ça fait plus de 2 phrases
-> 
plus de 40
caractères ça fait plus de 2 phrases

12345678901234567890
                   X
plus de 40 
caractères ça fait 
plus de 2 phrases
```

`MAX` : le nombre de caractère max d'une phrase (ex. MAX=20, ou encore MAX=130)

1. Un phrase n'est pas découpée si elle fait moins de `MAX` caractères
    - On retourne la même phrase
2. Une phrase est découpée en 2 phrases si elle fait plus de `MAX` caractères mais moins de `MAX * 2` caractères
    - On retourne la premiere moitié et la 2eme moitié
    - Le découpage se fait à partir de la position du dernier espace avant la position qui correspond à `MAX`
3. Une phrase très longue aura plus de 2 morceaux
    - On coupe en 2
        - à gauche : moins de `MAX` caractères
        - à droite : le reste
    - Tant que le reste (à droite) fait plus de `MAX` caractères on continue de découper
*/

/*
    split("abcde",5)
    >> ["abcde"]

    split("abcd e",5)
    >> ["abcd", "e"]
*/
#include <string>
#include <list>
#include <iostream>
using namespace std;


TEST_CASE("TODO", "[TODO]")
{
    SECTION("Ajouter des phrases augmente la taille de la liste")
    {
        list<string> phrases;

        // Ajoute un texte à la liste
        phrases.push_back("abcde");
        // Ajoute un texte à la liste
        phrases.push_back("fghi");
        // Ajoute un texte à la liste
        phrases.push_back("fghi");
        int size = phrases.size();

        REQUIRE(size == 3);
    }

    SECTION("Lire la première phrase")
    {
        list<string> phrases;

        // Ajoute un texte à la liste
        // ["abcde"]
        phrases.push_back("abcde");

        // Ajoute un texte à la liste
        // ["abcde"]->["fghi"]
        phrases.push_back("fghi");

        string premierePhrase = phrases.front();
        REQUIRE(premierePhrase == "abcde");
    }

    SECTION("Lire la deuxième phrase apres avoir supprimé la première")
    {
        list<string> phrases;

        phrases.push_back("abcde");
        phrases.push_back("fghi");
        phrases.push_back("klm");

        // ["abcde"]->["fghi"]->["klm"]
        //     |                   |
        //  front()              back()

        phrases.pop_front();

        // ["fghi"]->["klm"]
        //     |        |
        //  front()  back()

        string premierePhrase = phrases.front();

        REQUIRE(premierePhrase == "fghi");
    }

    SECTION("Lire toutes les phrases")
    {
        list<string> phrases;

        phrases.push_back("abcde");
        phrases.push_back("fghi");
        phrases.push_back("klm");

        bool thereAreSentencesLeft = !phrases.empty();
        int sentencesRead = 0;
        while (thereAreSentencesLeft)
        {
            string sentence = phrases.front();
            cout << sentence << endl;
            sentencesRead++;

            phrases.pop_front();
            thereAreSentencesLeft = !phrases.empty();
        }

        REQUIRE(sentencesRead == 3);
    }

    SECTION("split sentence below sentence length")
    {
        list<string> phrases = split("abcde", 5);

        REQUIRE(phrases.front() == "abcde");
        REQUIRE(phrases.size() == 1);
    }

    SECTION("split another sentence below sentence length")
    {
        list<string> phrases = split("test", 5);

        REQUIRE(phrases.front() == "test");
        REQUIRE(phrases.size() == 1);
    }

    SECTION("split a 2 words sentence longer than the limit")
    {
        //                            1234567
        list<string> phrases = split("abc def", 5);

        // il y a 2 mots
        REQUIRE(phrases.size() == 2);

        // premier mot
        REQUIRE(phrases.front() == "abc");
        phrases.pop_front();

        // deuxième mot
        REQUIRE(phrases.front() == "def");
    }

    SECTION("split a 3 words sentence longer than the limit")
    {
        //                                5
        list<string> phrases = split("a bcd def", 5);

        // il y a 2 mots
        REQUIRE(phrases.size() == 2);

        // premier mot
        REQUIRE(phrases.front() == "a bcd");
        phrases.pop_front();

        // deuxième mot
        REQUIRE(phrases.front() == "def");
    }

    SECTION("split a 4 words sentence longer than the limit")
    {
        //                                 5
        list<string> phrases = split("a bcd de f", 5);

        // il y a 2 mots
        REQUIRE(phrases.size() == 2);

        // premier mot
        REQUIRE(phrases.front() == "a bcd");
        phrases.pop_front();

        // deuxième mot
        REQUIRE(phrases.front() == "de f");
    }

    SECTION("split a 3 words sentence longer than the limit into 3 parts")
    {
        //                                 5
        list<string> phrases = split("abc def ghi", 5);

        // il y a 3 mots
        REQUIRE(phrases.size() == 3);

        // premier mot
        REQUIRE(phrases.front() == "abc");
        phrases.pop_front();

        // deuxième mot
        REQUIRE(phrases.front() == "def");
        phrases.pop_front();

        // troisème mot
        REQUIRE(phrases.front() == "ghi");
    }
}