#include <string>
#include <list>
#include <iostream>
using namespace std;


//
// [ ] -> [ ] -> [ ]

list<string> split(string originalText, int sentenceLength)
{

    // Crée variable resultList
    list<string> resultList;

    string to_split = originalText;
    while (to_split.size() > sentenceLength)
    {
        // On doit découper
        // "a bcd de f"
        // ["a bcd", "de f"]
        auto splitPosition = to_split.find_last_of(' ', sentenceLength);

        // TODO : trouver la position du dernier espace <= 5 c'est à dire à la taille max
        // Ca devrait être 5

        // On ajoute la premiere partie
        //              "a bcd de f"
        //         0 ____|    |
        // "a bcd" 6 _________|

        //                                     0         6
        string firstPart = to_split.substr(0, splitPosition);
        resultList.push_back(firstPart);

        //                                         6 + 1               9
        to_split = to_split.substr(splitPosition + 1, to_split.size());
    }
    resultList.push_back(to_split);

    // On retourne la liste
    return resultList;
}
