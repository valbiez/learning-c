#ifndef MAP_UTILS_HPP
#define MAP_UTILS_HPP

#include <map>
#include <vector>

using namespace std;

// Utilitaire pour récupérer les clés d'une map
// source : https://www.lonecpluspluscoder.com/2015/08/13/an-elegant-way-to-extract-keys-from-a-c-map/
template<typename TK, typename TV>
std::vector<TK> extract_keys(std::map<TK, TV> const& input_map) {
  std::vector<TK> retval;
  for (auto const& element : input_map) {
    retval.push_back(element.first);
  }
  return retval;
}

#endif