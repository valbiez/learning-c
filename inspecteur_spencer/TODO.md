# Todo

- [X] Traduire les différent gagdets et comportements test par test
    - [X] continuer de traduire les gadgets
- [ ] Découper un texte en phrases plus petites
    - [X] Nouvelle suite de test pour le découpage des phrases : `make test-sent`
    - [X] Appris à travailler avec std::list
        - `list.push_back("word")` : ajoute un élément à la fin de la liste
        - `list.front()` : retourne le premier élément de la liste
        - `list.pop_front()` : supprime le premier élément de la liste
    - [X] Nouvelles fonctions sur les  `string`
        - `string.find_last_of(' ')` : dernière occurence d'un caractère dans une chaine
        - `string.find_first_of(' ')` : première occurence d'un caractère dans une chaine
        - `string.size()` : nombre de caractères de la chaine
        - `string.substr(0,1)` : retourne un sous-ensemble (plus petit) d'une chaine
    - [X] Découper une phrase en dessous de la limite
    - [X] Découper une phrase par espaces
    - [X] Découper une phrase avec un espace dans la première partie
    - [X] Découper une phrase avec un espace dans la première partie et dans la dernière partie
        -[X] essayer de trouver l'espace qui correspond au bon critère (voir commentaires dans le code de la fonction  `split(...)`)
    - Spencer est limité à des phrases au max de 130 caractères
    - Découper une phrase de plus de 130 caractères en plusieurs phrases de moins de 130 caractères
- [ ] Ecrire un main en ligne de commande pour tester interactivement le découpage d'une phrase
    - [X] Crée un fichier cpp pour y déplacer la fonction `split()`
        - [ ] Séparer la spécification de la déclaration
        - [ ] 
    - [X] Mettre à jour le makefile pour compiler ce `main-split.cpp`
    - [X] Avec des valeurs en dur (phrase à découper et taille max de la phrase)
    - [ ] Avec une phrase en paramètre et la taille max en dur (130)
    - traduire les tests et noms en anglais
- [ ] Permettre à Spencer de dire des phrases longues
    - Utiliser notre découpeur de phrases dans le programme spencer principal
        - [ ] récupérer la phrase de l'inspecteur gadget
        - [ ] découper la phrase
        - [ ] ajouter chaque bout de phrase au text to speech
        - [ ] Téléverser le programme dans le spencer et voilà!
- [ ] Analyser les phrases dites par l'utilisateur pour choisir les actions à déclencher
    - Si l'utilisateur dit "Go Go Gadget" -> gadget aléatoire
    - Si l'utilisateur dit "Go Go Gadgeto coat" -> gadget coat
    - Si l'utilisateur dit "Help me" -> le robot dit quelles sont les commandes disponibles pour l'inspecteur
        - "Go Go Gadget"
        - "Go Go Gadgeto " + gadget name
        - "Help me"

## Dire des phrases longues

Par ex avec une pharse a au max 20 caractères

```
12345678901234567890

moins de 20
->
moins de 20

12345678901234567890

plus de 20 caractères
->
plus de 20 
caractères

12345678901234567890
                   X
plus de 40 caractères ça fait plus de 2 phrases
-> 
plus de 40
caractères ça fait plus de 2 phrases

12345678901234567890
                   X
plus de 40 
caractères ça fait 
plus de 2 phrases
```

`MAX` : le nombre de caractère max d'une phrase (ex. MAX=20, ou encore MAX=130)

1. Un phrase n'est pas découpée si elle fait moins de `MAX` caractères
    - On retourne la même phrase
2. Une phrase est découpée en 2 phrases si elle fait plus de `MAX` caractères mais moins de `MAX * 2` caractères
    - On retourne la premiere moitié et la 2eme moitié
    - Le découpage se fait à partir de la position du dernier espace avant la position qui correspond à `MAX`
3. Une phrase très longue aura plus de 2 morceaux
    - On coupe en 2
        - à gauche : moins de `MAX` caractères
        - à droite : le reste
    - Tant que le reste (à droite) fait plus de `MAX` caractères on continue de découper

## Menu vocal

