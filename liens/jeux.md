Analogies possibles avec arduino
- Blocky bird : https://blockly.games/bird
    - boucle infinie implicite
    - Capteurs
        - Ver collecté
        - Nid atteint
    - Actions
        - Changer de cap (degré)
        - 
- Blocky maze : https://blockly.games/maze
    - boucle infinie explicite
    - A chaque "tick" de robot
        - Chaque action prend moins de temps qu'un "tick"
    - Capteurs
        - Type de chemin (devant)
        - Type de chemin (vers la gauche)
        - Type de chemin (vers la droite)
        - Arrivée atteinte
    - Actions
        - changer de direction (tourner à gauche)
        - changer de direction (tourner à droite)
        - Avancer devant

https://learn.grasshopper.app/project/fundamentals

http://www.toxicode.fr/learn

https://www.codewars.com/

https://codecombat.com/

https://architect.toxicode.fr/


Shenzhen i/o : https://store.steampowered.com/app/504210/SHENZHEN_IO/

Katas : https://kata-log.rocks
- tu peux copier coller le starter : dossier `kata-starter`