#include <iostream>
#include <string>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

using namespace std;

int64_t rpn(string input){
    std::istringstream iss(input);
    std::vector<std::string> tokens(
        (std::istream_iterator<std::string>(iss)),
        std::istream_iterator<std::string>());

    if (tokens.size() == 1) {
        return std::stoi(tokens[0]);
    } else {
        return std::stoi(tokens[0]) + std::stoi(tokens[1]);
    }

    return std::stoi(input);
}



