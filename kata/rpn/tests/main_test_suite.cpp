#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch_amalgamated.hpp"
#include "../lib.hpp"


TEST_CASE("RPN on literal", "[rpn]")
{
    SECTION("polop")
    {

        REQUIRE(rpn("5") == 5);
        REQUIRE(rpn("7") == 7);
    }
}

TEST_CASE("RPN with addition", "[rpn]")
{   
    SECTION("polop")
    {

        REQUIRE(rpn("1 1 +") == 2);
        REQUIRE(rpn("100 13 +") == 113);
    }
}