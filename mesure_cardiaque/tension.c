#include "tension.h"

struct tension new_mesure(int systolique, int diastolique)
{
    struct tension mesure;
    mesure.systolique = systolique;
    mesure.diastolique = diastolique;
    return mesure;
}

bool mesure_normale(struct tension mesure)
{
    if (mesure.systolique < 100)
    {
        return false;
    }

    if (mesure.systolique > 134)
    {
        return false;
    }
    
    if (mesure.diastolique < 60)
    {
        return false;
    }

    if (mesure.diastolique > 89)
    {
        return false;
    }


    return true;
}
