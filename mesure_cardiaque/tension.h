#ifndef TENSION_H
#define TENSION_H
#include <stdbool.h>

/**
 * Structure pour représenter une mesure de tension.
 */
struct tension
{
    int systolique;
    int diastolique;
};

struct tension new_mesure(int systolique, int diastolique);

bool mesure_normale(struct tension mesure);

#endif