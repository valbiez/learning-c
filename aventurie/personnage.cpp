#include <iostream>
#include "personnage.hpp"

struct personnage personnage(std::string nom, std::string sexe, std::string type, int courage, int intelligence, int charisme, int addresse, int force, int protection, struct lancer point_impact)
{
    struct personnage personnage;
    personnage.nom = nom;
    personnage.sexe = sexe;
    personnage.type = type;
    personnage.niveau = 1;

    personnage.courage = courage;
    personnage.intelligence = intelligence;
    personnage.charisme = charisme;
    personnage.addresse = addresse;
    personnage.force = force;

    personnage.energie_vitale = 30;
    personnage.energie_astrale = 0;

    personnage.attaque = 10;
    personnage.parade = 8;

    personnage.protection = protection;
    personnage.point_impact = point_impact;

    return personnage;
}

struct personnage aventurier(std::string nom, std::string sexe)
{
    return personnage(
        nom, sexe, "Aventurier",
        n_de_mod(1, 6, 7),
        n_de_mod(1, 6, 7),
        n_de_mod(1, 6, 7),
        n_de_mod(1, 6, 7),
        n_de_mod(1, 6, 7),
        2,
        d(1, 6, 2));
}

struct personnage guerrier(char *nom, char *sexe)
{
    return personnage(nom, sexe, "Guerrier", n_de_mod(1, 6, 7),
                      n_de_mod(1, 6, 7),
                      n_de_mod(1, 6, 7),
                      n_de_mod(1, 6, 7),
                      n_de_mod(1, 6, 7),
                      4,
                      d(1, 6, 4));
}

void affiche_personnage(struct personnage fiche)
{
    std::cout << "Nom : " << fiche.nom << std::endl;
    std::cout << "Sex : " << fiche.sexe << std::endl;
    std::cout << "Type : " << fiche.type << std::endl;
    std::cout << "Niveau : " << fiche.niveau << std::endl;

    std::cout << std::endl;

    std::cout << "Courage : " << fiche.courage << std::endl;
    std::cout << "Intelligence : " << fiche.intelligence << std::endl;
    std::cout << "Charisme : " << fiche.charisme << std::endl;
    std::cout << "Addresse : " << fiche.addresse << std::endl;
    std::cout << "Force : " << fiche.force << std::endl;

    std::cout << std::endl;

    std::cout << "Energie vitale : " << fiche.energie_vitale << std::endl;
    std::cout << "Energie astrale : " << fiche.energie_astrale << std::endl;

    std::cout << std::endl;

    std::cout << "Attaque : " << fiche.attaque << std::endl;
    std::cout << "Parade : " << fiche.parade << std::endl;

    std::cout << "Protection : " << fiche.protection << std::endl;
    std::cout << "Impact: " << display(fiche.point_impact) << std::endl;
}
