#include <iostream>

#include "combat.hpp"
#include "de.hpp"

int phase_P_vs_M(struct personnage *personnage, struct monstre *monstre)
{
    if (de(20) <= personnage->attaque)
    {
        std::cout << "Paf" << std::endl;
        if (de(20) <= monstre->parade)
        {
            std::cout << "Clink" << std::endl;
        }
        else
        {
            int degats = std::max(0, roll(personnage->point_impact) - monstre->protection);
            std::cout << "Aie : " << degats << std::endl;
            monstre->energie_vitale -= degats;
        }
    }
    else
    {
        std::cout << "Raté" << std::endl;
    }
    return 0;
}

int phase_M_vs_P(struct personnage *personnage, struct monstre *monstre)
{
    if (de(20) <= monstre->attaque)
    {
        std::cout << "Paf" << std::endl;
        if (de(20) <= personnage->parade)
        {
            std::cout << "Clink" << std::endl;
        }
        else
        {
            int degats = std::max(0, roll(monstre->point_impact) - personnage->protection);
            std::cout << "Aie : " << degats << std::endl;
            personnage->energie_vitale -= degats;
        }
    }
    else
    {
        std::cout << "Raté" << std::endl;
    }
    return 0;
}

int assaut(struct personnage *personnage, struct monstre *monstre)
{
    phase_P_vs_M(personnage, monstre);
    phase_M_vs_P(personnage, monstre);

    return 0;
}
