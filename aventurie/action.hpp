


class scenario;

class game
{
    scenario &_scenario;
    std::string _scene;

public:
    game(scenario &scenario)
        : _scenario(scenario)
    {
        _scene = "001";
    }

    void moveto(std::string scene)
    {
        _scene = scene;
    }
};

class action
{
    virtual void process(game &) = 0;
};

class moveto : action
{
    std::string _scene;

public:
    moveto(std::string scene)
        : _scene(scene) {}

    virtual void process(game &game)
    {
        game.moveto(_scene);
    }
};
