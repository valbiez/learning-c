#ifndef MONSTRE_H
#define MONSTRE_H

#include <string>
#include "de.hpp"

struct monstre
{
    std::string nom;
    std::string type;

    int niveau;

    int courage;

    int energie_vitale;
    int energie_astrale;

    int attaque;
    int parade;

    int protection;
    struct lancer point_impact;
};

struct monstre monstre(std::string nom, std::string type, int niveau, int courage, int energie_vitale, int energie_astrale, int attaque, int parade, int protection, struct lancer point_impact);
struct monstre goblin(std::string nom);

void affiche_monstre(struct monstre monstre);

#endif