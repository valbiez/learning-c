#include <stdlib.h>
#include <time.h>
#include "de.hpp"

void init()
{
    srand(time(NULL));
}

struct lancer d(int nombre, int faces, int modificateur)
{
    struct lancer result;
    result.nombre = nombre;
    result.faces = faces;
    result.modificateur = modificateur;
    return result;
}

int de(int faces)
{
    return rand() % faces + 1;
}

int n_de(int nombre, int faces)
{
    int resultat = 0;
    while (nombre > 0)
    {
        resultat += de(faces);
        --nombre;
    }
    return resultat;
}

int n_de_mod(int nombre, int faces, int modificateur)
{
    return n_de(nombre, faces) + modificateur;
}

int roll(struct lancer lancer)
{
    return n_de_mod(lancer.nombre, lancer.faces, lancer.modificateur);
}

std::string display(struct lancer lancer)
{
    std::string result = std::to_string(lancer.nombre) + "d" + std::to_string(lancer.faces);
    if (lancer.modificateur > 0)
    {
        return result + "+" + std::to_string(lancer.modificateur);
    }
    if (lancer.modificateur < 0)
    {
        return result + std::to_string(lancer.modificateur);
    }
    return result;
}
