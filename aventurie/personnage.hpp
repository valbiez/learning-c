#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include <string>
#include "de.hpp"

struct personnage
{
    std::string nom;
    std::string sexe;
    std::string type;

    int niveau;

    int courage;
    int intelligence;
    int charisme;
    int addresse;
    int force;

    int energie_vitale;
    int energie_astrale;

    int attaque;
    int parade;

    int protection;
    struct lancer point_impact;
};

struct personnage personnage(std::string nom, std::string sexe, std::string type, int courage, int intelligence, int charisme, int addresse, int force, int protection, struct lancer point_impact);
struct personnage aventurier(std::string nom, std::string sexe);
struct personnage guerrier(std::string nom, std::string sexe);
void affiche_personnage(struct personnage fiche);

#endif