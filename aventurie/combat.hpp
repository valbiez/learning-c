#ifndef COMBAT_H
#define COMBAT_H

#include "monstre.hpp"
#include "personnage.hpp"

int assaut(struct personnage *personnage, struct monstre *monstre);

#endif