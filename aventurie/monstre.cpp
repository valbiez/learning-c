#include <iostream>

#include "monstre.hpp"
#include "de.hpp"

struct monstre monstre(std::string nom, std::string type, int niveau, int courage, int energie_vitale, int energie_astrale, int attaque, int parade, int protection, struct lancer point_impact)
{
    struct monstre monstre;
    monstre.nom = nom;
    monstre.type = type;
    monstre.niveau = niveau;
    monstre.courage = courage;

    monstre.energie_vitale = energie_vitale;
    monstre.energie_astrale = energie_astrale;
    monstre.attaque = attaque;
    monstre.parade = parade;

    monstre.protection = protection;
    monstre.point_impact = point_impact;

    return monstre;
}

void affiche_monstre(struct monstre fiche)
{
    std::cout << "Nom : " << fiche.nom << std::endl;
    std::cout << "Type : " << fiche.type << std::endl;
    std::cout << "Niveau : " << fiche.niveau << std::endl;

    std::cout << std::endl;

    std::cout << "Courage : " << fiche.courage << std::endl;

    std::cout << std::endl;

    std::cout << "Energie vitale : " << fiche.energie_vitale << std::endl;
    std::cout << "Energie astrale : " << fiche.energie_astrale << std::endl;

    std::cout << std::endl;

    std::cout << "Attaque : " << fiche.attaque << std::endl;
    std::cout << "Parade : " << fiche.parade << std::endl;
    std::cout << "Protection : " << fiche.protection << std::endl;
    std::cout << "Impact: " << display(fiche.point_impact) << std::endl;
}

struct monstre goblin(std::string nom)
{
    return monstre(nom, "Gobelin", 1, 5, 15, 0, 9, 7, 2, d(1, 6, 4));
}
