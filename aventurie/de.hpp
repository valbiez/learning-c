#ifndef DE_H
#define DE_H

#include <string>

struct lancer
{
    int nombre;
    int faces;
    int modificateur;
};

struct lancer d(int nombre, int faces, int modificateur);

void init();

int de(int faces);
int n_de(int nombre, int faces);
int n_de_mod(int nombre, int faces, int modificateur);

int roll(struct lancer lancer);

std::string display(struct lancer lancer);

#endif