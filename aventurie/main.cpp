#include <iostream>

#include "personnage.hpp"
#include "monstre.hpp"
#include "de.hpp"
#include "combat.hpp"
#include "scenario.hpp"

int main()
{
    init();

    struct personnage Alois = aventurier("Alois", "Female");
    affiche_personnage(Alois);

    struct monstre huru = goblin("Huru");
    affiche_monstre(huru);

    assaut(&Alois, &huru);

    scenario toto = scenario("001", "002");
    toto
        .add_scene(
            scene("001")
                .add_description(description("Vous êtes dans une pièce avec une porte", ""))
                .add_option(option("", "Ouvrir la porte", "GOTO:002")))
        .add_scene(
            scene("002")
                .add_description(description("Vous êtes dehors, Victoire!", "")));

    std::cout << toto.situation() << std::endl;

    return 0;
}