#ifndef LIEUX_H
#define LIEUX_H

#include <string>
#include <list>


class description
{
    std::string _text;
    std::string _condition;

public:
    description(std::string text, std::string condition = "")
        : _text(text), _condition(condition) {}
};

class option
{
    std::string _condition;
    std::string _text;
    std::string _action;

public:
    option(std::string text, std::string action, std::string condition = "")
        : _condition(condition), _text(text), _action(action) {}
};

struct scene
{
    std::string _identifier;
    std::list<description> _descriptions;
    std::list<option> _options;

public:
    scene(std::string identifier)
        : _identifier(identifier), _descriptions(), _options() {}

    scene &add_description(description item)
    {
        _descriptions.push_back(item);
        return *this;
    }

    scene &add_option(option item)
    {
        _options.push_back(item);
        return *this;
    }
};

class scenario
{
    std::string _initial;
    std::string _terminal;
    std::list<scene> _scenes;

public:
    scenario(std::string initial, std::string terminal)
        : _initial(initial), _terminal(terminal), _scenes() {}

    scenario &add_scene(scene item)
    {
        _scenes.push_back(item);
        return *this;
    }

    std::string situation();
};

#endif