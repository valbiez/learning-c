# Inspecteur Gadget - Orienté Objet

Starter C++ pour commencer des Kata de code.
Copier coller ce dossier prêt à l'emploi.

- Le principe des kata : https://apprendre-la-programmation.net/kata-code-programmation/
- https://kata-log.rocks/starter

## Tests automatisés

Lancer les tests

```bash
make test
```

