#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch_amalgamated.hpp"
#include "../lib.hpp"


TEST_CASE("Test initial", "[kata]")
{
    
    SECTION("Hello world")
    {

        REQUIRE(hello("world") == "Hello world");
         REQUIRE(fizzBuzz(1) == "1");
    }
}