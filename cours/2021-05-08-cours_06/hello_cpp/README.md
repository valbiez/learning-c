# Hello C++

## Compiler, assembler et exécuter

Compiler le programme

```
g++ Hello.cpp
```

Exécuter le programme

```bash
./a.out
```


## Compiler sans assembler ou lier

```
g++ -S Hello.cpp
```

Un fichier `Hello.s` est généré. C'est du code machine assembleur.

## Compiler et assembler sans lier


Compiler et assembler le programme sans lier, et donc sans créer de fichier exécutable.

```
g++ -c Hello.cpp
```

Un fichier `Hello.out` est généré

Si on essaye de l'exécuter

```bash
./Hello.out
```

Erreur, le fichier n'est pas exécutable

```
permission denied: ./Hello.o
```

Le fichier généré n'est pas un exécutable, il faut l'assembler pour en faire un exécutable.

## Compiler, assembler et lier

Compiler, assembler et lier le code pour en faire un fichier exécutable.

```
g++ -o hello Hello.cpp
```

Exécuter le programme

```
g++ -o hello Hello.cpp
```