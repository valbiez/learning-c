
#include <iostream> 
#include <cstdlib>
#include <ctime>
#include <string>
  
using namespace std;

// prototypes
void goGoGadgeto(string nomDuGadget);  
void goGoGadgetoGrappin();
void goGoGadgetoManteau();
void goGoGadgetoAleatoire();
int nombreAleatoire(int nombreMax);

// fonctions

int main(int argc, char *argv[])
{
    
    if(argc == 2){
        // l'utilisateur a saisi le nom d'un gadget
        string nomDuGadget =  argv[1];
        goGoGadgeto(nomDuGadget);
    }else{
        
        cout << "Lancement de gadget aléatoire!" << endl;
        goGoGadgetoAleatoire();
    }
      
    return 0;
}

void goGoGadgeto(string nomDuGadget){
    if(nomDuGadget.compare("grappin")==0){
        goGoGadgetoGrappin();
    }else if(nomDuGadget.compare("manteau")==0){
        goGoGadgetoManteau();
    }else{
        cout<<"Gadget non installé!"<<endl;
        cout<<"L'inspecteur se casse la gueule"<<endl;

    }
}

void goGoGadgetoGrappin(){
    cout<<"Go Go Gadgeto grappin! "<<endl;
}


void goGoGadgetoManteau(){
    cout<<"Go Go Gadgeto manteau!  le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule."<<endl;
}


void goGoGadgetoAleatoire(){

    switch (nombreAleatoire(1))
    {
    case 0:
        goGoGadgetoGrappin();
        break;
    case 1: 
        goGoGadgetoManteau();
    default:
        break;
    }
}

int nombreAleatoire(int nombreMax){
    std::srand(std::time(nullptr));
    int randomNumber = std::rand();
    
    return randomNumber % (nombreMax+1);
}