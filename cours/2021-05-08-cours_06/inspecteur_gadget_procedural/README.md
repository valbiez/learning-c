# Inspecteur Gadget - Procédural - C++

## Concepts de ce cours

- 
- classe
    - attribut
    - méthode
    - constructeur
- objet
- interface

## Hello C++!

```cpp
//: C02:Hello.cpp
// Dire bonjour en C++
#include <iostream> // Déclaration des flux
using namespace std;
 
int main() {
  cout << "Bonjour tout le monde ! J'ai "
       << 8 << " ans aujourd'hui !" << endl;
} ///:~
```