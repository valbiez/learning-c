#ifndef GADGETS_INSTALLES_HPP
#define GADGETS_INSTALLES_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "Gadget.hpp"
#include "GadgetInvente.hpp"
#include "GadgetNonInstalle.hpp"

using namespace std;

// Représente les Gadgets installés sur un robot (par exemple sur un inspecteur gadget)
class EmplacementPourGadgets
{
public:
    void installer(string nomDuGadget)
    {
        if (nomDuGadget.compare("manteau") == 0)
        {
            Gadget *gadget = creerGadget(
                nomDuGadget,
                "le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");

            gadgets[nomDuGadget] = gadget;
            cout << "Le gadget " << nomDuGadget << " est installé!" << endl;
        }
        else if (nomDuGadget.compare("grappin") == 0)
        {
            Gadget *gadget = creerGadget(
                nomDuGadget,
                "il attrape un voleur avec son grappin!");
            gadgets[nomDuGadget] = gadget;
            cout << "Le gadget " << nomDuGadget << " est installé!" << endl;
        }
        else
        {
            cout << "Le gadget " << nomDuGadget << " n'a pas été inventé! Installation impossible!" << endl;
        }
    }

    void activer(string nomDuGadget)
    {
        Gadget *gadget = recupereGadget(nomDuGadget);
        gadget->activer();
    }

    void activerAleatoirement()
    {
        if (nombreDeGadgetsInstalles() == 0)
        {
            return;
        }
        recupererGadgetAleatoirement()->activer();
    }

    // Constructeur (allocation dynamique de mémoire)
    EmplacementPourGadgets()
    {
        // initialisation d'une seed basée sur la date courante
        // pour le générateur de nombres aléatoires
        std::srand(std::time(nullptr));
        gadgetNonInstalle = new GadgetNonInstalle();
    }

    // Destructeur (désallocation de mémoire)
    ~EmplacementPourGadgets()
    {
        delete gadgetNonInstalle;
    }

private:

    Gadget *creerGadget(string nom, string action)
    {
        GadgetInvente *gadgetInvente = new GadgetInvente();
        gadgetInvente->nom = nom;
        gadgetInvente->action = action;
        return gadgetInvente;
    }

    Gadget *recupereGadget(string nomDuGadget)
    {
        std::map<string, Gadget *>::iterator search = gadgets.find(nomDuGadget);

        // si le gadget est installé
        if (search != gadgets.end())
        {
            return gadgets[nomDuGadget];
        }
        // sinon on retourne le gadget qui indique que le gadget demandé n'est pas installé
        return gadgetNonInstalle;
    }
    
    Gadget * recupererGadgetAleatoirement(){
                int gadgetAleatoirementSelectionne = nombreAleatoire(nombreDeGadgetsInstalles());
        
        auto iteratorFromBeginning = gadgets.begin();
        for (int i = 0; i < gadgetAleatoirementSelectionne; i++)
        {
            ++iteratorFromBeginning;
        }

        Gadget *gadget = iteratorFromBeginning->second;
        return gadget;
    }

    int nombreDeGadgetsInstalles()
    {
        return gadgets.size();
    }

    int nombreAleatoire(int nombreMax)
    {
        int randomNumber = std::rand();
        return randomNumber % (nombreMax);
    }

private:
    // liste des gadgets, on voudrait pouvoir accéder à un gadget par son nom
    std::map<string, Gadget *> gadgets;
    Gadget *gadgetNonInstalle;
};

#endif