#ifndef GADGET_NON_INSTALLE_HPP
#define GADGET_NON_INSTALLE_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "Gadget.hpp"

using namespace std;

// Cas particulier lorsque le gadget n'est pas installé sur l'inspecteur Gadget
// https://martinfowler.com/eaaCatalog/specialCase.html
class GadgetNonInstalle : public Gadget
{
public:
    virtual void activer()
    {
        cout << "L'inspecteur se casse la gueule!" << endl;
    }
};

#endif