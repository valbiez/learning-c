#ifndef GADGET_H
#define GADGET_H

// Gadget est une classe abstraite dont toutes les méthodes sont virtual pure
// On peut considérer que c'est une interface
class Gadget
{
public:
    // Le mot clé  `virtual` indique que la méthode peut être surchargée
    // =0 indique que la méthode devra obligatoirement être surchargée dans les classes filles
    virtual void activer() = 0;
};

#endif