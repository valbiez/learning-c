#ifndef GADGET_INVENTE_HPP
#define GADGET_INVENTE_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "Gadget.hpp"

using namespace std;

// Représente un gadget qui a deja été inventé
class GadgetInvente : public Gadget
{
public:
    string nom;
    string action;
    virtual void activer()
    {
        cout << "Go Go Gadgeto " << nom << "! " << action << endl;
    }
};

#endif