#include <iostream>
#include "InspecteurGadget.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    cout << endl
         << endl;
    InspecteurGadget inspecteur;

    inspecteur.installer("manteau");
    // Le gadget manteau installé!
    inspecteur.installer("grappin");
    //Le gadjet grappin installé!

    cout << endl
         << endl;

    inspecteur.goGoGadgeto("truc");
    // L'inspecteur se casse la gueule

    cout << endl
         << endl;

    inspecteur.goGoGadgeto("manteau");
    // Go Go Gadgeto manteau! le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.

    cout << endl
         << endl;

    inspecteur.goGoGadgeto("grappin");
    // Go Go Gadgeto grappin! il attrape un voleur avec son grappin!

    cout << endl
         << endl;

    inspecteur.goGoGadgetoAleatoire();
    inspecteur.goGoGadgetoAleatoire();
    inspecteur.goGoGadgetoAleatoire();
    inspecteur.goGoGadgetoAleatoire();
    inspecteur.goGoGadgetoAleatoire();

    return 0;
}
