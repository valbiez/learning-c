#ifndef INSPECTEUR_GADGET_HPP
#define INSPECTEUR_GADGET_HPP

#include "GadgetsInstalles.hpp"

using namespace std;


class InspecteurGadget
{
public:

    void installer(string nomDuGadget)
    {
        gadgetsInstalles.installer(nomDuGadget);
    }

    void goGoGadgeto(string nomDuGadget)
    {
        gadgetsInstalles.activer(nomDuGadget);
    }

    void goGoGadgetoAleatoire()
    {
        gadgetsInstalles.activerAleatoirement();
    }

private:
    GadgetsInstalles gadgetsInstalles;
};

#endif