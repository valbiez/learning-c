# Hello C++

## Compiler plusiers fichiers

Compiler et assembler plusieurs fichiers sans les lier.

```bash
g++ -c helloWorld.cpp hello.cpp
```

Un fichier `helloWorld.o` et un fichier `hello.o` sont créés.
Ces fichiers contiennent le **code objet**.


## Lier les codes objets en un exécutable

Lier les différents **codes objet** sous la forme d'un exécutable.

```bash
g++ -o main helloWorld.o hello.o
```

Exécuter le programme.

```bash
./main
```

