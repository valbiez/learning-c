#ifndef GADGET_NON_INVENTE_HPP
#define GADGET_NON_INVENTE_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "Gadget.hpp"

// Cas particulier lorsque le gadget n'est pas inventé
// https://martinfowler.com/eaaCatalog/specialCase.html
class GadgetNonInvente : public Gadget
{
public:
    
    virtual string activer()
    {
        return "Le gadget " + nom + " n'a pas été inventé!";
    }

    virtual string getNom()
    {
        return nom;
    }
       virtual string getType()
    {
        return "GadgetNonInvente";
    }
    GadgetNonInvente(string p_nom)
    {
        nom = p_nom;
    }
    private:
    string nom;
};

#endif