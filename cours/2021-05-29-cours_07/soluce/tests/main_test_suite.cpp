#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch_amalgamated.hpp"

#include "../InspecteurGadget.hpp"

TEST_CASE("L'inspecteur Gadget peut installer et utiliser des gadgets", "[inspecteurGadget]")
{
    InspecteurGadget inspecteur;
    SECTION("aucun gadget n'est installé par défaut")
    {
        REQUIRE(inspecteur.goGoGadgeto("manteau") == "Le gadget manteau n'est pas installé. L'inspecteur se casse la gueule!");
        REQUIRE(inspecteur.goGoGadgeto("grappin") == "Le gadget grappin n'est pas installé. L'inspecteur se casse la gueule!");
        REQUIRE(inspecteur.goGoGadgeto("truc non inventé") == "Le gadget truc non inventé n'est pas installé. L'inspecteur se casse la gueule!");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Aucun gadget installé! L'inspecteur se casse la gueule!");
    }
    SECTION("installer un manteau permet d'activer le manteau")
    {
        REQUIRE(inspecteur.installer("manteau") == "Le gadget manteau est installé!");
        REQUIRE(inspecteur.goGoGadgeto("manteau") == "Go Go Gadgeto manteau! le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Go Go Gadgeto manteau! le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");
    }

    SECTION("installer un grappin permet d'activer le grappin")
    {
    
        REQUIRE(inspecteur.installer("grappin") == "Le gadget grappin est installé!");
        REQUIRE(inspecteur.goGoGadgeto("grappin") == "Go Go Gadgeto grappin! il attrape un voleur avec son grappin!");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Go Go Gadgeto grappin! il attrape un voleur avec son grappin!");
    }

    SECTION("installer gadget qui n'a pas encore été inventé est impossible")
    {
        REQUIRE(inspecteur.installer("turbo smartphone") == "Le gadget turbo smartphone n'a pas été inventé! Installation impossible!");
    }

    // On est limité au manteau et au grappin. Peut on inventer d'autres gadget et les installer?
    //
}

TEST_CASE("On peut inventer des gadgets et fournir cette liste à l'inspecteur", "[inspecteurGadget]")
{
    GadgetsInventes *gadgetsInventes = new GadgetsInventes();

    SECTION("installer gadget qui n'a pas encore été inventé est impossible")
    {
        InspecteurGadget inspecteur = InspecteurGadget(*gadgetsInventes);
        REQUIRE(inspecteur.installer("turbo smartphone") == "Le gadget turbo smartphone n'a pas été inventé! Installation impossible!");
    }

    SECTION("installer gadget qui a été inventé est possible")
    {
        REQUIRE(gadgetsInventes->inventer("turbo smartphone", "action gadget") == "Le gadget turbo smartphone est inventé!");
        InspecteurGadget inspecteur = InspecteurGadget(*gadgetsInventes);
        REQUIRE(inspecteur.installer("turbo smartphone") == "Le gadget turbo smartphone est installé!");
    }

    SECTION("Les gadgets standard ne sont pas présents si on founit des gadgets inventés")
    {
        InspecteurGadget inspecteur = InspecteurGadget(*gadgetsInventes);
        REQUIRE(inspecteur.installer("manteau") == "Le gadget manteau n'a pas été inventé! Installation impossible!");
    }
}

TEST_CASE("L'inspecteur peut lister les gadget installés", "[inspecteurGadget]")
{
    SECTION("aucun gadget n'est installé par défaut")
    {

        InspecteurGadget inspecteur;
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{}));
    }

    SECTION("1 gadget installé")
    {
        InspecteurGadget inspecteur;
        inspecteur.installer("manteau");
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau"}));
    }
    SECTION("2 gadgets installés")
    {

        InspecteurGadget inspecteur;
        inspecteur.installer("manteau");
        inspecteur.installer("grappin");
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau", "grappin"}));
    }
}

TEST_CASE("L'inspecteur peut lister les gadget pouvant être installés", "[inspecteurGadget]")
{
    SECTION("par défaut le manteau et le grappin sont installables")
    {
        InspecteurGadget inspecteur;
        auto liste = inspecteur.listeGadgetsInstallables();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau", "grappin"}));
    }

    SECTION("aucun gadget n'est installable")
    {
        InspecteurGadget inspecteur = InspecteurGadget(GadgetsInventes());
        auto liste = inspecteur.listeGadgetsInstallables();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{}));
    }

}