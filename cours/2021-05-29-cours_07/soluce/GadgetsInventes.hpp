#ifndef GADGETS_INVENTES_HPP
#define GADGETS_INVENTES_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "Gadget.hpp"
#include "GadgetInvente.hpp"
#include "GadgetNonInvente.hpp"
#include "map_utils.hpp"

using namespace std;

// Représente les Gadgets inventés. Ils peuvent être installés sur un robot
class GadgetsInventes
{
public:
    string inventer(string nomDuGadget, string action)
    {
        Gadget *gadget = inventerGadget(nomDuGadget, action);
        gadgets[nomDuGadget] = gadget;
        return "Le gadget " + nomDuGadget + " est inventé!";
    }

    Gadget *creerGadget(string nomDuGadget)
    {
        std::map<string, Gadget *>::iterator search = gadgets.find(nomDuGadget);

        // si le gadget est inventé
        if (search != gadgets.end())
        {
            return gadgets[nomDuGadget];
        }
        // sinon on retourne le gadget qui indique que le gadget demandé n'est pas inventé
        return new GadgetNonInvente(nomDuGadget);
    }

    vector<string> liste()
    {
        return extract_keys(gadgets);
    }
    // Constructeur (allocation dynamique de mémoire)
    GadgetsInventes()
    {
    }

    // Constructeur par recopie
    GadgetsInventes(const GadgetsInventes &obj)
    {
        gadgets = std::map<string, Gadget *>(obj.gadgets);
    }

private:
    Gadget *inventerGadget(string nom, string action)
    {
        return new GadgetInvente(nom, action);
    }

private:
    // liste des gadgets, on voudrait pouvoir accéder à un gadget par son nom
    std::map<string, Gadget *> gadgets;
};

#endif