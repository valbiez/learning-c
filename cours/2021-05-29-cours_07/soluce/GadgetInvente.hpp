#ifndef GADGET_INVENTE_HPP
#define GADGET_INVENTE_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "Gadget.hpp"

using namespace std;

// Représente un gadget qui a deja été inventé
class GadgetInvente : public Gadget
{
public:
    virtual string activer()
    {
        return "Go Go Gadgeto " + nom + "! " + action;
    }
    virtual string getNom()
    {
        return nom;
    }
   virtual string getType()
    {
        return "GadgetInvente";
    }
    GadgetInvente(string p_nom, string p_action)
    {
        nom = p_nom;
        action = p_action;
    }

private:
    string nom;
    string action;
};

#endif