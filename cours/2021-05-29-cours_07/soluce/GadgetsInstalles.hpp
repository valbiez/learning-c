#ifndef GADGETS_INSTALLES_HPP
#define GADGETS_INSTALLES_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "Gadget.hpp"
#include "GadgetInvente.hpp"
#include "GadgetNonInvente.hpp"
#include "GadgetNonInstalle.hpp"
#include "map_utils.hpp"

using namespace std;

// Représente les Gadgets installés sur un robot (par exemple sur un inspecteur gadget)
class EmplacementPourGadgets
{
public:
    string installer(Gadget *const gadget)
    {
        string nom = gadget->getNom();
        if (gadget->getType() == "GadgetNonInvente")
        {

            return "Le gadget " + nom + " n'a pas été inventé! Installation impossible!";
        }

        gadgets[nom] = gadget;
        return "Le gadget " + nom + " est installé!";
    }

    string activer(string nomDuGadget)
    {
        Gadget *gadget = recupereGadget(nomDuGadget);
        return gadget->activer();
    }

    string activerAleatoirement()
    {
        if (nombreDeGadgetsInstalles() == 0)
        {
            return "Aucun gadget installé! L'inspecteur se casse la gueule!";
        }
        return recupererGadgetAleatoirement()->activer();
    }

    vector<string> liste()
    {
        return extract_keys(gadgets);
    }

    // Constructeur (allocation dynamique de mémoire)
    EmplacementPourGadgets()
    {
        // initialisation d'une seed basée sur la date courante
        // pour le générateur de nombres aléatoires
        std::srand(std::time(nullptr));
    }

private:
    Gadget *recupereGadget(string nomDuGadget)
    {
        std::map<string, Gadget *>::iterator search = gadgets.find(nomDuGadget);

        // si le gadget est installé
        if (search != gadgets.end())
        {
            return gadgets[nomDuGadget];
        }
        // sinon on retourne le gadget qui indique que le gadget demandé n'est pas installé
        return new GadgetNonInstalle(nomDuGadget);
    }

    Gadget *recupererGadgetAleatoirement()
    {
        int gadgetAleatoirementSelectionne = nombreAleatoire(nombreDeGadgetsInstalles());

        auto iteratorFromBeginning = gadgets.begin();
        for (int i = 0; i < gadgetAleatoirementSelectionne; i++)
        {
            ++iteratorFromBeginning;
        }

        Gadget *gadget = iteratorFromBeginning->second;
        return gadget;
    }

    int nombreDeGadgetsInstalles()
    {
        return gadgets.size();
    }

    int nombreAleatoire(int nombreMax)
    {
        int randomNumber = std::rand();
        return randomNumber % (nombreMax);
    }

private:
    // liste des gadgets, on voudrait pouvoir accéder à un gadget par son nom
    std::map<string, Gadget *> gadgets;
};

#endif