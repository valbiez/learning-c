#ifndef INSPECTEUR_GADGET_HPP
#define INSPECTEUR_GADGET_HPP

#include "GadgetsInstalles.hpp"
#include "GadgetsInventes.hpp"

using namespace std;

class InspecteurGadget
{
public:
    string installer(string nomDuGadget)
    {
        Gadget *gadget = gadgetsInventes->creerGadget(nomDuGadget);
        return gadgetsInstalles.installer(gadget);
    }

    string goGoGadgeto(string nomDuGadget)
    {
        return gadgetsInstalles.activer(nomDuGadget);
    }

    string goGoGadgetoAleatoire()
    {
        return gadgetsInstalles.activerAleatoirement();
    }

    vector<string> listeGadgetsInstalles(){
        return gadgetsInstalles.liste();
    }

vector<string> listeGadgetsInstallables(){
        return gadgetsInventes->liste();
}
    // Constructeur par défaut (allocation dynamique de mémoire)
    // Ce sont les gadgets inventés connus sur la gamme standard
    InspecteurGadget()
    {
        gadgetsInventes = new GadgetsInventes();
        gadgetsInventes->inventer("manteau","le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");
        gadgetsInventes->inventer("grappin","il attrape un voleur avec son grappin!");

    }
    // Constructeur (allocation dynamique de mémoire)
    InspecteurGadget(const GadgetsInventes &p_gadgetsInventes)
    {
        gadgetsInventes = new GadgetsInventes(p_gadgetsInventes);
    }

private:
    EmplacementPourGadgets gadgetsInstalles;
    GadgetsInventes *gadgetsInventes;
};

#endif