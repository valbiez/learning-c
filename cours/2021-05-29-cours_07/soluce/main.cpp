#include <iostream>
#include "InspecteurGadget.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    cout << endl
         << endl;
    InspecteurGadget inspecteur;

    cout << inspecteur.installer("manteau") << endl;
    // Le gadget manteau installé!
    cout << inspecteur.installer("grappin") << endl;
    //Le gadjet grappin installé!

    cout << endl
         << endl;

    cout << inspecteur.goGoGadgeto("truc") << endl;
    // L'inspecteur se casse la gueule

    cout << endl
         << endl;

   cout << inspecteur.goGoGadgeto("manteau") << endl;
    // Go Go Gadgeto manteau! le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.

    cout << endl
         << endl;

    cout << inspecteur.goGoGadgeto("grappin") << endl;
    // Go Go Gadgeto grappin! il attrape un voleur avec son grappin!

    cout << endl
         << endl;

    cout << inspecteur.goGoGadgetoAleatoire() << endl;
    cout << inspecteur.goGoGadgetoAleatoire() << endl;
    cout << inspecteur.goGoGadgetoAleatoire() << endl;
    cout << inspecteur.goGoGadgetoAleatoire() << endl;
    cout << inspecteur.goGoGadgetoAleatoire() << endl;

    return 0;
}
