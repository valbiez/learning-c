#ifndef GADGET_NON_INSTALLE_HPP
#define GADGET_NON_INSTALLE_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "Gadget.hpp"

using namespace std;

// Cas particulier lorsque le gadget n'est pas installé sur l'inspecteur Gadget
// https://martinfowler.com/eaaCatalog/specialCase.html
class GadgetNonInstalle : public Gadget
{

public:

    virtual string activer()
    {
        return "Le gadget " + nom + " n'est pas installé. L'inspecteur se casse la gueule!";
    }
    virtual string getNom()
    {
        return nom;
    }
       virtual string getType()
    {
        return "GadgetNonInstalle";
    }

    GadgetNonInstalle(string p_nom)
    {
        nom = p_nom;
    }

private:
    string nom;
};

#endif