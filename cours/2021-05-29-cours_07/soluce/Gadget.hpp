#ifndef GADGET_H
#define GADGET_H
#include <string>

using namespace std;

// Gadget est une classe abstraite dont toutes les méthodes sont virtual pure
// On peut considérer que c'est une interface
class Gadget
{
public:
    // Le mot clé  `virtual` indique que la méthode peut être surchargée
    // =0 indique que la méthode devra obligatoirement être surchargée dans les classes filles
    virtual string activer() = 0;
    virtual string getNom() = 0;
    virtual string getType() = 0;
};

#endif