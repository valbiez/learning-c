#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include "catch_amalgamated.hpp"

#include "../InspecteurGadget.hpp"
#include "../Divers/EmplacementDivers.hpp"
#include "../Divers/GadgetManteau.hpp"
#include "../Chapeau/EmplacementChapeau.hpp"
#include "../Chapeau/GadgetGrappin.hpp"
#include "../Doigts/EmplacementDoigts.hpp"
#include "../Doigts/GadgetLampe.hpp"

TEST_CASE("L'inspecteur Gadget peut installer et utiliser des gadgets", "[inspecteurGadget]")
{
    InspecteurGadget inspecteur;
    SECTION("aucun gadget n'est installé par défaut")
    {

        string actionDuGedget = inspecteur.goGoGadgeto("manteau");
        REQUIRE(actionDuGedget == "Le gadget manteau n'est pas installé. L'inspecteur se casse la gueule!");
        REQUIRE(inspecteur.goGoGadgeto("grappin") == "Le gadget grappin n'est pas installé. L'inspecteur se casse la gueule!");
        REQUIRE(inspecteur.goGoGadgeto("truc non inventé") == "Le gadget truc non inventé n'est pas installé. L'inspecteur se casse la gueule!");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Aucun gadget installé! L'inspecteur se casse la gueule!");
    }

    SECTION("installer un manteau permet d'activer le manteau")
    {
        REQUIRE(inspecteur.installer("manteau") == "Le gadget manteau est installé!");
        REQUIRE(inspecteur.goGoGadgeto("manteau") == "Go Go Gadgeto manteau! le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Go Go Gadgeto manteau! le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");
    }

    SECTION("installer un grappin permet d'activer le grappin")
    {

        REQUIRE(inspecteur.installer("grappin") == "Le gadget grappin est installé!");
        REQUIRE(inspecteur.goGoGadgeto("grappin") == "Go Go Gadgeto grappin! il attrape un voleur avec son grappin!");
        REQUIRE(inspecteur.goGoGadgetoAleatoire() == "Go Go Gadgeto grappin! il attrape un voleur avec son grappin!");
    }

    SECTION("installer gadget qui n'a pas encore été inventé est impossible")
    {
        REQUIRE(inspecteur.installer("turbo smartphone") == "Le gadget turbo smartphone n'a pas été inventé! Installation impossible!");
    }
}

TEST_CASE("On peut inventer des gadgets et fournir cette liste à l'inspecteur", "[inspecteurGadget]")
{

    GadgetsInventes gadgetsInventes;

    SECTION("installer gadget qui n'a pas encore été inventé est impossible")
    {
        InspecteurGadget inspecteur = InspecteurGadget(gadgetsInventes);
        REQUIRE(inspecteur.installer("turbo smartphone") == "Le gadget turbo smartphone n'a pas été inventé! Installation impossible!");
    }

    SECTION("installer gadget qui a été inventé est possible")
    {
        REQUIRE(gadgetsInventes.inventer("turbo smartphone", "L'inspecteur prend un selfie et l'envoie sur instagram!") == "Le gadget turbo smartphone est inventé!");
        InspecteurGadget inspecteur = InspecteurGadget(gadgetsInventes);
        REQUIRE(inspecteur.installer("turbo smartphone") == "Le gadget turbo smartphone est installé!");
        REQUIRE(inspecteur.goGoGadgeto("turbo smartphone") == "Go Go Gadgeto turbo smartphone! L'inspecteur prend un selfie et l'envoie sur instagram!");
    }

    SECTION("Les gadgets standard ne sont pas présents si on founit des gadgets inventés")
    {
        InspecteurGadget inspecteur = InspecteurGadget(gadgetsInventes);
        REQUIRE(inspecteur.installer("manteau") == "Le gadget manteau n'a pas été inventé! Installation impossible!");
    }
}

TEST_CASE("Emplacement des gadgets divers", "[emplacementDivers]")
{

    EmplacementDivers emplacementDivers;

    SECTION("on ne peut pas activer un gadget divers qui n'a pas été installé")
    {

        REQUIRE(emplacementDivers.activer("manteau") == "Le gadget manteau n'est pas installé. L'inspecteur se casse la gueule!");
    }

    SECTION("on peut activer un gadget divers qui a été installé")
    {
        emplacementDivers.installer(new GadgetManteau());
        REQUIRE(emplacementDivers.activer("manteau") == "Go Go Gadgeto manteau! le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");
    }
}

TEST_CASE("Emplacement des gadgets chapeau", "[gadgetChapeau]")
{

    EmplacementChapeau emplacementChapeau;

    SECTION("on ne peut pas activer un gadget du chapeau qui n'a pas été installé")
    {

        REQUIRE(emplacementChapeau.activer("grappin") == "Le gadget grappin n'est pas installé. L'inspecteur se casse la gueule!");
    }

    SECTION("on peut activer un gadget du chapeau qui a été installé")
    {
        emplacementChapeau.installer(new GadgetGrappin());
        REQUIRE(emplacementChapeau.activer("grappin") == "Go Go Gadgeto grappin! il attrape un voleur avec son grappin!");
    }
}

TEST_CASE("Emplacement des doigts", "[EmplacementDoigts]")
{

    EmplacementDoigts emplacementDoigts;

    SECTION("on ne peut pas activer un gadget du doigt qui n'a pas été installé")
    {
        REQUIRE(emplacementDoigts.activer("lampe") == "Le gadget lampe n'est pas installé. L'inspecteur se casse la gueule!");
    }

    SECTION("on peut activer un gadget de la lampe qui a été installé")
    {
        emplacementDoigts.installer(new GadgetLampe());
        REQUIRE(emplacementDoigts.activer("lampe") == "Go Go Gadgeto lampe! il sort un lampe d'un doigt pour éclairer dans l'obscurité !");
    }

}

// Etapes prochaines
// ✔ 1. Renommer "GadgetsInstalles" en "EmplacementPourGadgets"
// ✔ 2. Déplacer ce qui concerne les gadgets divers (EmplacementDivers, GadgetDivers, GadgetManteau) dans un dossier "Divers"
// ✔ 2.b. Créer les autres types d'emplacement et de gadgets
// ✔ 2.b.1 : Divers : Manteau
// ✔ 2.b.2 : Chapeau : Grappin
// ✔ 2.b.3 : Doigts : Lampe "Une lampe sort du doigt pour éclairer dans l'obscurité"
// ✔ - créer l'emplacementDoigts; juste 5 gadgets;
// ✔ - créer gadgetDoits;
// ✔ - créer gadgetLampe;
// ✔ - empecher de mettre un gadget autre que doigts sur un emplacement de doigt
// ✔ 3. Passer EmplacementPourGadgets::installer(Gadget* gadget) en `protected`
// ✔ - Dans l'inspecteur Gadget remplacer EmplacementPourGadgets par EmplacementDivers et EmplacementChapeau
// Mettre à jour le comportement de l'inspecteur et les tests car on a des novelles problématiques qui on émergées
// - Que se passe quand on installe un seul gadget et que l'inspecteur active aléatoirement?
// - Dans l'inspecteur on veut savoir de quel type est un gadget installable pour pouvoir le mettre dans le bon emplacement (Gadget::getType()?)
// - Tests pour les cas d'emplacements non supportés
// - Renommage GadgetsInventes -> GadgetsInstallables
// - Renommages Catégories de gadgets -> les catégories sont préfixées par "Gadget" et les gadgets concrets ne sont plus préfixés
//   Ex. GadgetLampe -> Lampe, GadgetGrappin -> Grapping ...
// - Est-ce que la classe "GadgetInvente" est encore utile?
// 4. Duplication dans les gadgets spécifiques : "GoGo Gadget ..." : ce comportement pourrait être remonté dans Gadget
// X. Branche l'inspecteur à l'assistant vocal
/*
TEST_CASE("L'inspecteur peut lister les gadget installés", "[inspecteurGadget]")
{
    SECTION("aucun gadget n'est installé par défaut")
    {

        InspecteurGadget inspecteur;
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{}));
    }

    SECTION("1 gadget installé")
    {
        InspecteurGadget inspecteur;
        inspecteur.installer("manteau");
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau"}));
    }
    SECTION("2 gadgets installés")
    {

        InspecteurGadget inspecteur;
        inspecteur.installer("manteau");
        inspecteur.installer("grappin");
        auto liste = inspecteur.listeGadgetsInstalles();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau", "grappin"}));
    }
}
*/
/*
TEST_CASE("L'inspecteur peut lister les gadget pouvant être installés", "[inspecteurGadget]")
{
    SECTION("par défaut le manteau et le grappin sont installables")
    {
        InspecteurGadget inspecteur;
        auto liste = inspecteur.listeGadgetsInstallables();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{"manteau", "grappin"}));
    }

    SECTION("aucun gadget n'est installable")
    {
        InspecteurGadget inspecteur = InspecteurGadget(GadgetsInventes());
        auto liste = inspecteur.listeGadgetsInstallables();
        REQUIRE_THAT(liste, Catch::Matchers::UnorderedEquals(std::vector<string>{}));
    }

}
*/