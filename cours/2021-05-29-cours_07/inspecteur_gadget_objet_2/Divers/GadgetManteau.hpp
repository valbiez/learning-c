#ifndef GADGETS_MANTEAU_HPP
#define GADGETS_MANTEAU_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "GadgetDivers.hpp"

using namespace std;

// Le manteau s'installe sur l'emplacement des gadgets divers
class GadgetManteau : public GadgetDivers
{

public:
  virtual string activer(){
    return "Go Go Gadgeto " + nom + "! " + action;
  }
  virtual string getNom(){
      return nom;
  }

private:
    string nom = "manteau";
    string action = "le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.";
};

#endif