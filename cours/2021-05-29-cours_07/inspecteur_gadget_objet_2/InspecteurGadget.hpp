#ifndef INSPECTEUR_GADGET_HPP
#define INSPECTEUR_GADGET_HPP

#include "EmplacementPourGadgets.hpp"
#include "Doigts/EmplacementDoigts.hpp"
#include "Chapeau/EmplacementChapeau.hpp"
#include "Divers/EmplacementDivers.hpp"

#include "GadgetsInventes.hpp"

using namespace std;

class InspecteurGadget
{
private:
    EmplacementDoigts emplacementDoigts;
    EmplacementChapeau emplacementChapeau;
    EmplacementDivers emplacementDivers;
    GadgetsInventes gadgetsInventes;

    int nombreAleatoire(int nombreMax)
    {
        int randomNumber = std::rand();
        return randomNumber % (nombreMax);
    }

public:
    // TODO : retourner une string (le message indiquant si le gadget a été installé)
    string installer(string nomDuGadget)
    {
        auto gadgetInstallable = gadgetsInventes.recupererGadgetInvente(nomDuGadget);
        // Ici, je veux savoir si les gadgets sont inventés avant de les installer
        if (gadgetInstallable == NULL)
        {
            return "Le gadget " + nomDuGadget + " n'a pas été inventé! Installation impossible!";
        }

        if (nomDuGadget.compare("lampe") == 0)
        {
            return emplacementDoigts.installer((GadgetDoigts *)gadgetInstallable);
        }
        if (nomDuGadget.compare("grappin") == 0)
        {
            return emplacementChapeau.installer((GadgetChapeau *)gadgetInstallable);
        }
        if (nomDuGadget.compare("manteau") == 0)
        {
            return emplacementDivers.installer((GadgetDivers *)gadgetInstallable);
        }
        return "Aucun emplacement ne supporte le gadget " + nomDuGadget;
    }

    // TODO : retourner une string (l'action du gadget)
    string goGoGadgeto(string nomDuGadget)
    {
        if (nomDuGadget.compare("lampe") == 0)
        {
            return emplacementDoigts.activer(nomDuGadget);
        }
        if (nomDuGadget.compare("grappin") == 0)
        {
            return emplacementChapeau.activer(nomDuGadget);
        }
        if (nomDuGadget.compare("manteau") == 0)
        {
            return emplacementDivers.activer(nomDuGadget);
        }
        //il se casse la gueule
        return emplacementDivers.activer(nomDuGadget);
    }

    // TODO : retourner une string (l'action du gadget)
    string goGoGadgetoAleatoire()
    {
        int randomNumber = nombreAleatoire(3);
        switch (randomNumber)
        {
        case 0:
            return emplacementChapeau.activerAleatoirement();
        case 1:
            return emplacementDoigts.activerAleatoirement();
        case 2:
            return emplacementDivers.activerAleatoirement();

        default:
            return emplacementChapeau.activerAleatoirement();
        }
    }

    InspecteurGadget()
    {
        gadgetsInventes.inventer("manteau", "le manteau se gonfle d'air ou de gaz plus léger que l'air. Vous voyer l'inspecteur s'envoler de façon ridicule.");
        gadgetsInventes.inventer("grappin", "il attrape un voleur avec son grappin!");
    }

    InspecteurGadget(GadgetsInventes p_gadgetsInventes)
    {
        gadgetsInventes = p_gadgetsInventes;
    }
};

#endif