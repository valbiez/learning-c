#ifndef GADGET_CHAPEAU_HPP
#define GADGET_CHAPEAU_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "../Gadget.hpp"

using namespace std;

// Représente un gadget qui s'installe sur l'emplacement "Chapeau"
class GadgetChapeau : public Gadget
{
public:
  virtual string activer() = 0;
  virtual string getNom() = 0;
};

#endif