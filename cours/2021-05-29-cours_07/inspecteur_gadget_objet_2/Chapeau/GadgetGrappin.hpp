#if !defined(GADGET_GRAPPIN_HPP)
#define GADGET_GRAPPIN_HPP

#include "GadgetChapeau.hpp"

class GadgetGrappin : public GadgetChapeau
{

public:
  virtual string activer(){
    return "Go Go Gadgeto " + nom + "! " + action;
  }
  virtual string getNom(){
      return nom;
  }

private:
    string nom = "grappin";
    string action = "il attrape un voleur avec son grappin!";

};

#endif // GADGET_GRAPPIN_HPP
