#ifndef GADGET_LAMPE_HPP
#define GADGET_LAMPE_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include "../Gadget.hpp"
#include "GadgetDoigts.hpp"

using namespace std;

class GadgetLampe : public GadgetDoigts
{
    
public:
  virtual string activer(){
    return "Go Go Gadgeto " + nom + "! " + action;
  }
  virtual string getNom(){
      return nom;
  }

private:
    string nom = "lampe";
    string action = "il sort un lampe d'un doigt pour éclairer dans l'obscurité !";

};


#endif