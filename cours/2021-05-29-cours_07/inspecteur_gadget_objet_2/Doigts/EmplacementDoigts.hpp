#ifndef EMPLACEMENT_DOIGTS_HPP
#define EMPLACEMENT_DOIGTS_HPP

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <map>
#include "GadgetDoigts.hpp"

#include "../EmplacementPourGadgets.hpp"

using namespace std;

class EmplacementDoigts : public EmplacementPourGadgets
{
public:
    string installer(GadgetDoigts *const gadget)
    {
        return this->EmplacementPourGadgets::installer(gadget);
    }
};

#endif