#include <stdio.h>
#include <stdlib.h>

void negate();
int moyenne(int a, int b, int c, int d);

// Portée des variables Globales : on peut y accéder partout

int varGlobale = 0;

int main()
{
    printf("%d\n", varGlobale);
    varGlobale++;
    printf("%d\n", varGlobale);
    negate();
    printf("%d\n", varGlobale);

    // Variable locale
    int varLocaleToMain = 10;
    printf("varLocaleToMain : %d\n", varLocaleToMain);

    int moyenneDes4 = moyenne(1, 2, 3, 4);
    printf("moyenneDes4 : %d\n", moyenneDes4);
}

void negate()
{
    varGlobale = 0 - varGlobale;
}

int moyenne(int a, int b, int c, int d)
{
    int sumOfIntegers = a + b + c + d;
    int result = sumOfIntegers / 4;
    printf("result : %d\n", result);
    return result;
}