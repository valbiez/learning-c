## Logistique

Pomodoro.

## Retour sur ce qui a été vu précédemment

> Voir ce qui a été fait dans les exercices
> Vérifier ce qui a été compris : demander à coder ou expliquer

Notions vues précédemment (5-10 min)
- 

Glossaire

## Vérifier mes propres connaissances

Le dire si mes connaissances sont limitées

## Etablir but et objectufs du cours du jour

> But : à quoi ça va servir
> Objectif : Ce qui est enseigné dans le cours du jour

But : 

Objectifs : 
- 
- 
- 

## Explication magistrale

a. Cadence normale, sans commentaire : ex résultat final vers lequel tendre
b. Cadence lente avec commentaires en détaillant progressivement
  - sur les détails, essayer d'identifier environs **3 points clés**
    !!! les points clé doivent être la bonne façon de faire (pas de faux-plis)
Ces points clés doivent m'obliger à bien penser les points primordiax de mon cours

### Exemple canonique

## Exercices, la répétition

Plusieurs Exercices pour chaque étape de façon atomique. Puis l'ensemble assemblé.

Lors de la correction, prendre 1 défaut à la fois. En commençant par le plus important.

> Ok, tu as compris ..., maintenant tu peux encore t'améliorer en ...

### Une fois les exercices faits, vérifier les connaissances

### Rappeler le but et les objectifs du cours

But : 

Objectifs : 

Et les points clés : 
- 
- 
- 

## Annoncer la suite, le prochain cours

But et objectifs
