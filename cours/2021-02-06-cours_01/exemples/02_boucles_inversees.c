#include <stdio.h>
#include <stdlib.h>

int main ()
{

    printf("\nBoucle while inversée.\n");

    int indexWhile=5;
    while(indexWhile >= 0 ){
    
        printf("Valeur de l'indexWhile : %d\n", indexWhile);

        // décrémentation de l'index
        indexWhile--;
    }


    printf("\nBoucle do while inversée.\n");
    // Dans une boucle do..while le code à l'intérieur du bloc do{ } est toujours exécuté au moins une fois

    // valeur initiale de l'index
    int indexDoWhile=5;
    do {

        printf("Valeur de l'indexDoWhile : %d\n", indexDoWhile);

        // décrémentation de l'index
        indexDoWhile--;

    } while(indexDoWhile >= 0); // Condition de continuation de la boucle

        
    int index =5;
    printf("\nBoucle for inversée.\n");

    // for  (valeur initiale de l'index ; condition de continuation ; décrémentation de l'index)
    for (index; index>=0; index--){
        
        printf("Valeur de l'index : %d\n", index);
    }

    return 0;
}
