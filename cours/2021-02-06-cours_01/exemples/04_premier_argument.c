#include <stdio.h>
#include <stdlib.h>

int main (int argc,  char *argv[])
{
    if(argc > 0){
        // char* c'est une chaine de caractères (un texte)
        char* argumentTexte = argv[1];
        printf("Valeur de l'argument 1 : %s\n",  argumentTexte);
    }
    return 0;
}
