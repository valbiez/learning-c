# Exemples

## 01_boucles.c

Ce programme montre comment compter de 0 à 4 avec

- une boucle while
- une boucle do ... while
- une boucle for

Pour exécuter le programme : 

```bash
make
bin/02_boucles_inversees
```

Résultat :

```
Boucle while.
Valeur de l'indexWhile : 0
Valeur de l'indexWhile : 1
Valeur de l'indexWhile : 2
Valeur de l'indexWhile : 3
Valeur de l'indexWhile : 4

Boucle do while.
Valeur de l'indexDoWhile : 0
Valeur de l'indexDoWhile : 1
Valeur de l'indexDoWhile : 2
Valeur de l'indexDoWhile : 3
Valeur de l'indexDoWhile : 4

Boucle for.
Valeur de l'index : 0
Valeur de l'index : 1
Valeur de l'index : 2
Valeur de l'index : 3
Valeur de l'index : 4
```


## 02_boucles_inversees.c

Ce programme montre comment compter de 0 à 4 avec

- une boucle while
- une boucle do ... while
- une boucle for

Pour exécuter le programme : 

```bash
make
bin/01_boucles
```

Résultat :

```
Boucle while inversée.
Valeur de l'indexWhile : 5
Valeur de l'indexWhile : 4
Valeur de l'indexWhile : 3
Valeur de l'indexWhile : 2
Valeur de l'indexWhile : 1
Valeur de l'indexWhile : 0

Boucle do while inversée.
Valeur de l'indexDoWhile : 5
Valeur de l'indexDoWhile : 4
Valeur de l'indexDoWhile : 3
Valeur de l'indexDoWhile : 2
Valeur de l'indexDoWhile : 1
Valeur de l'indexDoWhile : 0

Boucle for inversée.
Valeur de l'index : 5
Valeur de l'index : 4
Valeur de l'index : 3
Valeur de l'index : 2
Valeur de l'index : 1
Valeur de l'index : 0
```

## 03_nombres_d_arguments.c


```bash
make
```

Avec 0 argument

```bash
bin/03_nombre_d_arguments 
```

Résultat

```
0 arguments ont été saisis.
```
Avec 1 argument

```bash
bin/03_nombre_d_arguments 5aa 
```

Résultat

```
1 arguments ont été saisis.
```

Avec 4 arguments

```bash
bin/03_nombre_d_arguments 5 6 6 6
```

Résultat

```
4 arguments ont été saisis. 
```


## 04_premier_argument.c

Ce programme affiche la valeur du son premier argument.

Sans argument

```bash
bin/04_premier_argument 
```
Résultat :

```
Valeur de l'argument 1 : (null)
```


Sans argument

```bash
bin/04_premier_argument 5
```
Résultat :

```
Valeur de l'argument 1 : 5
```

## 05_texte_vers_entier.c

Transforme la chaine de caractères "3" en nombre entier 3.

```bash
make
```

Exécution du proramme

```bash
bin/05_texte_vers_entier
```
Résultat :

```
Valeur de l'entier : 3
```
