#include <stdio.h>
#include <stdlib.h>

// Les boucles permettent de répéter les instructions d'un bloc {   }
// Jusqu'à ce que la condition de continuation ne soit plus vraie
// Il existe plusieurs types de boucles
//
// Boucle do .. while
// Boucle while
// Boucle for
int main ()
{

    printf("\nBoucle while.\n");

    // valeur initiale de l'index
    int indexWhile=0;
    // condition de continuation de la boucle
    while(indexWhile < 5 ){
    
        printf("Valeur de l'indexWhile : %d\n", indexWhile);

        // incrémentation de l'index
        indexWhile++;
    }


    printf("\nBoucle do while.\n");
    // Dans une boucle do..while le code à l'intérieur du bloc do{ } est toujours exécuté au moins une fois

    // valeur initiale de l'index
    int indexDoWhile=0;
    do {

        printf("Valeur de l'indexDoWhile : %d\n", indexDoWhile);

        // Incrémentation de l'index
        indexDoWhile++;

    } while(indexDoWhile <5); // Condition de continuation de la boucle

        
    printf("\nBoucle for.\n");

    // for  (valeur initiale de l'index ; condition de continuation ; incrémentation de l'index)
    for (int index = 0; index < 5; index++){
        
        printf("Valeur de l'index : %d\n", index);
    }

    return 0;
}
