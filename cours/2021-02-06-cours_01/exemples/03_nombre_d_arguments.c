#include <stdio.h>
#include <stdlib.h>


// Pour tester, compiler le code
//
// gcc 03_nombre_d_arguments.c - 03
//
// ./03
// ./03 1 2 3
// ./03 premier deuxième troisième
// ./03 premier 2 troisième
int main (int argc,  char *argv[])
{
    printf("%d arguments ont été saisis.\n",argc-1);
    return 0;
}
