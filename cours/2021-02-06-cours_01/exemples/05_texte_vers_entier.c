#include <stdio.h>
#include <stdlib.h>

//https://devdocs.io/c/string/byte/atoi
int main ()
{

    char* texte = "3";
    // Conversion de la chaine de caractères (texte) en entier
    int entier = atoi(texte);
    printf("Valeur de l'entier : %d\n", entier);

    return 0;
}
