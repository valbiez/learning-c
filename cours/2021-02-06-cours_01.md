# 2021-02-06 Cours n°1

## Entretien

### Connaissances préalables

- Racket (Lisp) (ex. snake)
    - DrRacket
- C (Arduino)
    - Leds, bouttons
    - Boite à musique

### Buts et objectifs

Buts
- Programmer des robots comme hobbie
- Gagner en autonomie
- Pouvoir concrétiser des idées

Objectifs
- Arriver à comprendre du code
- Pouvoir organiser du code
- Mettre en place du feedback pour pouvoir perçevoir une progression et maintenir
- S'approprier un **modèle mental** appréhender les concepts abstraits de la programmation
- Maitriser des outils et langages de programmation suffisants pour progresser vers les buts

## Cours

- Commentaire multi-ligne : 
   ```c
   /* mon commentaire sur 
      plusieurs 
      lignes */
   ```


- Ecrire le resultat qu'on aimerait avoir sous forme de texte (ex en commentaires dans le code)
   ```c
    /*Quel est le nombre?
    10
    +1
    c'est plus
    42
    +1 
    Bravo, tu as trouvé en 2 coups!*/
    ```
- A partir du petit scénario décrit on peut compiler puis exécuter le code pour vérifier régulièrement que ça correspond à nos attentes. Ce sont des tests qu'on a écrit pour vérifier que notre programme fonctionne bien.
- Pour les besoins du test, on a fixé la valeur du nombre à deviner à `42`
  - L'objectif ici, est de vérifier que le compteur marche bien
  - On sait que le nombre aléatoire fonctionne donc, pour notre test il nous gène
  - Du coup, fixer la valeur nous a permis de simplifier notre programme pour les besoins de notre test (on remettra l'aléatoire quand notre test sera ok)

- On a vu `ls` pour lister les fichiers/dossiers du dossier courant
  - Il existe aussi `cd` peut être utilisé avec .. pour le dossier parent pour revenir en arriere `cat`
  - Il existe aussi 
- On a vu la compilation depuis le terminal
  ```bash
  # compilation du code main.c vers un fichier executable appelé : jeu
  gcc main.c -o jeu
  ```
- On a vu l'execution du programme depuis le terminal
  ```bash
  ./jeu
  ```

- On a exécuté notre programme `jeu` pour reproduire le scénario qu'on a décrit sous forme de texte. Ca nous a permis de constater que le résultat était incorrect
- On a essayé de changer le code pour que le résultat soit correct
  - ```c
    printf ("Bravo, tu as trouvé en %d coups!!!!\n\n");
    ```
  - on est tombé sur une erreur indiquée par l'éditeur de code
    ![](2021-02-06-cours_01/erreur_printf_vs_code.png)
  - on peut voir toutes les erreurs également dans la rubrique "Problems"
    ![](2021-02-06-cours_01/erreurs_problems_vs_code.PNG)
- On est bloqué car on manquait de connaissances
  - On a été chercher cette connaissance dans la documentation du langage
    - Ma recommandation c'est de chercher dans la documentation en premier
    - https://devdocs.io/c/
    - https://c.developpez.com/
    - https://arduino.developpez.com/


## Exercices

1. compiler le code main.c pour qu'il génère un programme appelé `jeuplusmoins`
2. executer le programme `jeuplusmoins`
3. (optionnel) modifier le programme pour qu'il s'arrête après 5 tentatives ratées ou lorsque le joueur a trouvé
4. (optionnel) lorsque que le joueur trouve après 2 essais (dont le dernier), afficher : "Bravo, tu as trouvé en 2 coups!!!!" et lorsqu'il n'a pas trouvé, "Tu feras mieux la prochaine fois (tu as fait 5 essais)"
5. (optionnel) Permettre de choisir le nombre d'essais au démarrage du programme. 
    ```bash
    # Lance le jeu avec 10 tentatives pour le joueur
    ./jeu 10
    Quel est le nombre?
    10
    c'est plus
    10
    c'est plus
    10
    c'est plus
    10
    c'est plus
    10
    c'est plus
    10
    c'est plus
    10
    c'est plus
    10
    c'est plus
    10
    c'est plus
    10
    Tu feras mieux la prochaine fois (tu as fait 10 essais)
    ```
6. (optionnel) Permettre de choisir la valeur max parmi les valeurs aléatoires au démarrage du programme (par argument)
    ```bash
    # Lance le jeu avec 10 tentatives pour le joueur, pour un nombre aléatoire de 0 à 500
    ./jeu 10 500
    Quel est le nombre?
    ```
7. (optionnel) Permettre de choisir la valeur max parmi les valeurs aléatoires si le 2ème argument est vide
    ```bash
    # Lance le jeu avec 10 tentatives pour le joueur, pour un nombre aléatoire de 0 à 500
    ./jeu 10
    Choisissez une valeur max: 500
    Quel est le nombre?
    ```

## Connaissances complémentaires

Tu as deja utilisé une forme de boucle : do .. while dans le jeu du min max.
Voici d'autres façons de boucler: 
- [01_boucles.c](2021-02-06-cours_01/exemples/01_boucles.c)
- [02_boucles_inversees.c](2021-02-06-cours_01/exemples/02_boucles_inversees.c)

Un programme peut prendre des arguments (comme la commande `gcc main.c -o jeu` prend les arguments "main.c", "-o", "jeu")
Voici des exemples de programmes qui prennent des arguments et peuvent les utiliser à l'intérieur du code :
- [03_nombre_d_arguments.c](2021-02-06-cours_01/exemples/03_nombre_d_arguments.c)
- [04_premier_argument.c](2021-02-06-cours_01/exemples/04_premier_argument.c)

Voici comment convertir une chaîne de caractères (un texte) en nombre entier
- [05_texte_vers_entier.c](2021-02-06-cours_01/exemples/05_texte_vers_entier.c)

## Exercices complémentaires

A l'aide des connaissances complémentaires ci-dessus.

- Ecrire un programme "liste_args" pour lister la valeur de chacun des arguments du programme. Voici un exemple d'exécution du programme et des résultats obtenus.
  ```bash
  ./liste_args 1 deux 3
  Liste des arguments : 
  argument 1 : 1
  argument 2 : deux
  argument 3 : 3
  ```
- Modifier le programme "jeu de plus ou du moin" pour qu'il prenne des arguments
  1. définir le coup max en premier argument
  ```bash
  ./jeu_par_args 2
  C'est parti! En 2 coups max!
  Quel est le nombre ? 5
  C'est plus !
  Quel est le nombre ? 5
  Perdu,Tu feras mieux la prochaine fois (tu as fait 2 essais)
  ```
  2. définir le nombre mystère en 2ème argument
  ```bash
  ./jeu_par_args 2 42
  C'est parti! En 2 coups max!
  Quel est le nombre ? 42
  Bravo, tu as trouvé en 1 coups!!!!
  ```
  3. si le nombre mystère n'est pas donné en argument, il doit être saisi pendant l'exécution du programme
  ```bash
  ./jeu_par_args 2
  C'est parti! En 2 coups max!
  Quel est ton nombre mystère? 42
  Quel est le nombre ? 42
  Bravo, tu as trouvé en 1 coups!!!!
  ```
  4. si aucun argument n'est donné, saisie du nombre de coups max et du nombre mystère
  ```bash
  ./jeu_par_args
  Combien de coup max veut tu ? 2
  C'est parti! En 2 coups max!
  Quel est ton nombre mystère? 42
  Quel est le nombre ? 42
  Bravo, tu as trouvé en 1 coups!!!!
  ```

- Maintenant que tu as écrit un jeu avec un menu, tu peux ajouter un mode "ordinateur contre ordinateur". Quand ce mode est sélectionné, le jeu choisit automatiquement un nombre aléatoire entre 10 et 500. 
  1. L'ordinateur tente de trouver la solution en générant un nombre aléatoire (ex. de tentatives : 12, 450, 212, 300). Le jeu s'arrète quand l'ordinateur a trouvé. Le programme affiche le nombre de tentatives.
  2. Pas très efficace tout ça! Essayons en commençant par 0 et en ajoutant 1 à chaque tentative (ex. de tentatives : 0, 1, 2, 3). Le jeu s'arrête quand l'ordinateur a trouvé. Le programme affiche le nombre de tentatives.
  3. Essayons maintenant en commençant par la fin 500 et en retranchant 1 à chaque tentative (ex. de tentatives : 500, 499, 498). Le jeu s'arrête quand l'ordinateur a trouvé. Le programme affiche le nombre de tentatives.
  4. Y a-t-il des façons plus efficaces de trouver?