# Introduction aux pointeurs

## Ressources externes

- [Tutoriel cplusplus.com sur les pointeurs](https://www.cplusplus.com/doc/tutorial/pointers/)

## Définitions

### Valeur d'une variable

- variable : 
- valeur :


### Adresse en mémoire

- adresse : dans un programme C, quand une variable est déclarée, elle prend une adresse en mémoire
- référence : valeur de l'adresse mémoire d'une variable
- %p : dans printf(), permet d'afficher l'adresse mémoire d'une référence

### Pointeur

- pointeur : une variable qui stocke une adresse
- déréférencement : `*variable` : obtenir la valeur qui correspond à l'adresse

## Les valeurs, adresses et pointeurs par la pratique

- [Tableau blanc interactif miro](https://miro.com/app/board/o9J_lOPou90=/) illustrant l'exemple qui suit.


Une variable dispose d'une adresse dans la mémoire.

```c
int entier = 1;
```

Valeurs et adresses

```
entier : 1
&entier : 0x7fff3ecd8aa0
```

L'adresse d'une variable est définie à sa déclaration.

```c
int variable1, variable2;
```

Valeurs et adresses

```
&variable1 : 0x7fff3ecd8aa4
&variable2 : 0x7fff3ecd8aa8
```

Quand on change **la valeur** d'une variable, **son adresse** reste la même.
On remarque que **l'adresse** de la `variable2` est juste après celle de la `variable1`.

```c
variable1 = 30;
variable2 = 20;

```

Valeurs et adresses

```
variable1 : 30, &variable1 : 0x7fff3ecd8aa4
variable2 : 20, &variable2 : 0x7fff3ecd8aa8
```

**La valeur** a changé, **l'adresse** de la variable, elle, n'a pas changé.

```c
variable1++;
```

Valeurs et adresses

```
variable1 : 31
&variable1 : 0x7fff3ecd8aa4
```

Création d'un **pointeur** `pointeur` vers **l'adresse** de la variable `variable1`.
```c

int *pointeur = &variable1;
```

Valeurs et adresses

```
pointeur : 0x7fff3ecd8aa4
&variable1 : 0x7fff3ecd8aa4
*pointeur : 31
variable1 : 31
```

On indique au **pointeur** de pointer vers **l'adresse** suivante en mémoire.
Ici, on remarque que **l'adresse** courante du **pointeur** a augmenté.
On remarque que c'est la même **adresse** que celle de la variable `variable2`
Par conséquent, le **déréférencement** du pointeur devrait correspondre à la **valeur** de la variable `variable2`.
C'est à dire : 20

```c
pointeur++;
```

Valeurs et adresses

```
pointeur : 0x7fff3ecd8aa8
*pointeur : 20
&variable2 : 0x7fff3ecd8aa8
variable2 : 20
```

Création d'une nouvelle variable `variable3`.
Elle occupe **une nouvelle adresse** en mémoire

```c
int variable3;
```

Valeurs et adresses

```
&variable3 : 0x7fff3ecd8aac
```

`*pointeur` : Déréférencement de la **valeur** pointée par le **pointeur**.
C'est à dire qu'on veut récupérer la **valeur** vers laquelle le pointeur pointe.

On affecte cette valeur à la variable `variable3`
Par contre, comme c'est **la valeur** pointée par le pointeur qu'on a affecté à la `variable3`, **l'adresse** de la `variable3` reste inchangée.

```c
variable3 = *pointeur;
```

Valeurs et adresses

```
*pointeur : 20
variable3 : 20

pointeur   : 0x7fff3ecd8aa8
&variable2 : 0x7fff3ecd8aa8
&variable3 : 0x7fff3ecd8aac
```

Du coup, incrémenter **la valeur** de la `variable3` n'affecte pas **la valeur** de la `variable2`, ni **la valeur** vers laquelle pointe le `pointeur`.

```c
variable3++;
```

Valeurs et adresses

```
variable3 : 21
variable2 : 20
*pointeur : 20
```

Par contre, lorsqu'on change **la valeur** de la `variable2`,
**la valeur** déréférencée par le `pointeur` est affectée
car **la zone en mémoire** correspondant à la `variable2` est
la même que celle vers laquelle pointe le `pointeur`.

```c
variable2 = 60;
```

Valeurs et adresses

```
variable2 : 60
*pointeur : 60
variable3 : 21
```

## Exemples visuels

- ![](2021-03-19-cours_05/assets/01_variable_et_adresse_memoire.png)
- ![](2021-03-19-cours_05/assets/02_variable_et_adresse_memoire_metaphore.png)
- ![](2021-03-19-cours_05/assets/03_variables_et_adresses_memoire.png)
- ![](2021-03-19-cours_05/assets/04_variables_et_adresses_memoire_metaphore.png)
- ![](2021-03-19-cours_05/assets/05_modification_valeur_variable.png)
- ![](2021-03-19-cours_05/assets/06_pointeur_vers_adresse_variable.png)
- ![](2021-03-19-cours_05/assets/07_pointeur_vers_adresse_variable.png)
- ![](2021-03-19-cours_05/assets/08_increment_pointeur.png)
- ![](2021-03-19-cours_05/assets/09_exercice.png)
- ![](2021-03-19-cours_05/assets/10_exercice_solution.png)