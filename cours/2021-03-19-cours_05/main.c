#include <stdio.h>
#include <stdlib.h>

int main()
{

    int entier = 1;
    printf("int entier = 1;\n> entier : %d\n", entier);
    // Une variable dispose d'une adresse dans la mémoire
    printf("> &entier : %p\n\n", &entier);

    int variable1, variable2;
    printf("int variable1, variable2;\n> &variable1 : %p, &variable2 : %p\n\n", &variable1, &variable2);
    // L'adresse d'une variable est définie à sa déclaration

    // // Attention aux variables non initialisées!!!
    // printf("variable1 : %d, variable2 : %d\n\n", variable1, variable2);

    variable1 = 30;
    variable2 = 20;
    printf("variable1 = 30;\nvariable2 = 20;\n");
    printf("> variable1 : %d, &variable1 : %p\n", variable1, &variable1);
    printf("> variable2 : %d, &variable2 : %p\n\n", variable2, &variable2);
    // Quand on change la valeur d'une variable, son adresse reste la même
    // On remarque que l'adresse de la variable2 est juste après celle de la variable1
    // variable1 : 30, &variable1 : 0x7fff73b70360
    // variable2 : 20, &variable2 : 0x7fff73b70364

    variable1++;
    printf("variable1++;\n> variable1 : %d, &variable1 : %p\n\n", variable1, &variable1);
    // La valeur a changé, l'adresse de la variable, elle n'a pas changé
    // variable1 : 31, &variable1 : 0x7fff73b70360

    // Création d'un pointeur vers l'adresse de la variable variable1
    int *pointeur = &variable1;
    printf("int *pointeur = &variable1;\n> pointeur : %p, &variable1 : %p\n", pointeur, &variable1);
    // pointeur : 0x7fff73b70360, &variable1 : 0x7fff73b70360
    printf("> *pointeur : %d, variable1 : %d\n\n", *pointeur, variable1);
    // *pointeur : 31, variable1 : 31

    // On indique au pointeur de pointer vers l'adresse suivantes en mémoire
    pointeur++;
    printf("pointeur++;\n> pointeur : %p\n", pointeur);
    // pointeur : 0x7fff73b70364
    // Ici, on remarque que l'adresse courante du pointeur est 0x7fff73b70364
    // On remarque que c'est la même adresse que celle de la variable variable2
    printf("> pointeur : %p, &variable2 : %p\n", pointeur, &variable2);
    // Par conséquent, le déréférencement du pointeur
    // devrait correspondre à la valeur de la variable variable2
    // c'est à dire : 20
    printf("> *pointeur : %d, variable2 : %d\n\n", *pointeur, variable2);
    // *pointeur : 20, variable2 : 20

    // Création d'une nouvelle variable variable3
    // elle occupe une nouvelle adresse en mémoire
    int variable3;
    printf("int variable3;\n> &variable3 : %p\n\n", &variable3);
    // &variable3 0x7fff73b70368

    // *pointeur : Déréférencement de la valeur pointée par le pointeur
    // C'est à dire qu'on veut récupérer la valeur vers laquelle le pointeur pointe
    // On affecte cette valeur à la variable variable3
    variable3 = *pointeur;
    printf("variable3 = *pointeur;\n> variable3 : %d, *pointeur : %d\n", variable3, *pointeur);

    // Par contre, comme c'est la valeur pointée par le pointeur qu'on a affecté à la variable3
    // l'adresse de la variable3 reste inchangée
    printf("> pointeur : %p, &variable2 : %p, &variable3 : %p\n\n", pointeur, &variable2, &variable3);
    // pointeur : 0x7fff73b70364, &variable2 : 0x7ffd6bee21c4, &variable3 : 0x7ffd6bee21c8

    // Du coup, incrémenter la valeur de la variable3 n'affecte pas la valeur de la variable2,
    // ni celle vers laquelle pointe le pointeur
    variable3++;
    printf("variable3++;\n> variable3 : %d, variable2 : %d, *pointeur : %d\n\n", variable3, variable2, *pointeur);
    // variable3 : 21, variable2 :20, *pointeur : 20

    // Par contre, lorsqu'on change la valeur de la variable2,
    // la valeur déréférencée par le pointeur est affectée
    // car la zone en mémoire correspondant à la variable2 et au pointeur est la même
    variable2 = 60;
    printf("variable2=60;\n> variable2 : %d, *pointeur : %d, variable3 : %d\n\n", variable2, *pointeur, variable3);
    // variable2 : 60, *pointeur : 60, variable3 : 21
}
