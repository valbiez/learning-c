#include <stdio.h>
#include <stdlib.h>

int main()
{
    // Chaines de caractères, tableaux et pointeurs.

    int maisons[3];

    // Pour chacune des maisons de la rue par défaut elle est vide : -1
    for (int i = 0; i < 3; i++)
    {
        maisons[i] = i;
    }

    int * maisonsPointeur = maisons;
    printf("Adresse de maisons %p\n", maisons);
    printf("Adresse du pointeur %p\n", maisonsPointeur);

    printf("Déréférencement de maisonsPointeur : %d\n", *maisonsPointeur);
    maisonsPointeur++;
    printf("Déréférencement de maisonsPointeur : %d\n", *maisonsPointeur);
    maisonsPointeur++;
    printf("Déréférencement de maisonsPointeur : %d\n", *maisonsPointeur);

    // maisonsPointeur++;
    // printf("Déréférencement de maisonsPointeur : %s\n", *maisonsPointeur);
    // un habitant de 20 ans emménage à la première maison de la rue
    // maisons[0] = 20;

    printf("Contenu de la maison0 : %d\n", maisons[0]);

    // Attention si la rue n'a pas été préalablement habitée
    printf("Contenu de la maison1 : %d\n", maisons[1]);

    printf("Contenu de la maison2 : %d\n", maisons[2]);
}
