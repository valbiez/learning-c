#include <stdio.h>
#include <stdlib.h>

int main()
{
    // Chaines de caractères, tableaux et pointeurs.

    char mot[3] = "oui";

    // mot[0]='o';
    // mot[1]='u';
    // mot[2]='i';

    printf("La letter 0 : %c\n", mot[0]);
    printf("La letter 1 : %c\n", mot[1]);
    printf("La letter 2 : %c\n", mot[2]);

    for (int i = 2; i >= 0; i--)
    {
        printf("%c", mot[i]);
    }
    printf("\n");

    // printf("%s\n", mot);

    // exemple de l'ultilisation d'un tableau pour matérialiser une chaine de caractères
    // on peut modifier un caractère dans celle-ci
    char hello[13] = "Salut M@rco!";
    printf("%s\n", hello);

    int indexOfFirstError=-1;

    for (int i = 0; i < 13; i++)
    {
        if (hello[i] == '@')
        {
            indexOfFirstError = i;
        }
    }
    if(indexOfFirstError!=-1){
        hello[indexOfFirstError] = 'a';
    }

    printf("%s\n", hello);
  
}
