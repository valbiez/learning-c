#include <stdio.h>
#include <stdlib.h>

// Compte à rebours avec une boucle do ... while
int main(int argc, char *argv[])
{

  int val = atoi(argv[1]);
  int compteArebours = val;

  do
  {

    printf("%d\n", compteArebours);

    compteArebours--;

  } while (compteArebours >= 0); // Condition de continuation de la boucle

  // A remplacer avec le code demandé par l'exercice
  // Voir README.md
  return 0;
}
