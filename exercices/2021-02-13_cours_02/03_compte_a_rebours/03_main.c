#include <stdio.h>
#include <stdlib.h>

// Compte à rebours avec une boucle for
int main(int argc, char *argv[])
{

  int val = atoi(argv[1]);
  int compteArebours = val;

  for (compteArebours = val; compteArebours >= 0; compteArebours--)
  {
    printf("%d\n", compteArebours);
  }

  return 0;
}
