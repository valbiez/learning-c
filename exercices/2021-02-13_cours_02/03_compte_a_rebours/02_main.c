#include <stdio.h>
#include <stdlib.h>

// Compte à rebours avec une boucle while

int main(int argc, char *argv[])
{

  int val = atoi(argv[1]);
  int compteArebours = val;

  while (compteArebours >= 0)
  {
    printf("%d\n", compteArebours);

    compteArebours--;
  }

  
  return 0;
}
