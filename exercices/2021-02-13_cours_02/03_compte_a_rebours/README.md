# Compte à rebours

## Avec une boucle do .. while

Modifier le fichier `01_main.c` pour que afficher un compte à rebours commençant par le nombre passé en argument et s'arrêtant à 0.

Contrainte
- Utiliser une boucle `do ... while`.

```bash
# compilation du programme 01_main.c
make
# exécution du programme 01_main
bin/01_main 5
```
Résultat

```
5
4
3
2
1
0
```

## Avec une boucle while

Modifier le fichier `02_main.c` pour que afficher un compte à rebours commençant par le nombre passé en argument et s'arrêtant à 0.

Contrainte
- Utiliser une boucle `while`.

```bash
# compilation du programme 02_main.c
make
# exécution du programme 02_main
bin/02_main 5
```
Résultat

```
5
4
3
2
1
0
```

## Avec une boucle for

Modifier le fichier `03_main.c` pour que afficher un compte à rebours commençant par le nombre passé en argument et s'arrêtant à 0.

Contrainte

- Utiliser une boucle `for`.

```bash
# compilation du programme 03_main.c
make 
# exécution du programme 03_main
bin/03_main 5
```

Résultat

```
5
4
3
2
1
0
```