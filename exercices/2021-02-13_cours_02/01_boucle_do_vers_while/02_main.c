#include <stdio.h>
#include <stdlib.h>

int main()
{

  // A remplacer avec le code demandé par l'exercice
  // Voir README.md

  int nombreEntre = 0;
  int compteur = 0;

  int coup_max = 5;
  int nombreMystere = 5;

  while (compteur < coup_max && nombreMystere != nombreEntre)
  {

    printf("Quel est le nombre ? ");
    scanf("%d", &nombreEntre);
    compteur++;

    if (nombreMystere > nombreEntre)
      printf("C'est plus !\n\n");

    else if (nombreMystere < nombreEntre)
      printf("C'est moins !\n\n");

    else if (compteur >= coup_max)
      printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais)\n", coup_max);

    else
    {
      printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
    
  }
    return 0;
}
