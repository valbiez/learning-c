#include <stdbool.h> // true : 1, false : 0
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Ici, un exemple où on enveloppe les conditions du while avec des fonctions
// pour améliorer la lisibilité. Tout en conservant une faible complexité dans
// la boucle.
int main() {
  int nombreMystere = 0;
  int nombreEntre = 0;
  int compteur = 0;
  int coup_max = 5;

  // tant que/while :
  //   je n'ai pas encore tenté
  //   OU
  //     je n'ai pas trouvé le nombre
  //     ET
  //     j'ai encore des tentatives
  while (jeNaiPasTente(compteur) ||
         (jeNaiPasTrouve(nombreEntre, nombreMystere) &&
          jaiEncoreDesTentatives(compteur, coup_max))) {
    // faire/do : proposer un nombre

    printf("Quel est le nombre ? ");
    scanf("%d", &nombreEntre);
    compteur++;
    if (compteur >= coup_max)
      printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais)\n",
             coup_max);
    else if (nombreMystere > nombreEntre)
      printf("C'est plus !\n\n");
    else if (nombreMystere < nombreEntre)
      printf("C'est moins !\n\n");
    else {
      printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
    }
  }

  return 0;
}

bool jeNaiPasTente(int compteurDeTentatives) {
  return compteurDeTentatives == 0;
}

bool jaiEncoreDesTentatives(int compteurDeTentatives, int coupsMax) {
  return compteurDeTentatives < coupsMax;
}

bool jeNaiPasTrouve(int nombreEntre, int nombreMystere) {
  return nombreEntre != nombreMystere;
}