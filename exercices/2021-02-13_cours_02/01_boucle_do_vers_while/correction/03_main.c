#include <stdbool.h> // true : 1, false : 0
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Exemple où on remplace les expressions de la boucle par des variables bool
//
// Inconvienents : on doit mettre à jour ces variables à plusieurs endroits à
// l'intérieur de la boucle.
//
// Avantages :
// C'est un peu plus lisible mais au détriment de plus de complexité dans le
// code de la boucle
int main() {
  int nombreMystere = 0;
  int nombreEntre = 0;
  int compteur = 0;
  int coup_max = 5;

  // Conditions de la boucle
  bool jeNaiPasTente = true;
  bool jeNaiPasTrouve = true;
  bool jaiEncoreDesTentatives = compteur < coup_max;

  // tant que/while :
  //   je n'ai pas encore tenté
  //   OU
  //     je n'ai pas trouvé le nombre
  //     ET
  //     j'ai encore des tentatives
  while (jeNaiPasTente || (jeNaiPasTrouve && jaiEncoreDesTentatives)) {
    // faire/do : proposer un nombre

    // lors du premier essai on indique que le joueur à tenté au moins une fois
    // nécessaire à la prochaine évaluation de la condition de la boucle
    jeNaiPasTente = false;

    printf("Quel est le nombre ? ");
    scanf("%d", &nombreEntre);
    // nécessaire à la prochaine évaluation de la condition de la boucle
    compteur++;
    // nécessaire à la prochaine évaluation de la condition de la boucle
    jaiEncoreDesTentatives = compteur < coup_max;

    if (compteur >= coup_max)
      printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais)\n",
             coup_max);
    else if (nombreMystere > nombreEntre)
      printf("C'est plus !\n\n");
    else if (nombreMystere < nombreEntre)
      printf("C'est moins !\n\n");
    else {
      // nécessaire à la prochaine évaluation de la condition de la boucle
      jeNaiPasTrouve = false;

      printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
    }
  }

  return 0;
}
