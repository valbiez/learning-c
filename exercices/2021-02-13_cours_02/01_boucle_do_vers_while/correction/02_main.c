#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Boucle while
// La condition de continuation de la boucle est evaluée au début de la boucle.
//
// Comme le nombre mystère à deviner est 0, nous devons détecter si le joueur a
// fait une tentative. Sinon le programme se termine sans même rentrer dans la
// boucle.
//
// Ceci car la condition de victoire (nombreMystere == nombreEntre)
// aurait été remplie sans même permettre au joueur de jouer

// Structure :
//
// While : Tant que
//   le joueur n'a pas encore essayé 
//   OU
//     (le joueur n'a pas trouvé le nombre mystère
//      ET 
//      qu'il lui reste des essais )
// Do : Faire une proposition de nombre

int main() {
  int nombreMystere = 0;
  int nombreEntre = 0;
  int compteur = 0;
  int coup_max = 5;

  // tant que/while :
  //   je n'ai pas encore tenté :         compteur == 0
  //   OU :                               ||
  //     je n'ai pas trouvé le nombre :   ( nombreMystere != nombreEntre
  //     ET :                               &&
  //     j'ai encore des tentatives :       compteur < coup_max )
  while (compteur == 0 ||
         (nombreMystere != nombreEntre && compteur < coup_max)) {
    // faire/do  : proposer un nombre

    printf("Quel est le nombre ? ");
    scanf("%d", &nombreEntre);
    compteur++;

    if (compteur >= coup_max)
      printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais)\n",
             coup_max);
    else if (nombreMystere > nombreEntre)
      printf("C'est plus !\n\n");
    else if (nombreMystere < nombreEntre)
      printf("C'est moins !\n\n");
    else {
      printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
    }
  }

  return 0;
}
