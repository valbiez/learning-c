# Différences entre `do` et `while`

Il y a quelques différences entre la boucle `do ... while` et la boucle `while`.

La boucle `do ... while` execute une première fois le code avant d'évaluer la condition de continuation.

La boucle `while` évalue la condition de continuation avant d'exécuter le code.
Cela signifie que le code à l'intérieur d'une boucle `while` peut ne pas être exécuté si la condition de continuation de n'est pas remplie au départ.

## Exercice : Passer d'une boucle do ... while à une boucle while 

Dans cet exercice, le fichier `01_main.c` ne sera pas modifié.

Dans le fichier `02_main.c`, reprendre le fonctionnement du fichier `01_main.c` et 
ré-écrire le jeu du plus ou du moins en utilisant une boucle `while` au lieu d'une boucle `do ... while`.

Remarque : le nombre mystère à deviner est le `0`. C'est aussi la valeur par défaut.

> Indice : S'assurer que le joueur a bien fait au moins une tentative au moment de rentrer dans la boucle `while`

Le programme `02_main` doit avoir exactement le même comportement `01_main`.

Afin de simplifier la compilation du programme la commande `make` peut être utilisée.

```bash
# compilation du programme 01_main.c
make 01_main
# exécution du programme 01_main
bin/01_main
```

```bash
# compilation du programme 02_main.c
make 02_main
# exécution du programme 02_main
bin/02_main
```