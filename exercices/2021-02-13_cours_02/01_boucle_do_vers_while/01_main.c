#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Boucle do
//
// Il y a au moins toujours une exécution de la boucle
// car la condition de continuation est à la fin de la boucle.

// Structure :
//
// Do : Faire une proposition de nombre
// While : Tant que le joueur n'a pas trouvé le nombre mystère
// et qu'il lui reste des essais

int main() {
  int nombreMystere = 0;
  int nombreEntre = 0;
  int compteur = 0;
  int coup_max = 5;

  // faire/do : proposer un nombre.
  do {

    printf("Quel est le nombre ? ");
    scanf("%d", &nombreEntre);
    compteur++;

    if (compteur >= coup_max)
      printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais)\n",
             coup_max);
    else if (nombreMystere > nombreEntre)
      printf("C'est plus !\n\n");
    else if (nombreMystere < nombreEntre)
      printf("C'est moins !\n\n");
    else {
      printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
    }
    // tant que/while :
    //   je n'ai pas trouvé le nombre :   ( nombreMystere != nombreEntre
    //   ET :                               &&
    //   j'ai encore des tentatives :       compteur < coup_max )
  } while (nombreMystere != nombreEntre && compteur < coup_max);

  return 0;
}
