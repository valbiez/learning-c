#include <stdio.h>
#include <stdlib.h>

/** Permet d'afficher le nombre entier positif le plus petit parmi les nombre en
 * argument.*/
int main(int argc, char *argv[]) {


 int MIN = 2147483647;

  for (int index = 1; index < argc; index++)
  {
    int val = atoi(argv[index]);
    if (val < MIN) {
      MIN = val;
    }
  }
  printf("Valeur MIN  : %d\n", MIN);
  // A remplacer avec le code demandé par l'exercice
  // Voir README.md

  return 0;
}
