# Utiliser une boucle pour traiter une liste

## Max

Dans le fichier `01_main.c` écrire un programme qui permet de retourner le nombre **le plus élevé** parmi les nombre saisis en argument.

Compliation

```bash
make
```

Exécution avec 1 seul nombre

```bash
bin/01_main 42
```

Résultat

```
42
```

Exécution avec plusieurs nombres

```bash
bin/01_main 51 65 1 12 9 20
```
Résultat

```
65
```

## Min

Dans le fichier `02_main.c` écrire un programme qui permet de retourner le nombre **le moins élevé** parmi les nombre saisis en argument.

Compilation

```bash
make
```

Exécution avec 1 argument

```bash
bin/02_main 42
```

Résultat

```
42
```

Exécution avec plusieurs nombres

```bash
bin/02_main 51 65 1 12 9 20
```

Résultat

```
1
```