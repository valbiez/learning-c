#include <stdio.h>
#include <stdlib.h>

/** Permet d'afficher le nombre entier positif le plus grand parmi les nombre en argument.*/

//regarder le premier argument
//le mémoriser
//regarder le deuxième argument
//s'il est plus grand que le premier, afficher celui là
//sinon continuer
int main(int argc, char *argv[])
{
  int MAX = 0;

  for (int index = 1; index < argc; index++)
  {
    int val = atoi(argv[index]);
    if (val > MAX) {
      MAX = val;
    }
  }
  printf("Valeur MAX  : %d\n", MAX);

  // A remplacer avec le code demandé par l'exercice
  // Voir README.md

  return 0;
}
