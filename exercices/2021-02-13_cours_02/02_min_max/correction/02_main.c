#include <stdio.h>
#include <stdlib.h>

/** Permet d'afficher le nombre entier positif le plus petit parmi les nombre en
 * argument.*/
int main(int argc, char *argv[]) {

  if (argc == 1)
    return 1;
    
  int min;
  for (int i = 1; i < argc; i++) {
    int nombre = atoi(argv[i]);

    if (nombre < min) {
      min = nombre;
    }
  }
  printf("%d", min);
  // A remplacer avec le code demandé par l'exercice
  // Voir README.md

  return 0;
}
