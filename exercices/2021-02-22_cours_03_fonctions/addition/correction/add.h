#ifndef Add_h
#define Add_h

// Prototypes et documentation des fonctions de la librairie "Add"

/* Adds a and b.*/
int add(int a, int b);

#endif
