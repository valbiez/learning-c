#include <stdio.h>

// Afin d'être connue explicitement par le programme "main", la fonction "add"
// est déclarée en haut du fichier.

/* Ajoute l'entier a et l'entier b.*/
int add(int a, int b) { return a + b; }

/* Programme principal.*/
int main() {
  add(1, 2);
  printf("%d\n", add(1, 2));
  return 0;
}