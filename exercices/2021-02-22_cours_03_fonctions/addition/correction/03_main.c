#include "add.h"
#include <stdio.h>

// Ici, le prototype de la fonction "add" est déclaré dans le fichier add.h
// L'algorithme de la fonction "add" est déclaré dans le fichier add.c

/* Programme principal. */
int main() {
  add(1, 2);
  printf("%d\n", add(1, 2));
  return 0;
}