#include <stdio.h>

// Déclarer le prototype de la fonction "add" permet de décrire son code
// n'importe où dans le fichier.

/* Ajoute l'entier a et l'entier b.*/
int add(int a, int b);

/* Programme principal.*/
int main() {
  add(1, 2);
  printf("%d\n", add(1, 2));
  return 0;
}

// L'algorithme de la fonction "add" peut être déclaré plus bas dans le fichier.

int add(int a, int b) { return a + b; }
