#include <stdio.h>
#include <stdlib.h>

// Nombre de jours par mois
// Voir les instructions : Readme.md

int Affichage_nombre_de_jours_par_mois(int nombre)
{
    switch (nombre)
    {
    case 1:
        printf("31\n");
        break;

    case 2:
        printf("28\n");
        break;

    case 3:
        printf("31\n");
        break;

    case 4:
        printf("30\n");
        break;

    case 5:
        printf("31\n");
        break;

    case 6:
        printf("30\n");
        break;

    case 7:
        printf("31\n");
        break;

    case 8:
        printf("31\n");
        break;

    case 9:
        printf("30\n");
        break;

    case 10:
        printf("31\n");
        break;

    case 11:
        printf("30\n");
        break;

    case 12:
        printf("31\n");
        break;
    }
}

void main()
{
    int nombre_de_jour_mois = 1;

    while (nombre_de_jour_mois <= 12)
    {
        Affichage_nombre_de_jours_par_mois(nombre_de_jour_mois);
        nombre_de_jour_mois++;
    }

    // 31 - jan
    // 28 - fev
    // 31 - mar
    // 30 - avr
    // 31 - mai
    // 30 - juin
    // 31 - juillet
    // 31 - aout
    // 30 - septembre
    // 31 - octobre
    // 30 - novembre
    // 31 - decembre
}