#ifndef LIB_11B
#define LIB_11B

// Prototypes des fonctions du calendrier

/**
 * Affiche une date au format lisible par un humain.
 * 
 * Ex.
 * > affiche_date(3,3,3);
 * > mercredi 03 mars
 * 
 * num_jour_dans_mois : numéro du jour dans l'année (1-366)
 * num_mois_dans_annee : numéro du mois dans l'année (1->12)
 * num_jour_dans_semaine : numéro du jour dans la semaine (1->7)
 */
void affiche_date(int num_jour_dans_mois, int num_mois_dans_annee, int num_jour_dans_semaine);

/**
 * Retourne le nombre de jours du mois.
 * 
 * Ex. pour janvier
 * > nombre_de_jours_par_mois(1);
 * > 31
 * 
 * num_mois_dans_annee : numéro du mois (1 -> 12)
 */
int nombre_de_jours_par_mois(int num_mois_dans_annee);

/**
 * Retourne le nom du mois.
 * 
 * Ex.
 * > nom_mois(12);
 * > "décembre"
 * 
 * num_mois_dans_annee : numéro du mois (1 -> 12)
 */
char *nom_mois(int num_mois_dans_annee);

/**
 * Retourne le nom du jour dans la semaine
 * 
 * Ex.
 * > nom_jour(3);
 * > "mercredi"
 * 
 * num_jour_dans_semaine : numéro du jour dans la semaine (1->7)
 */
char *nom_jour(int num_jour_dans_semaine);

#endif