# Métro boulot dodo

Pour compiler tous les programmes : 

```bash
make
```

## 01_main.c journée de travail

Définir une journée ouvrée.

Pour lancer le programme

```bash
make
bin/01_main 
```

Résultat attendu

```
Metro
Boulot
Dodo
```

## 02_main.c

Décrire une semaine de travail (hors weekend).

Concepts pouvant être utilisés :
- fonction
- (boucle) optionnel

Variantes :
- Bronze : moins de 25 lignes de code
- Argent : moins de 20 lignes de code
- Or : moins de 17 lignes de code

Pour lancer le programme

```bash
make
bin/02_main 
```

Résultat attendu
```
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
```


## 03_main.c la semaine complète!

Décrire la semaine en y ajoutant le weekend.

- fonction
- (boucle) optionnel

Variantes :
- bronze : en moins de 50 lignes de code
- argent : en moins de 40 lignes de code
- or : en moins de 30 lignes de code


Pour lancer le programme

```bash
make
bin/03_main 
```

Résultat attendu

```
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Télé
Dodo
Télé
Dodo
```

## 04_main.c - le mois

- fonction
- (boucle) optionnel

Variantes : 

- bronze : en moins de 50 lignes de code
- argent : en moins de 40 lignes de code
- or : en moins de 36 lignes de code

Pour lancer le programme

```bash
make
bin/04_main 
```

Résultat attendu

```
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Télé
Dodo
Télé
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Télé
Dodo
Télé
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Télé
Dodo
Télé
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Metro
Boulot
Dodo
Télé
Dodo
Télé
Dodo
```

## 05_main.c - la semaine avec le numéro de jour de la semaine

Pour chaque jour de la semaine, afficher le numéro du jour suivi des activités de la journée.

- fonction
    - avec 1 paramètre
- (boucle) optionnel

Pour lancer le programme

```bash
make
bin/05_main 
```

Résultat attendu

```
Jour n°1
Metro
Boulot
Dodo
Jour n°2
Metro
Boulot
Dodo
Jour n°3
Metro
Boulot
Dodo
Jour n°4
Metro
Boulot
Dodo
Jour n°5
Metro
Boulot
Dodo
Jour n°6
Télé
Dodo
Jour n°7
Télé
Dodo
```

## 06_main.c - nom du jour

A la place de "Jour n°", afficher le nom du jour.

Concepts pouvant être utilisés

- switch
- fonction
    - avec 1 paramètre

Pour lancer le programme

```bash
make
bin/06_main 
```

Résultat attendu

```
Lundi
Metro
Boulot
Dodo
Mardi
Metro
Boulot
Dodo
Mercredi
Metro
Boulot
Dodo
Jeudi
Metro
Boulot
Dodo
Vendredi
Metro
Boulot
Dodo
Samedi
Télé
Dodo
Dimanche
Télé
Dodo
```

## 07_main.c - avec nom du jour et numéro du jour dans le mois

Faire en sorte que le nom du jour soit affiché suivi de son numéro dans le mois.
Afficher 4 semaines.
Le premier du mois est le lundi 1.

Concepts pouvant être utilisés

- boucle
- switch
- fonction
    - avec paramètres
    - retournant une valeur
- int
    - opérateur +
    - opérateur -
    - opérateur *
    - parenthèses

Pour lancer le programme

```bash
make
bin/07_main 
```

Résultat attendu

```
Lundi 1
Metro
Boulot
Dodo
Mardi 2
Metro
Boulot
Dodo
Mercredi 3
Metro
Boulot
Dodo
Jeudi 4
Metro
Boulot
Dodo
Vendredi 5
Metro
Boulot
Dodo
Samedi 6
Télé
Dodo
Dimanche 7
Télé
Dodo
Lundi 8
Metro
Boulot
Dodo
Mardi 9
Metro
Boulot
Dodo
Mercredi 10
Metro
Boulot
Dodo
Jeudi 11
Metro
Boulot
Dodo
Vendredi 12
Metro
Boulot
Dodo
Samedi 13
Télé
Dodo
Dimanche 14
Télé
Dodo
Lundi 15
Metro
Boulot
Dodo
Mardi 16
Metro
Boulot
Dodo
Mercredi 17
Metro
Boulot
Dodo
Jeudi 18
Metro
Boulot
Dodo
Vendredi 19
Metro
Boulot
Dodo
Samedi 20
Télé
Dodo
Dimanche 21
Télé
Dodo
Lundi 22
Metro
Boulot
Dodo
Mardi 23
Metro
Boulot
Dodo
Mercredi 24
Metro
Boulot
Dodo
Jeudi 25
Metro
Boulot
Dodo
Vendredi 26
Metro
Boulot
Dodo
Samedi 27
Télé
Dodo
Dimanche 28
Télé
Dodo
```

## 08_main.c - nombre de jours de chaque mois

Afficher le nombre de jours de chaque mois de l'année 2021.

Concepts pouvant être utilisés

- boucle
- switch
- fonction
    - avec 1 argument
    - qui retourne une valeur

Pour lancer le programme

```bash
make
bin/08_main 
```

Résultat attendu

```
31
28
31
30
31
30
31
31
30
31
30
31
```

## 09_main.c - année 2021

Lister toutes les dates de l'année 2021.
Format attendu J/M.
Ex. 
- "1/1" pour le 1er janvier
- "27/2" pour le 27 février
- "25/10" pour le 25 octobre

Concepts pouvant être utilisés

- boucles imbriquées
- switch
- fonction
    - avec un argument
    - qui retourne une valeur

Pour lancer le programme

```bash
make
bin/09_main 
```

Résultat attendu

```
1/1
2/1
3/1
4/1
5/1
6/1
7/1
8/1
9/1
10/1
11/1
12/1
13/1
14/1
15/1
16/1
17/1
18/1
19/1
20/1
21/1
22/1
23/1
24/1
25/1
26/1
27/1
28/1
29/1
30/1
31/1
1/2
2/2
3/2
4/2
5/2
6/2
7/2
8/2
9/2
10/2
11/2
12/2
13/2
14/2
15/2
16/2
17/2
18/2
19/2
20/2
21/2
22/2
23/2
24/2
25/2
26/2
27/2
28/2
1/3
2/3
3/3
4/3
5/3
6/3
7/3
8/3
9/3
10/3
11/3
12/3
13/3
14/3
15/3
16/3
17/3
18/3
19/3
20/3
21/3
22/3
23/3
24/3
25/3
26/3
27/3
28/3
29/3
30/3
31/3
1/4
2/4
3/4
4/4
5/4
6/4
7/4
8/4
9/4
10/4
11/4
12/4
13/4
14/4
15/4
16/4
17/4
18/4
19/4
20/4
21/4
22/4
23/4
24/4
25/4
26/4
27/4
28/4
29/4
30/4
1/5
2/5
3/5
4/5
5/5
6/5
7/5
8/5
9/5
10/5
11/5
12/5
13/5
14/5
15/5
16/5
17/5
18/5
19/5
20/5
21/5
22/5
23/5
24/5
25/5
26/5
27/5
28/5
29/5
30/5
31/5
1/6
2/6
3/6
4/6
5/6
6/6
7/6
8/6
9/6
10/6
11/6
12/6
13/6
14/6
15/6
16/6
17/6
18/6
19/6
20/6
21/6
22/6
23/6
24/6
25/6
26/6
27/6
28/6
29/6
30/6
1/7
2/7
3/7
4/7
5/7
6/7
7/7
8/7
9/7
10/7
11/7
12/7
13/7
14/7
15/7
16/7
17/7
18/7
19/7
20/7
21/7
22/7
23/7
24/7
25/7
26/7
27/7
28/7
29/7
30/7
31/7
1/8
2/8
3/8
4/8
5/8
6/8
7/8
8/8
9/8
10/8
11/8
12/8
13/8
14/8
15/8
16/8
17/8
18/8
19/8
20/8
21/8
22/8
23/8
24/8
25/8
26/8
27/8
28/8
29/8
30/8
31/8
1/9
2/9
3/9
4/9
5/9
6/9
7/9
8/9
9/9
10/9
11/9
12/9
13/9
14/9
15/9
16/9
17/9
18/9
19/9
20/9
21/9
22/9
23/9
24/9
25/9
26/9
27/9
28/9
29/9
30/9
1/10
2/10
3/10
4/10
5/10
6/10
7/10
8/10
9/10
10/10
11/10
12/10
13/10
14/10
15/10
16/10
17/10
18/10
19/10
20/10
21/10
22/10
23/10
24/10
25/10
26/10
27/10
28/10
29/10
30/10
31/10
1/11
2/11
3/11
4/11
5/11
6/11
7/11
8/11
9/11
10/11
11/11
12/11
13/11
14/11
15/11
16/11
17/11
18/11
19/11
20/11
21/11
22/11
23/11
24/11
25/11
26/11
27/11
28/11
29/11
30/11
1/12
2/12
3/12
4/12
5/12
6/12
7/12
8/12
9/12
10/12
11/12
12/12
13/12
14/12
15/12
16/12
17/12
18/12
19/12
20/12
21/12
22/12
23/12
24/12
25/12
26/12
27/12
28/12
29/12
30/12
31/12
```

## 10_main.c

### Mini-cours

Chaine de caractères retournée par une fonction

Une fonction peut retourner une chaine de caractère ( `char *`).
Le résultat de cette fonction peut être utilisé par `printf()`.

Exemple.
```c
#include <stdio.h>
#include <stdlib.h>

char *fonction_qui_retourne_chaine()
{
    return "chaine";
}

void main()
{
    printf("%s\n", fonction_qui_retourne_chaine());
}
``` 

### Enoncé

A partir du résultat de l'exercice 09_main.c, modifier le programme 10_main.c pour que 
le nom du mois soit ajouté au début de chaque mois.

Ex. (voir exemple plus exhausif un peu plus bas)

```
## janvier
01/01
02/01
03/01
...

## février
01/02
02/02
03/02

## mars
01/03
02/03
03/03
...
```

### Concepts pouvant être utilisés

- boucles imbriquées
- switch
- fonction
    - avec un argument
    - qui retourne une valeur
- chaîne de caractères `char *`

### Pour lancer le programme

```bash
make
bin/10_main 
```

### Résultat attendu

```
## janvier
01/01
02/01
03/01
04/01
05/01
06/01
07/01
08/01
09/01
10/01
11/01
12/01
13/01
14/01
15/01
16/01
17/01
18/01
19/01
20/01
21/01
22/01
23/01
24/01
25/01
26/01
27/01
28/01
29/01
30/01
31/01

## février
01/02
02/02
03/02
04/02
05/02
06/02
07/02
08/02
09/02
10/02
11/02
12/02
13/02
14/02
15/02
16/02
17/02
18/02
19/02
20/02
21/02
22/02
23/02
24/02
25/02
26/02
27/02
28/02
```
Etc.. jusqu'à décembre compris

## 11_main.c

### Enoncé

Afficher chaque jour de l'année au format lisible par un humain.
Ex. `vendredi 01 janvier`

Le premier janvier 2021 tombe un vendredi.

### Concepts pouvant être utilisés

- boucles imbriquées
- switch
- fonction
    - avec un argument
    - qui retourne une valeur
- chaîne de caractères `char *`
- opérateur modulo `%`

### Pour lancer le programme

```bash
make
bin/11_main 
```

### Résultat attendu

```
vendredi 01 janvier
samedi 02 janvier
dimanche 03 janvier
lundi 04 janvier
mardi 05 janvier
mercredi 06 janvier
jeudi 07 janvier
vendredi 08 janvier
samedi 09 janvier
dimanche 10 janvier
lundi 11 janvier
mardi 12 janvier
mercredi 13 janvier
jeudi 14 janvier
vendredi 15 janvier
samedi 16 janvier
dimanche 17 janvier
lundi 18 janvier
mardi 19 janvier
mercredi 20 janvier
jeudi 21 janvier
vendredi 22 janvier
samedi 23 janvier
dimanche 24 janvier
lundi 25 janvier
mardi 26 janvier
mercredi 27 janvier
jeudi 28 janvier
vendredi 29 janvier
samedi 30 janvier
dimanche 31 janvier
lundi 01 février
mardi 02 février
mercredi 03 février
jeudi 04 février
vendredi 05 février
samedi 06 février
dimanche 07 février
lundi 08 février
mardi 09 février
mercredi 10 février
jeudi 11 février
vendredi 12 février
samedi 13 février
dimanche 14 février
lundi 15 février
mardi 16 février
mercredi 17 février
jeudi 18 février
vendredi 19 février
samedi 20 février
dimanche 21 février
lundi 22 février
mardi 23 février
mercredi 24 février
jeudi 25 février
vendredi 26 février
samedi 27 février
dimanche 28 février
lundi 01 mars
mardi 02 mars
mercredi 03 mars
jeudi 04 mars
vendredi 05 mars
samedi 06 mars
dimanche 07 mars
lundi 08 mars
mardi 09 mars
mercredi 10 mars
jeudi 11 mars
vendredi 12 mars
samedi 13 mars
dimanche 14 mars
lundi 15 mars
mardi 16 mars
mercredi 17 mars
jeudi 18 mars
vendredi 19 mars
samedi 20 mars
dimanche 21 mars
lundi 22 mars
mardi 23 mars
mercredi 24 mars
jeudi 25 mars
vendredi 26 mars
samedi 27 mars
dimanche 28 mars
lundi 29 mars
mardi 30 mars
mercredi 31 mars
jeudi 01 avril
vendredi 02 avril
samedi 03 avril
dimanche 04 avril
lundi 05 avril
mardi 06 avril
mercredi 07 avril
jeudi 08 avril
vendredi 09 avril
samedi 10 avril
dimanche 11 avril
lundi 12 avril
mardi 13 avril
mercredi 14 avril
jeudi 15 avril
vendredi 16 avril
samedi 17 avril
dimanche 18 avril
lundi 19 avril
mardi 20 avril
mercredi 21 avril
jeudi 22 avril
vendredi 23 avril
samedi 24 avril
dimanche 25 avril
lundi 26 avril
...
```
## 11b_main.c

### Enoncé

Le programme `11b_main.c` doit avoir exactement le même comportement que le programme `11_main.c`.

Les prototypes des fonctions utiles au programme  `11b_main.c` ont été préparés et documentés dans le fichier `11b_lib.h`.
Au départ le code du programme `11b_main.c` est commenté car les fonctions décrites dans le fichier `11b_lib.h` n'ont pas encore de code défini dans le fichier `11b_lib.c`.

1. dans le fichier `11b_lib.c`, écrire le codes des fonctions dont le prototype est décrit au niveau du fichier `11b_lib.h`
2. décommenter le code du programme principal : `11b_main.c`
3. le fonctionnement doit être identique au programme `11_main.c`

Ci dessous, une simplification du fichier `11b_lib.h` (pour la documentation détaillée, il faut ouvrir le fichier `11b_lib.h`)

```h
void affiche_date(int num_jour_dans_mois, int num_mois_dans_annee, int num_jour_dans_semaine);
int nombre_de_jours_par_mois(int num_mois_dans_annee);
char *nom_mois(int num_mois_dans_annee);
char *nom_jour(int num_jour_dans_semaine);
```

### Concepts pouvant être utilisés

- extraction de fonction dans une librairie
    - prototypes et documentation (fichier `nom_lib.h`)
    - définition du code des fonctions de librairie (fichier `nom_lib.c`)
- boucles imbriquées
- switch
- fonction
    - avec un argument
    - qui retourne une valeur
- chaîne de caractères `char *`
- opérateur modulo `%`

### Pour lancer le programme

```bash
make
bin/11b_main 
```

### Résultat attendu

Voir exercice `11_main.c`