#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Utilisation des prototypes pour pouvoir choisir 
// dans quel ordre écrire les fonctions
void affiche_planning_jour(int jour);
void affiche_planning_un_jour_ouvre();
void affiche_Jour_num(int jour);
bool est_jour_ouvre(int jour);
void affiche_planning_un_jour_weekend();

const int NOMBRE_JOURS_OUVRES_PAR_SEMAINE = 5;

//la semaine avec le numéro de jour de la semaine
// Voir les instructions : Readme.md

void main()
{
    for (int jour = 1; jour <= 7; jour++)
    {
        affiche_planning_jour(jour);
    }
}

void affiche_planning_jour(int jour)
{
    affiche_Jour_num(jour);
    if (est_jour_ouvre(jour))
    {
        affiche_planning_un_jour_ouvre();
    }
    else
    {
        affiche_planning_un_jour_weekend();
    }
}

void affiche_Jour_num(int jour)
{
    printf("Jour n°%d\n", jour);
}

bool est_jour_ouvre(int jour)
{
    return jour < 6;
}

void affiche_planning_un_jour_ouvre()
{
    printf("METRO\n");
    printf("BOULOT\n");
    printf("DODO\n");
}

void affiche_planning_un_jour_weekend()
{
    printf("TELE\n");
    printf("DODO\n");
}
