#include <stdio.h>
#include <stdlib.h>

// Nom du jour + numéro
// Voir les instructions : Readme.md

int NOMBRE_JOURS_OUVRES_PAR_SEMAINE = 5;


void affiche_planning_un_jour_ouvre()
{
    //
    printf("METRO\n");
    printf("BOULOT\n");
    printf("DODO\n");
}

void affiche_planning_un_jour_weekend()
{
    printf("TELE\n");
    printf("DODO\n");
}

int prochain_jour_de_semaine(int j)
{
    j++;
    if (j > 7)
    {
        j = 1;
    }
    return j;
}



int affichage_du_jour(int jour_dans_mois, int jour_dans_semaine)
{
    switch (jour_dans_mois)
    {
    case 1:
        printf("Lundi %d\n", jour_dans_semaine);
        affiche_planning_un_jour_ouvre();
        break;
    case 2:
        printf("Mardi %d\n", jour_dans_semaine);
        affiche_planning_un_jour_ouvre();
        break;
    case 3:
        printf("Mercredi %d\n", jour_dans_semaine);
        affiche_planning_un_jour_ouvre();
        break;
    case 4:
        printf("Jeudi %d\n", jour_dans_semaine);
        affiche_planning_un_jour_ouvre();
        break;
    case 5:
        printf("Vendredi %d\n", jour_dans_semaine);
        affiche_planning_un_jour_ouvre();
        break;
    case 6:
        printf("Samedi %d\n", jour_dans_semaine);
        affiche_planning_un_jour_weekend();
        break;
    case 7:
        printf("Dimanche %d\n", jour_dans_semaine);
        affiche_planning_un_jour_weekend();
        break;
    }
}


int main(int argc)
{
    int jour_du_mois = 1;
    int jour_de_semaine = 1;
    for (; jour_du_mois < 30; jour_du_mois++)
    {
        affichage_du_jour(jour_de_semaine, jour_du_mois);
        jour_de_semaine = prochain_jour_de_semaine(jour_de_semaine); 
    }
}
