#include <stdio.h>
#include <stdlib.h>
#include "11b_lib.h"

// Afficher toutes les date de l'année 2021 au format lisible par un humain
// Déclarer les prototypes de fonction dans le fichier "11_main_b_lib.h"
// Définir les fonctions dans le fichier "11_main_b_lib.c"

void main()
{
    // Le 01/01/2020 tombe un vendredi
    int num_jour_dans_semaine = 5;

     for (int mois = 1; mois <= 12; mois++)
     {
         for (int jour = 1; jour <= nombre_de_jours_par_mois(mois); jour++)
         {
             affiche_date(jour, mois, num_jour_dans_semaine);
             num_jour_dans_semaine = ((num_jour_dans_semaine++) % 7) + 1;
         }
     }
}
