#include <stdio.h>
#include <stdlib.h>

int nombre_de_jours_par_mois(int mois)
{
    switch (mois)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
        break;

    case 2:
        return 28;
        break;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
        break;
    }
}

// jour : 1 -> nombre de jour dans le mois, max à 31
// mois : 1 -> 12
void affiche_date(int jour, int mois)
{
    printf("%02d/%02d\n", jour, mois);
}

void main()
{
    for (int mois = 1; mois <= 12; mois++)
    {
        for (int jour = 1; jour <= nombre_de_jours_par_mois(mois); jour++)
        {
            affiche_date(jour, mois);
        }
    }
}