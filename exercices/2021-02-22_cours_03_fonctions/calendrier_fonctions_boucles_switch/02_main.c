#include <stdio.h>
#include <stdlib.h>

// Semaine ouvrée
// Voir les instructions : Readme.md
const int NOMBRE_JOURS_OUVRES_PAR_SEMAINE = 5;


void afficharge_un_jour()
{
    printf("METRO\n");
    printf("BOULOT\n");
    printf("DODO\n");
}

void main()
{
    for (int jour = 0; jour < NOMBRE_JOURS_OUVRES_PAR_SEMAINE; jour++)
    {
        afficharge_un_jour();
    }
}
