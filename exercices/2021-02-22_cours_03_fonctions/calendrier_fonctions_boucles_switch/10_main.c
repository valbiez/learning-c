#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int nombre_de_jours_par_mois(int mois)
{
    switch (mois)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
        break;

    case 2:
        return 28;
        break;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
        break;
    }
}

char *nom_du_mois(int mois)
{
    switch (mois)
    {
    case 1:
        return "janvier";
        break;

    case 2:
        return "févier";
        break;

    case 3:
        return "mars";
        break;

    case 4:
        return "avril";
        break;

    case 5:
        return "mai";
        break;

    case 6:
        return "juin";
        break;

    case 7:
        return "juillet";
        break;

    case 8:
        return "aout";
        break;

    case 9:
        return "septembre";
        break;

    case 10:
        return "octobre";
        break;

    case 11:
        return "novembre";
        break;

    case 12:
        return "décembre";
        break;
    }
}

// jour : 1 -> nombre de jour dans le mois, max à 31
// mois : 1 -> 12
void affiche_date(int jour, int mois)
{
    printf("%02d/%02d\n", jour, mois);
}

void main()
{
    
    for (int mois = 1; mois <= 12; mois++)
    {
        printf("\n# %s\n", nom_du_mois(mois));

        for (int jour = 1; jour <= nombre_de_jours_par_mois(mois); jour++)
        {
            affiche_date(jour, mois);
        }
    }
}
