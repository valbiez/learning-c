#include <stdio.h>
#include <stdlib.h>
/**
 * Affiche une date au format lisible par un humain.
 * 
 * Ex.
 * > affiche_date(3,3,3);
 * > mercredi 03 mars
 * 
 * num_jour_dans_mois : numéro du jour dans l'année (1-366)
 * num_mois_dans_annee : numéro du mois dans l'année (1->12)
 * num_jour_dans_semaine : numéro du jour dans la semaine (1->7)
 */

// Déclarations des fonctions du calendrier

// TODO : définir les fonctions qui ont été déclarées dans le fichier "11b_lib.h"
int nombre_de_jours_par_mois(int num_mois_dans_annee)
{
    switch (num_mois_dans_annee)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
        break;

    case 2:
        return 28;
        break;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
        break;
    }
}

char *nom_mois(int num_mois_dans_annee)
{
    switch (num_mois_dans_annee)
    {
    case 1:
        return "janvier";
        break;

    case 2:
        return "févier";
        break;

    case 3:
        return "mars";
        break;

    case 4:
        return "avril";
        break;

    case 5:
        return "mai";
        break;

    case 6:
        return "juin";
        break;

    case 7:
        return "juillet";
        break;

    case 8:
        return "aout";
        break;

    case 9:
        return "septembre";
        break;

    case 10:
        return "octobre";
        break;

    case 11:
        return "novembre";
        break;

    case 12:
        return "décembre";
        break;
    }
}

char *nom_jour(int num_jour_dans_semaine)
{
    switch (num_jour_dans_semaine)
    {
    case 1:
        return "lundi";
        break;

    case 2:
        return "mardi";
        break;

    case 3:
        return "mercredi";
        break;

    case 4:
        return "jeudi";
        break;

    case 5:
        return "vendredi";
        break;

    case 6:
        return "samedi";
        break;

    case 7:
        return "dimanche";
        break;
    }
}

void affiche_date(int num_jour_dans_mois, int num_mois_dans_annee, int num_jour_dans_semaine)
{
    printf("%s %02d %s\n", nom_jour(num_jour_dans_semaine), num_jour_dans_mois, nom_mois(num_mois_dans_annee));
}
