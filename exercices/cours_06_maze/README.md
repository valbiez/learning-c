# Maze (labyrinthe)

Cet exercice permet de revoir un certain nombre de notions vues dans les cours précédents.
L'exercice consiste en la création d'un jeu de labyrinthe dont la complexité des règles augementera au fur et a mesure de la progression de l'exercice.

Parmi celles-ci
- les conditions
- les fonctions
- les structs
- les chaines de caractères
- les tableaux
- les modules (.h / .c)

Nous introduisons ici les tests unitaires à l'aide d'un framework de test minimaliste (acutest.h).

Compiler et executer les tests.

```bash
make tests && bin/tests
```

## L'histoire

Vous êtes perdu dans un labyrinthe. 
Pour pouvoir sortir, il vous faudra trouver la clé puis la porte de sortie.

- `0` : case vide (couloir ou pièce)
- `D` : case vide (point de départ)
- `1` : mur (obstable)
- `C` : clé
- `S` : sortie
- `P` : personnage

Ex. de donjon très simple
- Case de départ
- couloir
- Porte de sortie

```
D0CS
```

Quand la partie commence, le joueur est sur la case de départ
```
P0CS
```

Le joueur se déplace 1 case à droite.
```
DPCS
```

Le joueur se déplace 1 case à droite.
```
D0PS
```
"Vous venez de récupérer la clé"

Le joueur se déplace 1 case à droite.
```
D00P
```
"Victoire! Vous avez réussi à sortir!"

## Mode facile!

Pour commencer, faisons simple.

- Le labyrinthe sera juste un couloir sur une seule dimension.
- Il n'y aura pas d'obstacles.
- Le personnage ne pourra que se déplacer à droite.
- Le personnage commence sur la case de départ (`D`).
- Quand le personnage trouve la clé (`C`), la case devient un couloir (`0`)
- Pour gagner, le personnage doit avoir ramassé la clé (`C`) et se trouver sur la case de sortie (`S`)

Les cases possibles seront :

- `0` : case vide (couloir ou pièce)
- `D` : case vide (point de départ)
- `C` : clé
- `S` : sortie
- `P` : personnage

Pour procéder aux différentes étapes, des tests ont été écrits à l'avance. Ils sont situés dans le fichier `tests.c`.

Pour rappel pour compiler la lib et exécuter les tests saisir la commande suivante :

```
make tests && bin/tests
```

Décommenter les tests de la liste 
au fur et a mesure de la progression des exercices.

```c
TEST_LIST = {
    {"01_initialiser_carte", test_01_initialiser_carte},
    //  {"02_placer_joueur", test_02_placer_joueur},
    //  {"03_deplacer_joueur_droite", test_03_deplacer_joueur_droite},
    //  {"04_arrivee_sortie_sans_cle", test_04_arrivee_sortie_sans_cle},
    //  {"05_recuperation_cle", test_05_recuperation_cle},
    //  {"06_deplacement_apres_recuperation_cle", test_06_deplacement_apres_recuperation_cle},
    //  {"07_arrivee_sortie_avec_cle", test_07_arrivee_sortie_avec_cle},
    {"string_equality", test_string_equality}, // testing unit test framework
    {NULL, NULL}};
```
### 01 - Initialiser la carte

Pour une carte donnée, afficher la carte à l'écran.

Ex. si la carte est 

```
D0S
```

La carte du jeu est 

```
D0S
```

### 02 - Placer le joueur

Pour une carte donnée, afficher la carte à l'écran.
Placer le joueur sur la case de départ.

Ex. si la carte est 
```
D0S
```

Afficher
```
P0S
```

### 03 - Déplacer le joueur à droite

A partir d'une partie commencée sur une carte

Ex. si la carte est 
```
D0S
```

Et la partie est
```
P0S
```

Le personnage se déplace de 1 case à droite.

```
DPS
```

### 04 - Arrivée sur la porte de sortie sans clé

A partir d'une partie commencée sur une carte

Ex. si la carte est 
```
DS
```

Et la partie est
```
PS
```

Demander la direction vers laquelle se déplacer.
```
Déplacement? : _
```

Le personnage est déplacé de 1 case à droite.
```
DP
```

Le programme affiche le message suivant:
"Vous devez avoir la clé pour ouvrir la porte"

### 05 - Récupération de la clé


A partir d'une partie commencée sur une carte

Ex. si la carte est 
```
DCS
```

Et la partie est
```
PCS
```

Demander la direction vers laquelle se déplacer.
```
Déplacement? : _
```

Le joueur saisit `d` puis <kbd>Enter</kbd>
```
Déplacement? : d
```

Le personnage est déplacé de 1 case à droite.
```
DPS
```

Le programme affiche le message suivant:
"Vous venez de récupérer la clé"

### 06 - Déplacement après récupération de la clé

A partir d'une partie commencée sur une carte

Ex. si la carte est 
```
DC0S
```

Et la partie est
```
PC0S
```

Demander la direction vers laquelle se déplacer.
```
Déplacement? : _
```

Le joueur saisit `d` puis <kbd>Enter</kbd>
```
Déplacement? : d
```

Le personnage est déplacé de 1 case à droite.
```
DP0S
```

Le programme affiche le message suivant:
"Vous venez de récupérer la clé"

Demander la direction vers laquelle se déplacer.
```
Déplacement? : _
```

Le joueur saisit `d` puis <kbd>Enter</kbd>
```
Déplacement? : d
```

Le personnage est déplacé de 1 case à droite.
(La case où était la clé devient un couloir)

```
D0PS
```

### 07 - Arrivée sur la sortie avec la clé

A partir d'une partie commencée sur une carte

Ex. si la carte est 
```
DCS
```

Et la partie est
```
PCS
```

Demander la direction vers laquelle se déplacer.
```
Déplacement? : _
```

Le joueur saisit `d` puis <kbd>Enter</kbd>
```
Déplacement? : d
```

Le personnage est déplacé de 1 case à droite.
```
DPS
```

Le programme affiche le message suivant:
"Vous venez de récupérer la clé"

Demander la direction vers laquelle se déplacer.
```
Déplacement? : _
```

Le joueur saisit `d` puis <kbd>Enter</kbd>
```
Déplacement? : d
```

Le personnage est déplacé de 1 case à droite.
(La case où était la clé devient un couloir)

```
D0P
```

Le programme affiche : "Victoire! Vous avez réussi à sortir!"

## Mode normal!

Maintenant, les choses vont se corser un peu!
Tu devras écrire toi-même les tests avant d'écrire du code dans `libmaze.h` ou `libmaze.c`.

Pour rappel, les tests portent sur le code de `libmaze`, il n'est pas nécessaire d'en écrire pour `maze.c`

Voici les nouvelles caractéristiques du jeu : 
- Les obstacles existent
- Le joueur peut maintenant se déplacer à gauche
- mode interactif
    - `maze.c` 
        - Le jeu demande au joueur comment il veut se déplacer (`d` : à droite, `g` : à gauche).
        - Le joueur peut fournir la carte au début du jeu en passant une chaine de caractère au main.
            - lors de l'initialisation de la partie, la carte doit au moins avoir un point de départ (`D`), une clé (`C`), et la sortie (`S`). Si ces conditions ne sont pas réunies l'état du jeu indique que la partie ne peut pas être jouée.

### 08 - Tu n'es pas passe-muraille!

Te voila face à un mur!
Si ton personnage essaye d'avancer à droite, il ne doit pas y arriver.

Ecris un test pour vérifier le comportement suivant. Puis écris le code dans `libmaze` pour le faire passer.

Jeu de départ:

```
D1S
```

Avec le personnage

```
P1S
```

Le joueur se déplace 1 case à droite.

Il doit rester au même endroit.

```
P1S
```

### 09 - A babord toute!

Te voila face à un mur!
Le personnage réalise qu'il peut aller à gauche!

Ecris un test pour vérifier le comportement suivant. Puis écris le code dans `libmaze` pour le faire passer.


Jeu de départ:

```
0D1
```

Avec le personnage

```
0P1
```

Le joueur se déplace 1 case à gauche.

```
PD1
```

### 10 - Interactif (1)

Pour le moment, ce sont des tests qui jouent à notre jeu. Nous aussi on voudrait y jouer!

Ecrire un programme `int main()` dans le fichier `maze.c` qui s'appuie sur `libmaze.h`.

Ce programme te permettra de jouer au jeu de façon interactive.

Le programme
- initialise un labyrinthe avec la carte : `"DCS"`
- Affiche la carte courante (avec le personnage)
- Invite le joueur à se déplacer
    - `d` : pour se déplacer à droite
    - `g` : pour se déplacer à gauche
    - Le joueur peut saisir son choix et valide avec <kbd>Enter</kbd>
- Le joueur se déplace dans la direction souhaitée
- Affiche la carte courante (avec le personnage)

```
Bienvenue dans le labyrinthe!

---
PCS
---

Déplacement? : _

```

Le joueur saisit `d` puis <kbd>Enter</kbd>

```
Déplacement? : d

---
DPS
---

Déplacement? : _

```

Le joueur saisit `g` puis <kbd>Enter</kbd>

```
Déplacement? : g

---
P0S
---

Déplacement? : _

```

### 11 - Interactif (2)

Continuer le mode interactif. 
- Quand un événement a lieu (Clé récupérée, Sur la sortie sans clé, Victoire), le message de l'événement est affiché.
- En cas de victoire (Arrivée sur la case de sortie (`S`)), le programme est terminé

### 12 - Interactif (3)

C'est bien triste, nous n'avons qu'une seule carte pour jouer!

- Le joueur peut fournir la carte au début du jeu en passant une chaine de caractère au `main` du programme `maze.c`.
    - lors de l'initialisation de la partie,pour être jouable, la carte doit au moins avoir un point de départ (`D`), une clé (`C`), et la sortie (`S`). Si ces conditions ne sont pas réunies l'état du jeu indique que la partie ne peut pas être jouée.

Tout d'abord, écrire un test pour vérifier qu'à son initialisation une carte est jouable. 

Ecrire le code nécessaire pour faire passer le test.

Si la carte n'est pas jouable, son état est `not_playable`.

Ensuite, modifier le `maze.c`. 
Lors de l'exécution du programme, le premier argument donné est la carte à jouer.


```bash
bin/maze D00C00S
Bienvenue dans le labyrinthe!

---
P00C00S
---

Déplacement? : _
```

Si aucun argument n'est donné, la carte par défaut est chargée (`"DCS"`)

```bash
bin/maze
Bienvenue dans le labyrinthe!

---
PCS
---

Déplacement? : _
```

En cas de carte incorrecte le programme s'arrête avec un message d'erreur.

```bash
bin/maze D0000
Carte incorrecte!
```

## Mode difficile

- plusieurs grilles verouillées (+ clés)
    - Les grilles (`G`) empèchent de passer, récolter les clés en argent  (`A`) pour ouvrir les grilles. Quand une clé en argent est utilisée, elle est détruite. Il y a au moins 2 grilles (et 2 clés en argent par carte)
- Brouillard de guerre (le joueur ne peut voir que 1 case autour de lui). Les autres cases sont remplacées par `*`
- 2D
- générateur de donjon (d'abord à 1 dimension)

```
11111
100CS
1D111
11111
```