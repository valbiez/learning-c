#include "acutest.h"
#include "libmaze.h"

void test_01_initialiser_carte(void)
{
  // Arrange
  MazeGame game;

  // Act
  game = init_carte("D0S");

  // Assert
  TEST_CHECK(strcmp(game.room, "D0S") == 0);
  TEST_MSG("game.room: %s", game.room);
}

void test_02_placer_joueur(void)
{
  // Arrange
  MazeGame game = init_carte("D0S");

  // Act
  char *carteAffichee = afficher_carte(game);

  // Assert
  TEST_CHECK(strcmp(carteAffichee, "P0S") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);

  TEST_CHECK(game.playerPosition == 0);
  TEST_MSG("game.playerPosition: %d", game.playerPosition);
}

void test_03_deplacer_joueur_droite(void)
{
  // Arrange
  MazeGame game = init_carte("D0S");

  // Act
  deplacer_a_droite(&game);
  int position_joueur = game.playerPosition;

  // Assert
  TEST_CHECK(position_joueur == 1);
  TEST_MSG("position joueur : %d", position_joueur);

  char *carteAffichee = afficher_carte(game);
  TEST_CHECK(strcmp(carteAffichee, "DPS") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);
}

void test_03b_afficher_autre_carte(void)
{
  // Arrange
  MazeGame game = init_carte("0DS");

  // Act
  int position_joueur = game.playerPosition;

  // Assert
  TEST_CHECK(position_joueur == 1);
  TEST_MSG("position joueur : %d", position_joueur);

  char *carteAffichee = afficher_carte(game);
  TEST_CHECK(strcmp(carteAffichee, "0PS") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);
}

void test_03c_afficher_autre_grosse_carte(void)
{
  // Arrange
  MazeGame game = init_carte("0000000D00000S");

  // Act
  int position_joueur = game.playerPosition;

  // Assert
  TEST_CHECK(position_joueur == 7);
  TEST_MSG("position joueur : %d", position_joueur);

  char *carteAffichee = afficher_carte(game);
  TEST_CHECK(strcmp(carteAffichee, "0000000P00000S") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);
}

void test_04_arrivee_sortie_sans_cle(void)
{
  // Arrange
  MazeGame game = init_carte("DS");

  // Act
  char *evenement = deplacer_a_droite(&game);

  // Assert
  MazeState etatJeu = etat_jeu(game);
  TEST_CHECK(etatJeu == no_key);
  TEST_MSG("etatJeu: %d", etatJeu);

  TEST_CHECK(
      strcmp(evenement, "Vous devez avoir la clé pour ouvrir la porte") == 0);
  TEST_MSG("evenement: %s", evenement);

  char *carteAffichee = afficher_carte(game);
  TEST_CHECK(strcmp(carteAffichee, "DP") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);
}

void test_05_recuperation_cle(void)
{
  // Arrange
  MazeGame game = init_carte("DCS");

  // Act
  char *evenement = deplacer_a_droite(&game);

  // Assert
  TEST_CHECK(strcmp(evenement, "Vous venez de récupérer la clé") == 0);
  TEST_MSG("evenement: %s", evenement);

  char *carteAffichee = afficher_carte(game);
  TEST_CHECK(strcmp(carteAffichee, "DPS") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);

  MazeState etatJeu = etat_jeu(game);
  TEST_CHECK(etatJeu == has_key);
  TEST_MSG("etatJeu: %d", etatJeu);
}

void test_06_deplacement_apres_recuperation_cle(void)
{
  // Arrange
  MazeGame game = init_carte("DC0S");

  // Act
  deplacer_a_droite(&game);
  deplacer_a_droite(&game);

  // Assert
  char *carteAffichee = afficher_carte(game);
  TEST_CHECK(strcmp(carteAffichee, "D0PS") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);
}

void test_07_arrivee_sortie_avec_cle(void)
{
  // Arrange
  MazeGame game = init_carte("DCS");

  // Act
  deplacer_a_droite(&game);
  char *evenement = deplacer_a_droite(&game);

  // Assert
  TEST_CHECK(strcmp(evenement, "Victoire! Vous avez réussi à sortir!") == 0);
  TEST_MSG("evenement: %s", evenement);

  char *carteAffichee = afficher_carte(game);
  TEST_CHECK(strcmp(carteAffichee, "D0P") == 0);
  TEST_MSG("carteAffichee: %s", carteAffichee);

  MazeState etatJeu = etat_jeu(game);
  TEST_CHECK(etatJeu == victory);
  TEST_MSG("etatJeu: %d", etatJeu);
}

// For more examples, see https://github.com/mity/acutest
void test_string_equality(void)
{
  char *a = "test";
  char *b = "test";

  TEST_CHECK(a == b);
  TEST_MSG("a: %s", a);
  TEST_MSG("b: %s", b);
}

TEST_LIST = {
    {"01_initialiser_carte", test_01_initialiser_carte},
    // {"02_placer_joueur", test_02_placer_joueur},
    // {"03_deplacer_joueur_droite", test_03_deplacer_joueur_droite},
    // {"03b_afficher_autre_carte", test_03b_afficher_autre_carte},
    // {"03c_afficher_autre_grosse_carte", test_03c_afficher_autre_grosse_carte},
    // {"04_arrivee_sortie_sans_cle", test_04_arrivee_sortie_sans_cle},
    // {"05_recuperation_cle", test_05_recuperation_cle},
    // {"06_deplacement_apres_recuperation_cle",
    //  test_06_deplacement_apres_recuperation_cle},
    // {"07_arrivee_sortie_avec_cle", test_07_arrivee_sortie_avec_cle},
    {"string_equality", test_string_equality}, // testing unit test framework
    {NULL, NULL}};