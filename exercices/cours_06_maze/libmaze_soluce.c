#include "libmaze.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// private functions prototypes
int positionInitialeJoueur(const char *carte);
// public functions

MazeGame init_carte(const char *carte)
{
    MazeGame resultat;
    resultat.room = strdup(carte);
    //D0S
    //position initale = 0
    //0DS
    //position initale = 1
    resultat.playerPosition = positionInitialeJoueur(carte);
    //resultat.state =
    return resultat;
}

int positionInitialeJoueur(const char *carte)
{
    int taille_carte = strlen(carte);
    for (int position_case = 0; position_case < taille_carte; position_case++)
    {
        // valeur_case
        char valeur_case = carte[position_case];
        if ('D' == valeur_case)
        {
            return position_case;
        }
    }
    return NULL;
}

char *afficher_carte(MazeGame game)
{
    char *carte_a_afficher = strdup(game.room);
    int position_courante = game.playerPosition;
    carte_a_afficher[position_courante] = 'P';
    return carte_a_afficher;
    //D0S
    //P0S
    //positionJoueur = 0

    //0DS
    //0PS
    //positionJoueur 1

    //D0000S
    //D0P00S
    //positionJoueur 2
}

char *deplacer_a_droite(MazeGame *game)
{
    game->playerPosition++;

    //On n'a pas la clé
    //et on est arrivé à la sortie
    if (game->room[game->playerPosition] == 'S' 
    && game->state == no_key)
    {
        return "Vous devez avoir la clé pour ouvrir la porte";
    }
   
      if (game->room[game->playerPosition] == 'C' 
     )
    {
        // case courante
        game->room[game->playerPosition] = '0';
        // remplacer par 0

        game->state = has_key;
        return "Vous venez de récupérer la clé";

    }



    return "";
}

MazeState etat_jeu(MazeGame game)
{
    return game.state;
}
