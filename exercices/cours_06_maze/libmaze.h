#ifndef LIB_MAZE_H
#define LIB_MAZE_H

typedef enum {no_key,has_key,victory} MazeState;

typedef struct {
  char *room;
  int playerPosition;
  MazeState state;
} MazeGame;


/**
 * Crée une nouvelle partie à partir d'une carte (chaine de caractères).
 * Retourne le jeu initialisé.
 */
MazeGame  init_carte(const char *carte);

/**
 * Affiche la carte avec le joueur positionné dessus.
 * Retourne la chaine de caractères à afficher.
 */
char *afficher_carte( MazeGame game);

/**
 * Déplace le joueur d'une case à droite.
 * Si le joueur tombe sur une case spéciale (C ou S),
 * un événement peut être produit.
 * Retourne l'événement produit
 */
char *deplacer_a_droite(MazeGame *game);

/**
 * Retourne l'état du jeu en cours (pas de clé, clé trouvée, victoire).
 */
MazeState etat_jeu( MazeGame game);

#endif