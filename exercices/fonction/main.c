#include <stdio.h>
#include <stdlib.h>



int triple(int nombre){

    return 3 * nombre;
}    

double foo(double euros){

    //double francs = 0; 
    //francs = 6.55957 * euros;
    return  6.55957 * euros;
}    

double conversion(double francs){
    return 6.55957 / francs;
}


void aireRectangle(double largeur, double hauteur){

    double aire = 0;
    
    aire = largeur * hauteur;
    printf("Rectangle de largeur %f et hauteur %f. Aire = %f\n", largeur, hauteur, aire);
}    

int main(){

    int nombreEntre = 0, nombreTriple = 0;
    
    printf("Entrez un nombre... ");
    scanf("%d", &nombreEntre);
    
    nombreTriple = triple(nombreEntre);
    printf("Le triple de ce nombre est %d\n", nombreTriple);
    
    printf("10 euros = %fF\n", conversion(10));
    printf("50 euros = %fF\n", conversion(50));
    printf("100 euros = %fF\n", conversion(100));
    printf("200 euros = %fF\n", conversion(200));

    printf("10 francs = %fE\n", foo(10));
    printf("50 francs = %fE\n", foo(50));
    
    aireRectangle(5, 10);
    aireRectangle(2.5, 3.5);
    aireRectangle(4.2, 9.7);
       

    return 0;
}


// ex de fonction:
// int donne_MAX(int argc, char *argv[])
// {
//     int resultat;
//     if (argc > 2)
//     {
//         char *valeur = argv[2];
//         // Conversion de la chaine de caractères (texte) en entier
//         resultat = atoi(valeur);
//         printf("La valeur max est de %d\n", resultat);
//     }
//     else
//     {
//         printf("Quelle valeur max veut tu\n");
//         scanf("%d", &resultat);
//     }
//     return resultat;
// }
