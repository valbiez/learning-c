
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
    printf("\nBoucle for.\n");

    // for  (valeur initiale de l'index ; condition de continuation ; incrémentation de l'index)
    for (int argument = 1; argument < argc; argument++)
    {
        printf("argument %d : %s\n", argument, argv[argument]);
    }

    return 0;
}