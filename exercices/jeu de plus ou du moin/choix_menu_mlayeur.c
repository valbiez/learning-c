#include <stdio.h>
#include <stdlib.h>
#include <time.h>


//////////MENU////1-joueur/////2-joueurs/////////



//variable:
int compteur = 0;

//fonction:
void jouer(int nombreMystere, int MAX, int coup_max)
{

    //La boucle du programme.
    int nombreEntre;
    do
    {
        //on propose au joueur de deviner
        // On demande le nombre
        printf("Quel est le nombre ? ");
        scanf("%d", &nombreEntre);

        compteur++;

        //on regarde si le nombre est suppérieur à la valeur MAX
        if (nombreEntre > MAX)
        {
            printf("Je t'avais dit que c'était %d\n", MAX);
        }
        //on regarde si le compteur est supp ou égale à coup max
        else if (compteur >= coup_max)
        {
            printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais\n", coup_max);
        }
        // On compare le nombre entre avec le nombre mystère
        else if (nombreMystere > nombreEntre)
        {
            printf("C'est plus !\n\n");
        }
        else if (nombreMystere < nombreEntre)
        {
            printf("C'est moins !\n\n");
        }
        else
        {
            printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
        }

        //si le nombre est trouver et si compteur est à 5 coups,
    } while (nombreEntre != nombreMystere && compteur < coup_max);
}

void jeu_1_joueur()
{
    int MAX = 50;
    int MIN = 0;
    int coup_max = 10;

    srand(time(NULL));
    int nombreMystere = (rand() % (MAX - MIN + 1)) + MIN;
    jouer(nombreMystere, MAX, coup_max);
}

void jeu_2_joueur()
{
    int MAX = 50;
    printf("Quelle valeur max veux-tu?\n");
    scanf("%d", &MAX);

    int coup_max = 10;
    printf("Combien de coup max veut tu ?\n");
    scanf("%d", &coup_max);

    int nombreMystere = 0;
    printf("Quel est ton nombre mystère?\n");
    scanf("%d", &nombreMystere);

    jouer(nombreMystere, MAX, coup_max);
}

int main()
{
    int choixMenu;

    printf("=== MenuJoueurs ===\n\n");
    printf("1. one playeur\n");
    printf("2. two playeur\n");
    printf("\nVotre choix ? ");
    scanf("%d", &choixMenu);
    printf("\n");

    switch (choixMenu)
    {

    case 1:
        printf("Vous avez choisi one playeur !\n");
        jeu_1_joueur();
        break;

    case 2:
        printf("Vous avez choisi two playeur !\n");
        jeu_2_joueur();
        break;

    default:
        printf("Vous n'avez pas choisi de menu, vous ne pouvez pas jouer !\n");
        break;
    }

    return 0;
}
