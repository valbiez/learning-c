#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//nombre mystére
int nombreMystere = 0;
// coup max
const int DEFAULT_COUP_MAX = 5;

int main(int argc, char *argv[])
{

    // valeurs
    int MIN = 0;
    int MAX = 1000;
    int coup_max = DEFAULT_COUP_MAX;

    if (argc > 1)
    {
        char *coup_max_text = argv[1];
        // Conversion de la chaine de caractères (texte) en entier
        coup_max = atoi(coup_max_text);
        printf("Valeur des coups max est de: %d\n", coup_max);
    }

    if (argc > 2)
    {
        char *MAX_text = argv[2];
        // Conversion de la chaine de caractères (texte) en entier
        MAX = atoi(MAX_text);
        printf("Valeur  max est de: %d\n", MAX);
    }

    // Génération du nombre aléatoire
    srand(time(NULL));
    nombreMystere = rand() % 500 + 0; // nombre entre 0 et 500

    jeu(MAX, coup_max);

    return 0;
}

void jeu(int max_autorise, const int COUP_MAX)
{
    int nombreEntre = 0;
    //comteur de coup
    int compteur = 0;
    //La boucle du programme.
    do
    {
        // on propose au joueur de deviner
        // On demande le nombre
        printf("Quel est le nombre ? ");
        scanf("%d", &nombreEntre);

        compteur++;

        //on regarde si le nombre est suppérieur à la valeur MAX
        if (nombreEntre > max_autorise)
        {

            printf("Je t'avais dit que la valeur max était %d\n", max_autorise);
        }
        //on regarde si le compteur est supp ou égale à coup max
        else if (compteur >= COUP_MAX)
        {
            printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais\n", COUP_MAX);
        }
        // On compare le nombre entre avec le nombre mystère
        else if (nombreMystere > nombreEntre)
        {
            printf("C'est plus !\n\n");
        }
        else if (nombreMystere < nombreEntre)
        {
            printf("C'est moins !\n\n");
        }
        else
        {
            printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
        }

        //si le nombre est trouver et si compteur est à 5 coups,
    } while (nombreEntre != nombreMystere && compteur < COUP_MAX);
}