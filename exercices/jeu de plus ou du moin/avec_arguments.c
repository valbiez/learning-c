#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//variables:
// valeurs
int MIN = 0;

int MAX = 1000;

//nombre mystére
int nombreMystere = 0, nombreEntre = 0;

//comteur de coup
int compteur = 0;
int coup_max = 0;

//fonctions:
int donne_argument(int argc, char *argv[], int position, char *question)
{
    int resultat;
    if (argc > position)
    {
        char *valeur = argv[position];
        // Conversion de la chaine de caractères (texte) en entier
        resultat = atoi(valeur);
    }
    else
    {
        printf(question);
        scanf("%d", &resultat);
    }
    return resultat;
}

int donne_argument_avec_valeur_par_defaut(int argc, char *argv[], int position, int valeur_par_defaut)
{
    if (argc > position)
    {
        char *valeur = argv[position];
        // Conversion de la chaine de caractères (texte) en entier
        return atoi(valeur);
    }
    else
    {
        return valeur_par_defaut;
    }
}

int generation_nombre_aleatoire(int valeur_min, int valeur_max)
{
    srand(time(NULL));
    return rand() % valeur_max + valeur_min;
}


int donne_argument_avec_valeur_aleatoire(int argc, char *argv[], int position, int valeur_max, int valeur_min)
{
    int resultat;
    if (argc > position)
    {
        char *valeur = argv[position];
        // Conversion de la chaine de caractères (texte) en entier
        resultat = atoi(valeur);
    }
    else
    { 
        resultat = generation_nombre_aleatoire(valeur_min, valeur_max);
    }
    return resultat;
}

int main(int argc, char *argv[])
{
    coup_max = donne_argument(
        argc, argv,
        1,
        "Combien de coup max veut tu?\n");

    MAX = donne_argument(
        argc, argv,
        2,
        "Quelle valeur max veut tu?\n");

    nombreMystere = donne_argument_avec_valeur_aleatoire(
        argc, argv,
        3,
        MAX, 0);

    printf("Vous devez trouver un nombre entre 0 et %d en maximum %d coups\n", MAX, coup_max);

    //La boucle du programme.
    do
    {
        //on propose au joueur de deviner
        // On demande le nombre
        printf("Quel est le nombre ? ");
        scanf("%d", &nombreEntre);

        compteur++;

        //on regarde si le nombre est suppérieur à la valeur MAX
        if (nombreEntre > MAX)
        {
            printf("Je t'avais dit que la valeur max était %d\n", MAX);
        }
        //on regarde si le compteur est supp ou égale à coup max
        else if (compteur >= coup_max)
        {
            printf("Perdu,Tu feras mieux la prochaine fois (tu as fait %d essais\n", coup_max);
        }
        // On compare le nombre entre avec le nombre mystère
        else if (nombreMystere > nombreEntre)
        {
            printf("C'est plus !\n\n");
        }
        else if (nombreMystere < nombreEntre)
        {
            printf("C'est moins !\n\n");
        }
        else
        {
            printf("Bravo, tu as trouvé en %d coups!!!!\n\n", compteur);
        }

        //si le nombre est trouver et si compteur est à 5 coups,
    } while (nombreEntre != nombreMystere && compteur < coup_max);

    return 0;
}
