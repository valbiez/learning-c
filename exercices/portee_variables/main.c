
#include <stdio.h>
#include <stdlib.h>

// https://riptutorial.com/c/example/10900/parameters-are-passed-by-value

// dans ce cas une nouvelle variable est créée en faisant une copie
void essaye_de_changer_nombre_par_valeur(int nombre){
    int nombre=5;
}

// Utiliser des adresses dans les paramètres permet de changer des valeurs
void change_nombre_par_reference(int * c){
    // en déréférencant l'adresse c,
    // on accède à l'espace mémoire de celle-ci
    // quand on lui change la valeur, elle est bien modifiée
    // même après avoir terminé l'appel de la fonction
    *c= 5;
}

void main(){

    int a=1;
    // on passe la valeur de la variable a
    essaye_de_changer_nombre_par_valeur(a);
    printf("a : %d\n",a);
    // a n'a pas changé

    // on passe la référence (adresse) de la variable a
    change_nombre_par_reference(&a);
    printf("a : %d\n",a);
    // a a changé

    // Voir le jeu du plus ou moins : scanf() utilise ce principe

}

