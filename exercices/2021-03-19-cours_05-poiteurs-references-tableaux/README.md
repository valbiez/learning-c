# Pointeurs et tableaux

## 01_main.c - afficher la première lettre du jour au lieu du jour complet

Indice : utiliser les pointeurs ou tableaux.
Possible aussi de réutiliser sous forme de lib (.h ...)

Avant

```
vendredi 01 janvier
samedi 02 janvier
dimanche 03 janvier
lundi 04 janvier
mardi 05 janvier
mercredi 06 janvier
jeudi 07 janvier
vendredi 08 janvier
...
```

Après

```
v 01 janvier
s 02 janvier
d 03 janvier
l 04 janvier
m 05 janvier
m 06 janvier
j 07 janvier
v 08 janvier
...
```