#include <stdio.h>
#include <stdlib.h>

// Afficher toutes les date de l'année 2021 au format lisible par un humain

// TODO : au lieu d'afficher le nom complet du jour, essayer d'afficher que sa première lettre
// Indice : Utiliser les pointeurs ou les tableaux

char *nom_jour_de_semaine(int jour)
{
    switch (jour)
    {
    case 1:
        return "lundi";
        break;

    case 2:
        return "mardi";
        break;

    case 3:
        return "mercredi";
        break;

    case 4:
        return "jeudi";
        break;

    case 5:
        return "vendredi";
        break;

    case 6:
        return "samedi";
        break;

    case 7:
        return "dimanche";
        break;
    }
}


char initial_nom_jour_de_semaine(int jour)
{
    //printf("## initial_nom_jour_de_semaine(jour=%d)\n", jour);
    char* nom_jour = nom_jour_de_semaine(jour);
    return nom_jour[0];
}



int prochain_jour_de_semaine(int jour)
{
    jour++;
    if (jour > 7)
    {
        jour = 1;
    }
    return jour;
}


int nombre_de_jours_par_mois(int mois)
{
    switch (mois)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
        break;

    case 2:
        return 28;
        break;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
        break;
    }
}

char *nom_du_mois(int mois)
{
    switch (mois)
    {
    case 1:
        return "janvier";
        break;

    case 2:
        return "févier";
        break;

    case 3:
        return "mars";
        break;

    case 4:
        return "avril";
        break;

    case 5:
        return "mai";
        break;

    case 6:
        return "juin";
        break;

    case 7:
        return "juillet";
        break;

    case 8:
        return "aout";
        break;

    case 9:
        return "septembre";
        break;

    case 10:
        return "octobre";
        break;

    case 11:
        return "novembre";
        break;

    case 12:
        return "décembre";
        break;
    }
}

// jour : 1 -> nombre de jour dans le mois, max à 31
// mois : 1 -> 12
void affiche_date(int jour)
{
    printf("%02d\n", jour);
}

void date_jj_mm(int jour, int mois)
{
    printf("%02d/%02d\n", jour, mois);
}

void date_jj_mois(int jour, int mois)
{
    printf("%02d %s\n", jour, nom_du_mois(mois));
}

void date_jour_mois(int jour_de_la_semaine, int jour, int mois)
{
    printf("%c %02d %s\n", initial_nom_jour_de_semaine(jour_de_la_semaine), jour, nom_du_mois(mois));
}

void main()
{
    int jour_de_la_semaine = 5;

    for (int mois = 1; mois <= 12; mois++)
    {
        for (int jour = 1; jour <= nombre_de_jours_par_mois(mois); jour++)
        {
            date_jour_mois(jour_de_la_semaine, jour, mois);
            jour_de_la_semaine = prochain_jour_de_semaine(jour_de_la_semaine);
        }
    }
}
