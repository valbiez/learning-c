# Grouper des données avec des structs

## 12_main.c - réutilisation de fonctions et introduction aux structs

Le code pour récupérer les informations de la date du jour est déjà présent dans le programme  `12_main.c` (utilisation de `<time.h>`).

Réutiliser le code de l'exercice précédent (`11b_lib_calendar.h`) pour afficher la date du jour.

### Concepts pouvant être utilisés

- réutilisation de fonction dans une librairie
    - directive `#include`
    - prototypes et documentation (fichier `11b_lib_calendar.h`)
- `struct`
    - opérateur `.`
- pointeurs
    - opérateur `*`
    - références opérateur `&`
    - opérateur `->`
   

### Pour lancer le programme

```bash
make
bin/12_main 
```

### Résultat attendu

Si la date du jour est le 6 mars 2021

```
samedi 06 mars
```

## 12b_main.c

Dans l'exercice `12_main.c`, le code pour récupérer la date du jour est compliqué (9 lignes).
Il pourrait être plus simple si on pouvait faire la même chose en 1 ligne!

Le problème c'est que jusque là on a vu qu'une fonction ne pouvait retourner qu'une seule valeur!

### Cours : Les `struct` à la rescousse! 

Voir le cours `2021-03-06-cours_04.md`.

### Prototypes pour l'exercice

Prototypes simplifiés (la version avec la documentation complète est dans `12b_lib.h`)

```h
// Définition d'une struct
struct simple_date
{
    int num_jour_dans_mois;
    int num_jour_dans_semaine;
    int num_mois_dans_annee;
};

// Définition d'une fonction qui retourne une struct de type `simple_date`
struct simple_date current_date();

// Définition d'une fonction qui prend un paramètre struct de type `simple_date`
void affiche_date_by_struct(struct simple_date date);
```

### Enoncé

A partir des informations des prototypes du fichier `12b_lib.h`, et de la lib `11b_lib_calendar.h`, 
compléter le code dans le fichier `12b_lib.c`.

1. dé-commenter les lignes de code dans le programme principal `12b_main.c`
2. écrire le code des fonctions suivantes dans `12b_lib.c`
- `current_date()`
- `affiche_date_by_struct(struct simple_date date)` 

### Pour lancer le programme

```bash
make
bin/12b_main 
```

### Résultat attendu

```
Affiche la date avec affiche_date(int, int, int)
mercredi 03 mars

Affiche la date avec affiche_date_by_struct(struct simple_date)
mercredi 03 mars
```

## 13_main.c Géométrie en 2D

Utilisons maintenant les `struct` pour résoudre un problème différent.

Nous voulons représenter des points sur un plan en 2D (plan cartésien). Nous voulons calculer la longeur d'un segment.

### Explication des concepts géométriques de points et segments

Un **point** en 2D dispose de 2 coordonnées
- x : coordonnée horizontale (abscisse)
- y : coordonnée verticale (ordonnée)

Par exemple voici un **point** avec les coordonnées suivantes
- x = 1
- y = 3

![Point A avec coordonnées x = 1 , y = 3](assets/point_a.png)

Un **segment** (un "trait") est une ligne droite qui relie deux **points**.

On peut considérer qu'un **segment** est composé de points
- origine : point de départ
- fin : point d'arrivée


Par exemple voici un **segment** avec les coordonnées suivantes
- origine = point(x=1, y=3)
- fin = point(x=5, y=6)

![Segment partant de (x=1,y=3) et se terminant à (x=5,y=6)](assets/segment_a_b.png)

### Concepts pouvant être utilisés

- réutilisation de fonction dans une librairie
    - directive `#include`
    - prototypes et documentation (fichier `.h`)
- `struct` : permet de grouper des données dans une même variable
    - opérateur `.`
- `double` : nombre décimal
    - `printf("%f", double_var)` : pour afficher les nombres décimaux, il faut utiliser `%f`
- `#include <math.h>` : fonctions mathématiques (voir https://devdocs.io/c/numeric/math)
    - `pow(double nombre, double exposant)` : calculer la puissance d'un nombre décimal
        - Ex. `pow(5, 2)` vaut 5² = 5 * 5 = 25
        - Ex. `pow(3, 2)` vaut 3² = 3 * 3 = 9
        - Ex. `pow(3, 3)` vaut 3³ = 3 * 3 * 3 = 27
    - `sqrt(double nombre)` : calculer la racine carrée d'un nombre décimal
        - Ex. `sqrt(9)` vaut √9 = 3
        - Ex. `sqrt(25)` vaut √25 = 5

### Enoncé

Calculer la longueur d'un segment ayant pour extremités les points.

- point a
    - x = 1
    - y = 3
- point b
    - x = 5
    - y = 6

![Image : Plan cartésien le segment A B](assets/segment_a_b.png)

Pour visualiser interactivement : https://www.mathsisfun.com/data/cartesian-coordinates-interactive.html
- cliquer sur le bouton "Edit" et coller : (1.00,3.00), (5.00,6.00)
- cliquer sur le bouton "Sides"
- cliquer sur le bouton Coords

Pour résoudre le problème, nous allons utiliser le théorème de Pythagore. 

Pour pouvoir effectuer ce calcul mathématique, nous pouvons utiliser des fonctions proposées par la librairie standard `<math.h>`

#### Fonctions mathématiques

En C, la librairie `<math.h>` dispose de fonctions qui nous permettront de calculer le carré (puissance avec exposant 2) et la racine carrée de nombre décimaux.

Ex.

```c
// calculer le carré du nombre décimal 3
pow(3, 2);
9

// calculer le carré du nombre décimal 5
pow(5, 2);
25

// calculer la racine carrée du nombre décimal 9
sqrt(9);
3

// calculer la racine carrée du nombre décimal 25
sqrt(25);
5
```

Attention! pour pouvoir être compilé, il peut être nécessaire d'ajouter `-lm` à la commande `gcc`.

Ex.

```bash
gcc 13_lib_2D.c 13_main.c -o bin/13_main -lm
```

### Résolution du problème sans structs

TL;DR : Voici l'algorithme du calcul de longueur du segment AB.

```c
// point A
double ax = 1;
double ay = 3;

// point B
double bx = 5;
double by = 6;

double longueur_ab = sqrt(pow(bx - ax, 2) + pow(by - ay, 2));
```

Il n'est pas nécessaire de comprendre l'explication ci-dessus pour résoudre l'exercice. Pour plus de détails voir les chapitres optionnels.

### Résolution avec des structs

En s'inspirant de l'algorithme du chapitre précédent, utiliser les structs pour représenter les notions de **point** et **segment**.

Définition simplifiée des `structs` (définition détaillée dans `13_lib_2D.h`)

```h
struct point_2d
{
    double x;
    double y;
};

struct segment_2d
{
    struct point_2d origine;
    struct point_2d fin;
};

struct point_2d new_point_2d(double x, double y);
struct segment_2d new_segment(struct point_2d origine, struct point_2d fin);
double longueur_segment(struct segment_2d segment_param);
```

Voici un exemple d'utilisation de la libraire créée.

```c
    // Création d'un point 2D
    struct point_2d point_a = new_point_2d(1, 3);

    // Création d'un point 2D
    struct point_2d point_b = new_point_2d(5, 6);

    // Création d'un segment à partir de 2 points 2D
    struct segment_2d segment_a = new_segment(point_a, point_b);

    // Calcul de la longueur du segment
    double longueur_segment_a = longueur_segment(segment_a);

    // Affichage du résultat
    printf("Longueur du segment %f\n", longueur_segment_a);
```

### A faire

Dans le fichier `13_lib_2D.c`, écrire le code des fonctions suivantes permettant au programme de s'exécuter.

- `struct pcdoint_2d new_point_2d(double x, double y){...}`
- `struct segment_2d new_segment(struct point_2d origine, struct point_2d fin){...}`
- `double longueur_segment(struct segment_2d segment_param){...}`


### Pour lancer le programme

```bash
make
bin/13_main 
```

### Résultat attendu

```
Longueur du segment 5.000000
```

### (optionnel) Explication détaillée de la résolution 

Tout d'abord, réfléchissons au problème visuellement.

Pour visualiser interactivement : https://www.mathsisfun.com/data/cartesian-coordinates-interactive.html
- cliquer sur le bouton "Edit" et coller : (1.00,3.00), (5.00,6.00)
- cliquer sur le bouton "Sides"
- cliquer sur le bouton Coords

Dans l'exemple représenté sur l'image ci-dessous, l'outil en ligne nous permet de vérifier que le segment "AB" mesure 5.0.
Cette donnée nous permettra de vérifier le résultat de notre programme.

![Image : Plan cartésien le segment A B](assets/segment_a_b.png)

Comment calculer la longueur du segment maintenant?

Il y a très longtemps, un illustre personnage de l'antiquité nommé Pythagore a produit un théorème qui permet de calculer la longueur de l'hypoténuse d'un triangle rectangle.

Un **triangle rectangle** est un triangle dont un des côté a un **angle droit** (90°).

**L'hypoténuse** est le côté opposé à l'angle droit dans un triangle rectangle.

Le théorème est énoncé ainsi:
> Dans un **triangle rectangle**, le carré de la longueur de **l’hypoténuse** est égal à la somme des carrés des longueurs des deux autres côtés.

Ce qui se traduit par l'équation suivante:
> Si un triangle ABC est rectangle en C, alors 
> > AB² = AC² + BC²

Des maths! Pas de panique! Les langages de programmation sont plutôt à l'aise avec ces concepts.

Dans notre exemple, on veut mesurer la longueur du segment AB.

![Image : Triangle ABC pour calculer la longueur du segment AB](assets/points_segment_longueur_pythagore.png)

- En reconstruisant un triangle rectangle, nous pourrons connaitre la longueur des 2 autres côtés du triangle
    1. un côté **horizontal** : AC
    2. un côté **vertical** : BC
- Pour calculer la longueur de AC : on soustrait uniquement les coordonnées horizontales du point B et du point A
    - avec B ( x = 5, y = 6 ) et A ( x = 1, y = 3)
        - soit : B.x - A.x
            - 5 - 1 = 4
                - longueur AC = 4
- Pour calculer la longueur de BC : on soustrait uniquement les coordonnées verticales du point B et du point A
    - avec B ( x = 5, y = 6 ) et A ( x = 1, y = 3)
        - soit : B.y - A.y
            - 6 - 3 = 3
                - longueur BC = 3
- Nous connaissons maintenant la longueur de chacun des 2 autres côtés du triangle
    - > AC = 4, BC = 3
    - Rappelons nous du théorème de pythagore
    - > AB² = AC² + BC²
    - Remplaçons les variables AC et BC dans le théorème de pythagore
    - > AB² = 3² + 4²
    - > AB² = 9 + 16
    - > AB² = 25
- Problème! ce n'est pas AB² qui nous intéresse mais AB!
    - Appliquons la racine carrée de AB² pour obtnire la valeur de AB
    - > AB = √(AB²)
    - > AB = √(25)
    - > AB = 5

![Image : Plan cartésien illustrant l'énoncé et une piste de solution utilisant le théorème de pythagore](assets/points_segment_longueur_pythagore.png)

La formule de calcul finale peut s'écrire ainsi :

> √((B.x - A.x)² + (B.y - A.y)²)

#### Explication détaillée de l'algorithme en C

A l'aide des fonctions `pow` et `sqrt` voici comment le calcul de la longueur du segment peut-être décomposé.

```c
// point A
double ax = 1;
double ay = 3;

// point B
double bx = 5;
double by = 6;

double longueur_ac = bx - ax; 
double longueur_bc = by - ay;

double longueur_ac_carre = pow(longueur_ac, 2);
double longueur_bc_carre = pow(longueur_bc, 2);

double somme_des_carres_des_cotes = longueur_ac_carre + longueur_bc_carre;

double longueur_hypotenuse_ab = sqrt(somme_des_carres_des_cotes);
```

Soit en version condensée (sans les variables intermédiaires).

```c

// point A
double ax = 1;
double ay = 1;

// point B
double bx = 5;
double by = 6;

double longueur_ab = sqrt(pow(bx - ax, 2) + pow(by - ay, 2));

```
