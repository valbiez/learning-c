#include <stdio.h>
#include <stdlib.h>
#include "11b_lib_calendar.h"

// Déclarations des fonctions du calendrier

int nombre_de_jours_par_mois(int num_mois_dans_annee)
{
    switch (num_mois_dans_annee)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
        break;

    case 2:
        return 28;
        break;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
        break;
    }
}

void affiche_date(int num_jour_dans_mois, int num_mois_dans_annee, int num_jour_dans_semaine)
{
    printf("%s %02d %s\n", nom_jour(num_jour_dans_semaine), num_jour_dans_mois, nom_mois(num_mois_dans_annee));
}

char *nom_jour(int num_jour_dans_semaine)
{
    switch (num_jour_dans_semaine)
    {
    case 1:
        return "lundi";
    case 2:
        return "mardi";
    case 3:
        return "mercredi";
    case 4:
        return "jeudi";
    case 5:
        return "vendredi";
    case 6:
        return "samedi";
    case 7:
        return "dimanche";
    default:
        return "";
    }
}

char *nom_mois(int num_mois_dans_annee)
{
    switch (num_mois_dans_annee)
    {
    case 1:
        return "janvier";
    case 2:
        return "février";
    case 3:
        return "mars";
    case 4:
        return "avril";
    case 5:
        return "mai";
    case 6:
        return "juin";
    case 7:
        return "juillet";
    case 8:
        return "août";
    case 9:
        return "septembre";
    case 10:
        return "octobre";
    case 11:
        return "novembre";
    case 12:
        return "décembre";
    default:
        return "";
    }
}
