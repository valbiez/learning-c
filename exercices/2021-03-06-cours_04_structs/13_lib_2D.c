#include <math.h>
#include "13_lib_2D.h"

struct point_2d new_point_2d(double x, double y)
{
    struct point_2d point;
    point.x = x;
    point.y = y;
    return point;
}

struct segment_2d new_segment(struct point_2d origine, struct point_2d fin)
{
    struct segment_2d segment;
    segment.origine = origine;
    segment.fin = fin;
    return segment;
}

double longueur_segment(struct segment_2d segment_param)
{
    // point A
    double ax = segment_param.origine.x;
    double ay = segment_param.origine.y;

    // point B
    double bx = segment_param.fin.x;
    double by = segment_param.fin.y;

    double longueur_ab = sqrt(pow(bx - ax, 2) + pow(by - ay, 2));

    return longueur_ab;
}