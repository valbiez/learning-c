#include <stdio.h>
#include <stdlib.h>
#include "13_lib_2D.h"

// Calcul de la longueur d'un segment dans un plan cartésien
// Pour visualiser : https://www.mathsisfun.com/data/cartesian-coordinates-interactive.html
// Edit : (1.00,3.00), (5.00,6.00), (5.00,3.00)
// Sides
// Coords
void main()
{
    //Le code suivant pourra être décommenté quand les fonctions auront été définies dans le fichier "13_lib_2D.c"

    struct point_2d point_a = new_point_2d(1, 3);
    struct point_2d point_b = new_point_2d(5, 6);
    struct segment_2d segment_a = new_segment(point_a, point_b);

    double longueur_segment_a = longueur_segment(segment_a);

    printf("Longueur du segment %f\n", longueur_segment_a);
}
