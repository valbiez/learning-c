#include <stdio.h>
#include <stdlib.h>

#include "11b_lib_calendar.h"
#include "12b_lib.h"

// Récupération et affichage de la date courante

void main()
{

    //TODO : décommenter les ligne suivante après avoir inclus les librairies manquantes

    struct simple_date date_courante = current_date();

    // printf("Affiche la date avec affiche_date(int, int, int)\n");
    // affiche_date(date_courante.num_jour_dans_mois, date_courante.num_mois_dans_annee, date_courante.num_jour_dans_semaine);

    // printf("\n");
    // printf("Affiche la date avec affiche_date_by_struct(struct simple_date)\n");
    affiche_date_by_struct(date_courante);
}
