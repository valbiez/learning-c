#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "11b_lib_calendar.h"

// Afficher la date du jour courant au format lisible par un humain
// Réutiliser le code de l'exercice 11 sous forme de librairie

void main()
{
    // Voir readme pour les instructions

    // Récupération de la date courante grâce à <time.h>
    time_t rawtime;
    time(&rawtime);
    struct tm *currentTimeInfo;
    currentTimeInfo = localtime(&rawtime);

    int num_jour_dans_mois = currentTimeInfo->tm_mday;
    int num_jour_dans_semaine = currentTimeInfo->tm_wday;
    // tm_wday = nombre de jours depuis dimanche (0->6)
    // Ex.
    // mercredi = 3
    // dimanche = 0
    if (num_jour_dans_semaine == 0)
        num_jour_dans_semaine = 7;
    int num_mois_dans_annee = currentTimeInfo->tm_mon + 1;

    //TODO : décommenter la ligne suivante après avoir inclus la librairie manquante

    affiche_date(num_jour_dans_mois, num_mois_dans_annee, num_jour_dans_semaine);
}