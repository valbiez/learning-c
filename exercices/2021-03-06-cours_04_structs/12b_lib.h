#ifndef LIB_12B
#define LIB_12B

// Prototypes des fonctions du calendrier

/**
 * Structure simplifiée pour représenter une date.
 */
struct simple_date
{
    /** Numéro du jour dans le mois (1->31).*/
    int num_jour_dans_mois;
    /** Numéro du jour dans la semaine (1->7).*/
    int num_jour_dans_semaine;
    /** Numéro du mois dans l'année (1->12).*/
    int num_mois_dans_annee;
};

/**
 * Modifie la référence de `simple_date` en paramètre avec les information de la date du jour.
 * 
 * Ex. (si la date du jour est le jeudi 25 mars 2021)
 * > struct simple_date date_courante = current_date();
 * 
 * > date_courante.num_jour_dans_mois; // 25 
 * > date_courante.num_jour_dans_semaine; // 4
 * > date_courante.num_mois_dans_annee; // 3
 */
struct simple_date current_date();

/**
 * Affiche la date au format lisible par un humain.
 * 
 * Ex. 
 * > struct simple_date date_courante;
 * > date_courante.num_jour_dans_mois = 25; 
 * > date_courante.num_jour_dans_semaine = 4;
 * > date_courante.num_mois_dans_annee = 3;
 * > affiche_date_by_struct(date_courante);
 * > "jeudi 25 mars"
 * 
 * date : struct de type `simple_date`
 */
void affiche_date_by_struct(struct simple_date date);

#endif