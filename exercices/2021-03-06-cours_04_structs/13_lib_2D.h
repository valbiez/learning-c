#ifndef LIB13_2D
#define LIB13_2D

/**
 * Point dans un espace à 2 dimensions (système de coordonnées cartesiennes)
 * Voir https://fr.wikipedia.org/wiki/Coordonn%C3%A9es_cart%C3%A9siennes.
 */
struct point_2d
{
    /** Coordonnée horizontale (abscisse).*/
    double x;
    /** Coordonnée verticale (abscisse).*/
    double y;
};

/**
 * Segment dans un espace à 2 dimensions (système de coordonnées cartesiennes)
 * Voir https://fr.wikipedia.org/wiki/Coordonn%C3%A9es_cart%C3%A9siennes.
 */
struct segment_2d
{
    /** Point d'origine du segment.*/
    struct point_2d origine;
    /** Point d'arrivée du segment.*/
    struct point_2d fin;
};

/**
 * Construit un point 2D à partir de ses coordonnées.
 * 
 * x : coordonnée horizontale (abscisse)
 * y : coordonnée verticale (ordonnée)
 */
struct point_2d new_point_2d(double x, double y);

/**
 * Construit un segment 2D à partir de ses extrémités (points).
 * 
 * origine : extrémité d'origine
 * fin : extrémité de fin
 */
struct segment_2d new_segment(struct point_2d origine, struct point_2d fin);

/**
 * Retourne la longueur d'un segment.
 */
double longueur_segment(struct segment_2d segment_param);

#endif