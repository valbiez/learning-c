#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "11b_lib_calendar.h"
#include "12b_lib.h"

// Déclarations des fonctions du calendrier

struct simple_date current_date()
{
    struct simple_date result;

    time_t rawtime;
    time(&rawtime);
    struct tm *currentTimeInfo;
    currentTimeInfo = localtime(&rawtime);

    result.num_jour_dans_mois = currentTimeInfo->tm_mday;
    result.num_jour_dans_semaine = currentTimeInfo->tm_wday;
    // tm_wday = nombre de jours depuis dimanche (0->6)
    // Ex.
    // mercredi = 3
    // dimanche = 0
    if (result.num_jour_dans_semaine == 0)
        result.num_jour_dans_semaine = 7;
    result.num_mois_dans_annee = currentTimeInfo->tm_mon + 1;
    return result;
}

void affiche_date_by_struct(struct simple_date date)
{
    affiche_date(date.num_jour_dans_mois, date.num_mois_dans_annee, date.num_jour_dans_semaine);
}
