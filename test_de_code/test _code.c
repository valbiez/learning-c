

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//changer le nom des structures et créer d'autre fonction aléatoires force suivant le métier;

int Force_aleatoire()
{
    return rand() % 10 + 1;
}

struct test_structure_fiche_perso
{
    char *Prenom;
    char *Sex;
    char *Metier;
    char *Race;
    int Age;
    int Force;
};

struct test_structure_fiche_perso result_resulta(char *Prenom, char *Sex, char *Metier, char *Race, int Age, int Force)
{
    struct test_structure_fiche_perso result;
    result.Prenom = Prenom;
    result.Sex = Sex;
    result.Metier = Metier;
    result.Race = Race;
    result.Age = Age;
    result.Force = Force;
    return result;
};

struct test_structure_fiche_perso grosse_patate(char *Prenom, char *Sex, char *Race, int Age)
{
    return result_resulta(Prenom, Sex, "Guerrier", Race, Age, Force_aleatoire() + 10);
}


void affiche_fdp(struct test_structure_fiche_perso fiche)
{
    printf("Nom : %s\n", fiche.Prenom);
    printf("Sex : %s\n", fiche.Sex);
    printf("Metier : %s\n", fiche.Metier);
    printf("Race : %s\n", fiche.Race);
    printf("Age : %d\n", fiche.Age);
    printf("Force : %d\n", fiche.Force);
}

void main()
{
    srand(time(NULL));

    struct test_structure_fiche_perso Alois = grosse_patate("Alois", "Female", "Humain", 25);
    struct test_structure_fiche_perso Gogo = result_resulta("Gogo", "Homme", "voleur", "elfe", 36, Force_aleatoire());
    affiche_fdp(Alois);
    affiche_fdp(Gogo);
}

// Génération du nombre aléatoire
//srand(time(NULL));
//Force = rand() % 10 + 1; // nombre entre 1et 10
