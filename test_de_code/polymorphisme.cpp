#include <iostream>
#include <list>

class meuble
{
public:
    virtual void ouvrir() = 0;
    virtual void fermer() = 0;
};

class buffet : public meuble
{
public:
    void ouvrir()
    {
        std::cout << "Ouvre un buffet" << std::endl;
    }

    void fermer()
    {
        std::cout << "Ferme un buffet" << std::endl;
    }

};

class armoire : public meuble
{
public:
    void ouvrir()
    {
        std::cout << "Ouvre une armoire" << std::endl;
    }

    void fermer()
    {
        std::cout << "Ferme une armoire" << std::endl;
    }
};

int main()
{
    std::list<meuble *> meubles;
    meubles.push_back(new armoire());
    meubles.push_back(new buffet());

    for (std::list<meuble *>::iterator it = meubles.begin(); it != meubles.end(); ++it)
    {
        (*it)->ouvrir();
    }

    for (std::list<meuble *>::iterator it = meubles.begin(); it != meubles.end(); ++it)
    {
        (*it)->fermer();
    }

}
