#include <stdio.h>
#include <stdlib.h>

// Nombre de jours par mois
// Voir les instructions : Readme.md

int nombre_de_jours_par_mois(int mois)
{
    switch (mois)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;
        break;

    case 2:
        return 28;
        break;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
        break;
    }
}

void main()
{
    int mois = 1;

    while (mois <= 12)
    {
        printf("%d\n", nombre_de_jours_par_mois(mois));
        mois++;
    }

    // 31 - jan
    // 28 - fev
    // 31 - mar
    // 30 - avr
    // 31 - mai
    // 30 - juin
    // 31 - juillet
    // 31 - aout
    // 30 - septembre
    // 31 - octobre
    // 30 - novembre
    // 31 - decembre
}