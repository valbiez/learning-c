
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define FIZZ 1
#define BUZZ 2
#define FIZZBUZZ 3
#define NUMBER 4

bool est_divisible(int x, int divisor)
{
    return x % divisor == 0;
}

struct fizzbuzz
{
    int category;
    int nombre;
};

/**
 * Créé une structure fizzbuzz de categorie NUMBER avec value comme nombre.
 */
struct fizzbuzz mk_number(int value)
{
    struct fizzbuzz resultat;
    resultat.nombre = value;
    resultat.category = NUMBER;
    return resultat;
}

struct fizzbuzz mk_fizz()
{
    struct fizzbuzz resultat;
    resultat.nombre = 0;
    resultat.category = FIZZ;
    return resultat;
}

struct fizzbuzz mk_buzz()
{
    struct fizzbuzz resultat;
    resultat.nombre = 0;
    resultat.category = BUZZ;
    return resultat;
}

struct fizzbuzz mk_fizzbuzz()
{
    struct fizzbuzz resultat;
    resultat.nombre = 0;
    resultat.category = FIZZBUZZ;
    return resultat;
}

struct fizzbuzz fizzbuzz(int nombre)
{
    if (est_divisible(nombre, 3) && est_divisible(nombre, 5))
    {
        return mk_fizzbuzz();
    }
    if (est_divisible(nombre, 3))
    {
        return mk_fizz();
    }
    if (est_divisible(nombre, 5))
    {
        return mk_buzz();
    }
    return mk_number(nombre);
}

void affiche(struct fizzbuzz result)
{
    switch (result.category)
    {
    case FIZZ:
        printf("fizz\n");
        break;
    case BUZZ:
        printf("buzz\n");
        break;
    case FIZZBUZZ:
        printf("fizzbuzz\n");
        break;
    case NUMBER:
        printf("%d\n", result.nombre);
        break;

    default:
        printf("what ???? %d %d\n", result.nombre, result.category);
        break;
    }
}

int main()
{
    //srand(time(NULL));
    

    for (int i = 1; i < 101; i++)
    {
        int nombre = 0;
        printf("Quel nombre  veux-tu?\n");
        scanf("%d", &nombre);
        affiche(fizzbuzz(nombre));
    }
}
